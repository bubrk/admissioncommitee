# Admission Committee

##Description

This site is an implementation of the task "Admissions Committee" ("Приймальна комісія"). 
The system has a list of departments and educational programmes that can be viewed in the section "departments" or 
"search programme". Implemented the ability to sort by name, by number of budget places or by number of total places. 
Applicant can login or register in the system. During registration, you have to enter full name, email, city, region and
name of the school, attach a scan of the certificate with grades. The applicant can apply for one or more specialties.
When applying for the specialty, the applicant enters the results for the relevant subjects.

The system administrator has the rights:
- adding, deleting, or editing departments and specialities;
- blocking or unlocking the applicant;
- adding the results of applicants to the Report;
- finalization Report for enrollment
  
After finalizing the information, the system calculates the total scores and determines the applicants, enrolled in 
the university for budget places and the contract

##Project structure
```
AdmissionCommittee
├─ documents............... Given task
├─ sql..................... Diagram for database and sql files
└─ src
   └─ main
      ├─ java
      │  └─ edu.university
      │     ├─ config...... Object factory and configurators
      │     ├─ controller.. Controller layer, context listener and command container
      │     │  ├─ command.. Collection of classes command handlers
      │     │  └─ filter... Filters
      │     ├─ dao......... DAO interfaces and Mysql DAO implementation
      │     ├─ entity...... Entity classes
      │     ├─ exception... Custom exception classes
      │     ├─ service..... Services
      │     └─ view........ Classes handlers for custom tags
      ├─ resources......... Resources and translations
      │  └─ I18N
      └─ webapp............ Jsp pages and static web content
```

##Installation

1. Checkout [app.properties](src/main/resources/app.properties) file and change the following settings accordingly to your system:
   - database properties
   - default locations for uploads and reports
>You may create another properties file and change "config.file" property in the [web.xml](src/main/webapp/WEB-INF/web.xml)
   
2. Checkout [sql](sql) folder and run following scripts:
   - [create_db.sql](sql/create_db.sql) for creating database structure
   - [fill_with_test_data.sql](sql/fill_with_test_data.sql) to fill tables with basic data for testing service


3. Optionally check [log4j2.xml](src/main/resources/log4j2.xml) to change default location for log file