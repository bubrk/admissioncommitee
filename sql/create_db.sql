
-- -----------------------------------------------------
-- Schema admissions_db
-- -----------------------------------------------------
-- CREATE SCHEMA IF NOT EXISTS admissions_db DEFAULT CHARACTER SET utf8 ;

DROP DATABASE IF EXISTS admissions_db;

CREATE DATABASE IF NOT EXISTS admissions_db DEFAULT CHARACTER SET utf8;

USE admissions_db;

-- -----------------------------------------------------
-- Table admissions_db.roles
-- -----------------------------------------------------
DROP TABLE IF EXISTS roles;

CREATE TABLE IF NOT EXISTS roles(
    role_id INT AUTO_INCREMENT,         -- role id
    role_name VARCHAR(20) NOT NULL,     -- role name (USER, ADMIN etc)
    PRIMARY KEY (role_id),
    UNIQUE (role_name)
);

-- Fill roles with data
-- DO NOT CHANGE THIS TABLE WITHOUT MODIFYING CORRESPONDING ENUM!
-- See edu.university.entity.Roles
INSERT INTO roles VALUES  (1, 'USER'),
                          (2, 'ADMIN');


-- -----------------------------------------------------
-- Table admissions_db.users
-- -----------------------------------------------------
DROP TABLE IF EXISTS users ;

CREATE TABLE IF NOT EXISTS users(
    user_id INT AUTO_INCREMENT,         -- unique user id
    email VARCHAR(100) NOT NULL,        -- email address
    first_name VARCHAR(40) NOT NULL,    -- first name
    last_name VARCHAR(40) NOT NULL,     -- last name
    middle_name VARCHAR(40),            -- middle name
    password VARCHAR(100) NOT NULL,     -- hash code of the password
    role_id INT NOT NULL,               -- role id
    PRIMARY KEY (user_id),
    UNIQUE INDEX (email),
    CONSTRAINT users_fk1
        FOREIGN KEY (role_id)
        REFERENCES roles(role_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.user_details
-- -----------------------------------------------------
DROP TABLE IF EXISTS user_details ;

CREATE TABLE IF NOT EXISTS user_details(
    user_id INT NOT NULL,               -- unique user id
    region VARCHAR(100) NOT NULL,       -- region
    city VARCHAR(100) NOT NULL,          -- city
    school VARCHAR(100) NOT NULL,       -- school
    PRIMARY KEY (user_id),
    CONSTRAINT  user_details_fk1
        FOREIGN KEY (user_id)
        REFERENCES users(user_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.user_documents
-- -----------------------------------------------------
DROP TABLE IF EXISTS user_documents ;

CREATE TABLE IF NOT EXISTS user_documents(
    document_id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    document_name VARCHAR(200) NOT NULL,
    document_file VARCHAR(40) NOT NULL,
    PRIMARY KEY (document_id),
    UNIQUE (document_file),
    CONSTRAINT  user_documents_fk1
        FOREIGN KEY (user_id)
        REFERENCES users(user_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.users_blacklist
-- -----------------------------------------------------
DROP TABLE IF EXISTS users_blacklist ;

CREATE TABLE IF NOT EXISTS users_blacklist(
                                           user_id INT NOT NULL,
                                           comment VARCHAR(250) NOT NULL,
                                           PRIMARY KEY (user_id),
                                           CONSTRAINT  users_blacklist_fk1
                                               FOREIGN KEY (user_id)
                                                   REFERENCES users(user_id)
                                                   ON DELETE CASCADE
                                                   ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.subjects
-- -----------------------------------------------------
DROP TABLE IF EXISTS subjects ;

CREATE TABLE IF NOT EXISTS subjects(
   subject_id INT AUTO_INCREMENT,
   name VARCHAR(40) NOT NULL,
   PRIMARY KEY (subject_id),
   UNIQUE (name)
);

-- -----------------------------------------------------
-- Table admissions_db.departments
-- -----------------------------------------------------
DROP TABLE IF EXISTS departments;

CREATE TABLE IF NOT EXISTS departments(
    department_id INT NOT NULL AUTO_INCREMENT,
    parent_id INT,
    name VARCHAR(150) NOT NULL,
    description VARCHAR(255) NOT NULL,
    PRIMARY KEY (department_id),
    CONSTRAINT departments_fk1
        FOREIGN KEY (parent_id)
        REFERENCES departments(department_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.programmes
-- -----------------------------------------------------
DROP TABLE IF EXISTS programmes;

CREATE TABLE IF NOT EXISTS programmes(
    programme_id INT NOT NULL AUTO_INCREMENT,
    department_id INT NOT NULL,
    name VARCHAR(150) NOT NULL,
    short_description VARCHAR(255),
    PRIMARY KEY (programme_id),
    UNIQUE (name),
    CONSTRAINT  programmes_fk1
        FOREIGN KEY (department_id)
        REFERENCES departments(department_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table admissions_db.programme_details
-- -----------------------------------------------------
DROP TABLE IF EXISTS programme_details;

CREATE TABLE IF NOT EXISTS programme_details (
    programme_id INT NOT NULL,
    description VARCHAR(500),
    budget_places INT NOT NULL,
    total_places INT NOT NULL,
    admissions_start_date DATETIME NOT NULL,
    admissions_end_date DATETIME NOT NULL,
    PRIMARY KEY (programme_id),
    CONSTRAINT  programme_details_fk1
        FOREIGN KEY (programme_id)
        REFERENCES programmes(programme_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.programme_subjects_requirements
-- -----------------------------------------------------
DROP TABLE IF EXISTS programme_subjects_requirements;

CREATE TABLE IF NOT EXISTS programme_subjects_requirements (
    programme_id INT NOT NULL,
    subject_group INT NOT NULL,
    subject_id INT NOT NULL,
    UNIQUE (programme_id,subject_id),
    CONSTRAINT  programme_requirements_fk1
        FOREIGN KEY (programme_id)
        REFERENCES programmes(programme_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT  programme_requirements_fk2
        FOREIGN KEY (subject_id)
        REFERENCES subjects(subject_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.programme_subjects_scores_requirements
-- -----------------------------------------------------
DROP TABLE IF EXISTS programme_subjects_scores_requirements;

CREATE TABLE IF NOT EXISTS programme_subjects_scores_requirements (
    programme_id INT NOT NULL,
    subject_group INT NOT NULL,
    min_score INT NOT NULL,
    coefficient DOUBLE NOT NULL,
    UNIQUE (programme_id,subject_group),
    CONSTRAINT  programme_scores_requirements_fk1
        FOREIGN KEY (programme_id)
            REFERENCES programmes(programme_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table admissions_db.statuses
-- -----------------------------------------------------
DROP TABLE IF EXISTS statuses ;

CREATE TABLE IF NOT EXISTS statuses(
    status_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NOT NULL,
    PRIMARY KEY (status_id),
    UNIQUE (name)
);

-- Fill statuses with data
-- DO NOT CHANGE THIS TABLE WITHOUT MODIFYING CORRESPONDING ENUM!
-- See edu.university.entity.ApplicationStatus
INSERT INTO statuses VALUES
                         (1,'NEW'),
                         (2,'ACCEPTED'),
                         (3,'REJECTED'),
                         (4,'ENROLLED');


-- -----------------------------------------------------
-- Table admissions_db.applications
-- -----------------------------------------------------
DROP TABLE IF EXISTS applications;

CREATE TABLE IF NOT EXISTS applications(
    application_id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    programme_id INT NOT NULL,
    PRIMARY KEY (application_id),
    INDEX applications_user_id_idx (user_id),
    INDEX applications_department_id_idx (programme_id),
    CONSTRAINT applications_fk1
        FOREIGN KEY (user_id)
            REFERENCES users (user_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT applications_fk2
        FOREIGN KEY (programme_id)
            REFERENCES programmes (programme_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.status_flow
-- -----------------------------------------------------
DROP TABLE IF EXISTS status_flow ;

CREATE TABLE IF NOT EXISTS status_flow(
    status_flow_id INT NOT NULL AUTO_INCREMENT,
    application_id INT NOT NULL,
    status_id INT NOT NULL,
    comment VARCHAR(255),
    create_datetime TIMESTAMP NOT NULL,
    PRIMARY KEY (status_flow_id),
    CONSTRAINT status_flow_fk1
        FOREIGN KEY (application_id)
            REFERENCES applications(application_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT status_flow_fk2
        FOREIGN KEY (status_id)
            REFERENCES statuses(status_id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS application_subject_scores(
    application_id INT NOT NULL,
    subject_id INT NOT NULL,
    score INT NOT NULL,
    UNIQUE (application_id,subject_id),
    CONSTRAINT application_subject_scores_fk1
        FOREIGN KEY (application_id)
        REFERENCES applications (application_id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT application_subject_scores_fk2
        FOREIGN KEY (subject_id)
        REFERENCES subjects (subject_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table admissions_db.reports
-- -----------------------------------------------------
DROP TABLE IF EXISTS reports ;

CREATE TABLE IF NOT EXISTS reports(
    report_id INT NOT NULL AUTO_INCREMENT,
    programme_id INT NOT NULL,
    report_name VARCHAR(200) NOT NULL,
    report_file VARCHAR(40) NOT NULL,
    PRIMARY KEY (report_id),
    UNIQUE (report_file),
    CONSTRAINT  reports_fk1
        FOREIGN KEY (programme_id)
            REFERENCES programmes(programme_id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);