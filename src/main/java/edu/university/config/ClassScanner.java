package edu.university.config;

import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class used for searching implementations of given interfaces in the package
 */
public class ClassScanner {
    private final Reflections scanner;
    private final Map<Class, Class> defaultImplementations;

    /**
     * Default constructor
     * @param packageToScan name of the package for scanning
     * @param defaultImplementations Map of classes which will be used as a default implementation
     */
    public ClassScanner(String packageToScan, Map<Class, Class> defaultImplementations) {
        this.defaultImplementations = defaultImplementations == null ? new HashMap<>() : defaultImplementations;
        this.scanner = new Reflections(packageToScan);
    }

    public Reflections getScanner() {
        return scanner;
    }

    /**
     * Searches implementation for given interface
     * @param ifc given interface
     * @return class implementation of the interface <code>ifc</code>
     */
    public <T> Class<? extends T> getImplementationOfInterface(Class<T> ifc) {
        return defaultImplementations.computeIfAbsent(ifc, aClass -> {
            Set<Class<? extends T>> classes = scanner.getSubTypesOf(ifc);
            if (classes.size() != 1) {
                throw new IllegalStateException(ifc + " has 0 or more than one implementations, please update configuration");
            }
            return classes.iterator().next();
        });
    }

    /**
     * Searches all implementations of given interface in the package
     * @param ifc given interface
     * @return Set of classes implementations of interface <code>ifc</code>
     */
    public <T> Set<Class<? extends T>> getAllImplementationsOfInterface(Class<T> ifc){
        return scanner.getSubTypesOf(ifc);
    }
}
