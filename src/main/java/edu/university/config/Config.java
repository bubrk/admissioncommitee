package edu.university.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <p>This class is used for loading configuration properties from the file specified in <code>web.xml</code>
 * or {@link Config#DEFAULT_PROPERTIES_FILE} if "config.file" parameter does not exist in <code>web.xml</code> file</p>
 * <p>The {@link Config#getProperty(String)} method is used to access specific property value.</p>
 * <p>If an exception occurs during loading
 * properties from the file, then {@link IllegalStateException} will be thrown</p>
 */
public class Config {
    private static final Logger log = LoggerFactory.getLogger(Config.class);

    public static final String DEFAULT_PROPERTIES_FILE = "app.properties";
    public static final String DAO_FACTORY = "daoFactory";
    public static final String DB_CONNECTION_URL = "connection.url";
    public static final String DB_USER_NAME = "connection.username";
    public static final String DB_CONNECTION_PASSWORD = "connection.password";
    public static final String DB_CONNECTION_DRIVER = "connection.driver";
    public static final String SECURITY_SALT = "security.salt";
    public static final String UPLOADS_LOCATION = "uploads.location";

    private static final Properties properties = new Properties();

    private Config() {
    }

    /**
     * Method is used for getting specific property from the config file
     *
     * @param name the property key
     * @return value of the property or <code>null</code> if property key not found in the config
     * @throws IllegalStateException thrown if failed to load parameters from the config file
     * @see Config#DEFAULT_PROPERTIES_FILE
     */
    public static String getProperty(String name) {
        if (properties.isEmpty()) {
            loadPropertiesFromFile();
        }

        if (properties.isEmpty()) {
            return null;
        }

        return properties.getProperty(name);
    }

    private static void loadPropertiesFromFile() {
        try {
            String propFile = System.getProperty("config.file");
            if (propFile == null) {
                propFile = DEFAULT_PROPERTIES_FILE;
            }
            log.debug("Loading resources from: {}", propFile);
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream(propFile);

            properties.load(input);

        } catch (IOException e) {
            final String msg = "Failed to load properties";
            log.error(msg, e);
            throw new IllegalStateException(msg);
        }
    }
}
