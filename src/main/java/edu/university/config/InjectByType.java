package edu.university.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Indicates field that should be injected with default implementation of given interface
 * @see ObjectConfigurator
 * @see InjectByTypeAnnotationObjectConfigurator
 */
@Retention(RUNTIME)
@Target({ElementType.FIELD})
public @interface InjectByType {
}