package edu.university.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * This configurator is used for processing fields indicated with {@link InjectByType} annotation
 * @see ObjectConfigurator
 */
public class InjectByTypeAnnotationObjectConfigurator implements ObjectConfigurator {
    private static final Logger log = LoggerFactory.getLogger(InjectByTypeAnnotationObjectConfigurator.class);

    @Override
    public void configure(Object obj) {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(InjectByType.class)) {
                field.setAccessible(true);
                Object object = ObjectFactory.getInstance()
                        .getObject(field.getType());
                try {
                    field.set(obj, object);
                    log.debug("Injected implementation [{}] for [{}] in [{}]",object.getClass(),field.getName(),obj.getClass());
                } catch (IllegalAccessException e) {
                    log.error("Cannot initialize field [" + field.getName() +
                            "] for class [" + obj.getClass() + "]", e);
                    throw new IllegalStateException("Failed to inject field, program terminated");
                }
            }
        }
    }
}
