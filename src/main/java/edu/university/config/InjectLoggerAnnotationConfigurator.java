package edu.university.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * This configurator is used for processing fields indicated with {@link InjectLogger} annotation
 * @see ObjectConfigurator
 */
public class InjectLoggerAnnotationConfigurator implements ObjectConfigurator {
    private static final Logger log = LoggerFactory.getLogger(InjectLoggerAnnotationConfigurator.class);

    @Override
    public void configure(Object obj) {
        Class<?> implClass = obj.getClass();
        for (Field field : implClass.getDeclaredFields()) {
            InjectLogger annotation = field.getAnnotation(InjectLogger.class);
            if (annotation != null) {
                field.setAccessible(true);
                try {
                    Logger logger = LoggerFactory.getLogger(obj.getClass());
                    injectLogger(obj, field, logger);
                    log.debug("Injected logger for [{}]",obj.getClass());
                } catch (IllegalAccessException | IllegalArgumentException | ClassCastException e) {
                    log.error("Failed to inject logger for {}", obj.getClass(), e);
                    throw new IllegalStateException("Can't initialize object, program terminated");
                }
            }
        }
    }

    private void injectLogger(Object obj, Field field, Logger logger) throws IllegalAccessException {
        if (field.getType() == Logger.class) {
            field.set(obj, logger);
        } else {
            throw new IllegalArgumentException("Can't inject logger into "+ field.getType());
        }
    }
}
