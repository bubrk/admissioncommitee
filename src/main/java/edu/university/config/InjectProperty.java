package edu.university.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Indicates field which should be injected with application property
 * @see ObjectConfigurator
 * @see InjectPropertyAnnotationConfigurator
 * @see Config
 */
@Retention(RUNTIME)
@Target({ElementType.FIELD})
public @interface InjectProperty {
    String value() default "";
}

