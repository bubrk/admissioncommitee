package edu.university.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

/**
 * This configurator is used for processing fields indicated with {@link InjectProperty} annotation.
 * @see ObjectConfigurator
 */
public class InjectPropertyAnnotationConfigurator implements ObjectConfigurator {
    private static final Logger log = LoggerFactory.getLogger(InjectPropertyAnnotationConfigurator.class);

    @Override
    public void configure(Object obj) {
        Class<?> implClass = obj.getClass();
        for (Field field : implClass.getDeclaredFields()) {
            InjectProperty annotation = field.getAnnotation(InjectProperty.class);
            if (annotation != null) {
                String value = annotation.value().isEmpty() ? Config.getProperty(field.getName()) : Config.getProperty(annotation.value());
                field.setAccessible(true);
                try {
                    log.debug("injecting property [{}]=[{}] for [{}]", field.getName(), value, obj.getClass());
                    injectField(obj, field, value);
                    log.debug("injected property [{}]=[{}] for {}", field.getName(), field.get(obj), obj.getClass());
                } catch (IllegalAccessException | IllegalArgumentException | ClassCastException e) {
                    log.error("Failed to inject property {} for {}", field.getName(), obj.getClass(), e);
                    throw new IllegalStateException("Can't initialize object, program terminated");
                }
            }
        }
    }

    private void injectField(Object obj, Field field, String value) throws IllegalAccessException {
        if (field.getType() == String.class) {
            field.set(obj, value);
        }

        if (field.getType() == int.class || field.getType() == Integer.class) {
            field.set(obj, Integer.parseInt(value));
        } else {
            field.set(obj, field.getType().cast(value));
        }
    }
}
