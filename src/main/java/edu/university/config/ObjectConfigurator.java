package edu.university.config;

/**
 * Interface for object configurators. Object configurators are used for configuring new instances
 * created by {@link ObjectFactory}
 */
public interface ObjectConfigurator {
    void configure(Object obj);
}
