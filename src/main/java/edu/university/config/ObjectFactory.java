package edu.university.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Global factory, used for creating and configuring objects
 */
public class ObjectFactory {
    private static final Logger log = LoggerFactory.getLogger(ObjectFactory.class);

    private static ObjectFactory instance;
    private final List<ObjectConfigurator> configurators = new ArrayList<>();
    private Map<Class, Object> cache = new ConcurrentHashMap<>();
    private ClassScanner classScanner;

    protected ObjectFactory() {
        classScanner = new ClassScanner(Config.getProperty("root.package"),null);
        try {
            for (Class<? extends ObjectConfigurator> aClass : classScanner.getScanner().getSubTypesOf(ObjectConfigurator.class)) {
                configurators.add(aClass.getDeclaredConstructor().newInstance());
            }
            log.debug("configurators: {}", configurators);

        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            log.error("Failed to initialize Object Factory", e);
            throw new IllegalStateException("Critical error happen while initializing Object Factory");
        }
    }

    public static synchronized ObjectFactory getInstance() {
        if (instance == null) {
            instance = new ObjectFactory();
        }
        return instance;
    }

    public <T> T getObject(Class<T> type) {
        if (cache.containsKey(type)) {
            return (T) cache.get(type);
        }

        Class<? extends T> implClass = type;

        if (type.isInterface()) {
            implClass = classScanner.getImplementationOfInterface(type);
        }
        T t = createObject(implClass);

        if (!implClass.isAnnotationPresent(NonSingleton.class)) {
            cache.put(type, t);
        }

        return t;
    }

    private <T> T createObject(Class<T> implClass) {
        try {
            log.trace("creating object: {}",implClass);
            T t = create(implClass);

            log.trace("configuring: {}",t);
            configure(t);

            log.trace("post construct: {}",t);
            invokeInit(implClass, t);

            return t;

        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            log.error("Failed to create an object", e);
            throw new IllegalStateException("Critical error happen while creating object inside Object Factory");
        }
    }

    private <T> void invokeInit(Class<T> implClass, T t) throws IllegalAccessException, InvocationTargetException {
        for (Method method : implClass.getMethods()) {
            if (method.isAnnotationPresent(PostConstruct.class)) {
                method.invoke(t);
            }
        }
    }

    private <T> void configure(T t) {
        configurators.forEach(objectConfigurator ->
                objectConfigurator.configure(t));
    }

    private <T> T create(Class<T> implClass) throws InstantiationException, IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
        return implClass.getDeclaredConstructor().newInstance();
    }
}
