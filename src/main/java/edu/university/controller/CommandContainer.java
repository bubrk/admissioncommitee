package edu.university.controller;

import edu.university.config.ClassScanner;
import edu.university.config.Config;
import edu.university.config.ObjectFactory;
import edu.university.controller.command.Command;
import edu.university.controller.command.HttpMethod;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * Container handles commands - classes responsible for processing specific http requests. Command classes implement
 * {@link Command} interface and have {@link RequestHandler} annotation
 *
 * @see Command
 * @see RequestHandler
 * @see Controller
 */
public class CommandContainer {
    private static final Logger log = LoggerFactory.getLogger(CommandContainer.class);

    private static final String COMMANDS_PACKAGE = Config.getProperty("commands.package");

    //Main container for commands
    private static final Map<HttpMethod, Map<String, Command>> container = new EnumMap<>(HttpMethod.class);

    //Initialize container with commands
    static {
        log.debug("Initializing CommandContainer");
        populateContainer();
    }

    /**
     * Returns instance of the {@link Command} by giving name and Http method
     *
     * @param method      HttpMethod (GET, POST etc. See {@link HttpMethod})
     * @param commandName Name of the command (Provided in {@link RequestHandler} annotation)
     * @return instance of the {@link Command} or <code>null</code> if command is not found in the container
     * @see Command
     * @see RequestHandler
     * @see HttpMethod
     * @see Controller
     */
    public static Optional<Command> getCommand(HttpMethod method, String commandName) {
        if (commandName == null || commandName.isEmpty()) {
            return Optional.empty();
        }

        Command cmd = null;
        if (container.containsKey(method)) {
            cmd = container.get(method)
                    .get(commandName);
        }

        if (cmd == null) {
            return Optional.empty();
        }

        return Optional.of(cmd);
    }

    /**
     * Returns a list of roles that are allowed to use given command. If command is opened for any role, method returns an empty array
     *
     * @param commandName name of the command
     * @return array of {@link Roles}
     */
    public static Optional<Roles[]> getAccessList(HttpMethod method, String commandName) {
        Optional<Command> optionalCommand = getCommand(method, commandName);

        return optionalCommand.map(command -> command
                .getClass()
                .getAnnotation(RequestHandler.class)
                .accessList());
    }

    //populating containers
    private static void populateContainer() {
        log.info("Populating commands from package: {}", COMMANDS_PACKAGE);

        ClassScanner scanner = new ClassScanner(COMMANDS_PACKAGE, null);
        Set<Class<? extends Command>> commands = scanner.getAllImplementationsOfInterface(Command.class)
                .stream()
                .filter(cmd -> cmd.isAnnotationPresent(RequestHandler.class))
                .collect(Collectors.toSet());

        if (commands.isEmpty()) {
            String msg = "Failed to initialize CommandContainer, no commands found";
            log.error(msg);
            throw new IllegalStateException(msg);
        }

        //initialize empty container
        for (HttpMethod method : HttpMethod.values()) {
            container.put(method, new HashMap<>());
        }

        //populate container with commands
        ObjectFactory factory = ObjectFactory.getInstance();

        for (Class<? extends Command> cmd : commands) {
            RequestHandler rh = cmd.getAnnotation(RequestHandler.class);
            container.get(rh.method())
                    .put(rh.name(), factory.getObject(cmd));
        }

        log.info("Total commands found: {}", totalCommands());
    }

    private static int totalCommands() {
        return container.values()
                .stream()
                .map(Map::values)
                .mapToInt(Collection::size)
                .sum();
    }
}
