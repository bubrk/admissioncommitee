package edu.university.controller;

import edu.university.config.*;
import edu.university.dao.DAOFactory;
import edu.university.dao.UserDAO;
import edu.university.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

@WebListener
public class ContextListener implements ServletContextListener {
    private static final Logger log = LoggerFactory.getLogger(ContextListener.class);
    CommandContainer commandContainer;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.info("Initializing servlet context");
        ServletContext context = servletContextEvent.getServletContext();

        //set appName
        String appName = context.getContextPath();
        context.setAttribute("appName", appName);
        System.setProperty("appName", appName);
        log.info("appName = {}", appName);

        //Initialize container with commands
        initCommandContainer();

        //Prepare folder which will be used for uploading documents
        prepareUploadsFolder(context);

        //Test connection with database
        testConnectionWithDatabase();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.info("Destroying servlet context");
    }

    private void initCommandContainer() {
        //commandContainer = new CommandContainer();
        log.debug("Command container is ready");
    }

    private void testConnectionWithDatabase() {
        log.info("Testing connection with database...");
        try {
            //DAOFactory df = DAOFactory.getInstance();
            //UserDAO ud = df.getUserDAO();
            UserDAO ud = ObjectFactory.getInstance().getObject(UserDAO.class);
            log.debug("ud: {}",ud);
            ud.getUser(1);
            log.info("Connection established");
        } catch (DAOException e) {
            log.error("Error has occurred while testing connection with database", e);
            throw new IllegalStateException("Failed to connect with database");
        }
    }

    private void prepareUploadsFolder(ServletContext context) {
        try {
            String uploadsLocation = Objects.requireNonNull(
                    Config.getProperty(Config.UPLOADS_LOCATION),
                    "Uploads folder is not specified");

            if (!Files.exists(Paths.get(uploadsLocation))) {
                Files.createDirectory(Paths.get(uploadsLocation));
            }
        } catch (NullPointerException | IOException e) {
            String msg = "Failed to prepare folder for uploads";
            log.error(msg, e);
            throw new IllegalStateException(msg);
        }
    }
}
