package edu.university.controller;

import edu.university.controller.command.Command;
import edu.university.controller.command.HttpMethod;
import edu.university.controller.command.RequestHandler;
import edu.university.exception.ControllerException;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import static edu.university.controller.PagePath.*;


/**
 * Controller layer, responsible for routing. Uses {@link CommandContainer} to process specific commands-requests
 *
 * @see CommandContainer
 * @see Command
 * @see RequestHandler
 */

@WebServlet("/app")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class Controller extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(Controller.class);
    private static final String COMMAND = "cmd";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String cmdName = req.getParameter(COMMAND);
        log.debug("GET request, command: {}", cmdName);

        try {
            if (cmdName == null || cmdName.isEmpty()) {
                log.error("Can't process Get request, command name is empty or null");
                throw new ControllerException("Can't process empty command");
            }

            Command command = CommandContainer.getCommand(HttpMethod.GET, cmdName)
                    .orElseThrow(() -> new ControllerException("Command ["+cmdName+"] does not exist or deleted"));

            if (command == null) {
                log.error("Can't process Get request, no such command: {}", cmdName);
                throw new ControllerException("Command does not exist or deleted");
            }

            String page = command.execute(req, resp);

            if (!page.equals(NO_ACTION)) {
                req.getRequestDispatcher(page).forward(req, resp);
            }

        } catch (Exception e) {
            log.error("Failed to process the GET request");
            log.error("Error message: ", e);

            req.setAttribute("errorMessage", e.getMessage());
            req.getRequestDispatcher(PAGE_ERROR).forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String cmdName = req.getParameter(COMMAND);
        log.debug("POST request, command: {}", cmdName);

        try {
            if (cmdName == null || cmdName.isEmpty()) {
                log.error("Can't process Post request, command name is empty or null");
                throw new ControllerException("Can't process empty command");
            }
            Command command = CommandContainer.getCommand(HttpMethod.POST, cmdName)
                    .orElseThrow(() -> new ControllerException("Command ["+cmdName+"] does not exist or deleted"));

            String page = command.execute(req, resp);
            resp.sendRedirect(page);

        } catch (Exception e) {
            log.error("Failed to process the POST request");
            log.error("Error message: ", e);

            req.getSession().setAttribute("errorMessage", e.getMessage());
            resp.sendRedirect(URL_ERROR_PAGE);
        }
    }
}
