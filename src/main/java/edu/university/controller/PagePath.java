package edu.university.controller;

/**
 * Library for string constants
 */
public class PagePath {
    public static final String PAGES_ROOT = "WEB-INF/jsp/";
    public static final String PAGE_INDEX = "index.jsp";
    public static final String PAGE_ERROR = "/error.jsp";
    public static final String PAGE_FORBIDDEN = "/forbidden.jsp";
    public static final String PAGE_BLACKLISTED = "/blacklisted.jsp";
    public static final String PAGE_WELCOME = PAGES_ROOT + "welcome.jsp";
    public static final String PAGE_LOGIN = PAGES_ROOT + "login.jsp";
    public static final String PAGE_REGISTRATION = PAGES_ROOT + "register.jsp";
    public static final String PAGE_VALIDATION = PAGES_ROOT + "validation.jsp";
    public static final String PAGE_EDIT_INFO = PAGES_ROOT + "personal_info.jsp";
    public static final String PAGE_EDIT_SCHOOL = PAGES_ROOT + "edit_school.jsp";
    public static final String PAGE_DEPARTMENTS = PAGES_ROOT + "departments.jsp";
    public static final String PAGE_MANAGE_DEPARTMENTS = PAGES_ROOT + "manage_departments.jsp";
    public static final String PAGE_PROGRAMME = PAGES_ROOT + "programme.jsp";
    public static final String PAGE_SEARCH_PROGRAMME = PAGES_ROOT + "search.jsp";
    public static final String PAGE_APPLY = PAGES_ROOT + "apply.jsp";
    public static final String PAGE_MY_APPLICATIONS = PAGES_ROOT + "my_applications.jsp";
    public static final String PAGE_APPLICATION = PAGES_ROOT + "application.jsp";
    public static final String PAGE_EDIT_DEPARTMENT = PAGES_ROOT + "edit_department.jsp";
    public static final String PAGE_EDIT_PROGRAMME = PAGES_ROOT + "edit_programme.jsp";
    public static final String PAGE_DELETE_PROGRAMME = PAGES_ROOT + "delete_programme.jsp";
    public static final String PAGE_DELETE_DEPARTMENT = PAGES_ROOT + "delete_department.jsp";
    public static final String PAGE_ADD_DEPARTMENT = PAGES_ROOT + "add_department.jsp";
    public static final String PAGE_MANAGE_APPLICATIONS = PAGES_ROOT + "manage_applications.jsp";
    public static final String PAGE_MANAGE_USERS = PAGES_ROOT + "manage_users.jsp";
    public static final String PAGE_MANAGE_ONE_APPLICATION = PAGES_ROOT + "manage_one_application.jsp";
    public static final String PAGE_MANAGE_ONE_USER = PAGES_ROOT + "manage_one_user.jsp";
    public static final String PAGE_REPORTS = PAGES_ROOT + "reports.jsp";
    public static final String PAGE_ONE_REPORT = PAGES_ROOT + "report_xml.jsp";
    public static final String PAGE_COMMITTEE = PAGES_ROOT + "committee.jsp";

    public static final String NO_ACTION = "";
    public static final String URL_ROOT = System.getProperty("appName");
    public static final String URL_APP = URL_ROOT + "/app";
    public static final String URL_CMD_LOGIN = URL_APP + "?cmd=login";
    public static final String URL_CMD_REGISTER = URL_APP + "?cmd=register";
    public static final String URL_CMD_VALIDATE = URL_APP + "?cmd=validate";
    public static final String URL_CMD_WELCOME = URL_APP + "?cmd=welcome";
    public static final String URL_CMD_DEPARTMENTS = URL_APP + "?cmd=departments";
    public static final String URL_CMD_PROGRAMME = URL_APP + "?cmd=programme";
    public static final String URL_CMD_EDIT_PROGRAMME = URL_APP + "?cmd=editProgramme";
    public static final String URL_CMD_MANAGE_DEPARTMENTS = URL_APP + "?cmd=manageDepartments";
    public static final String URL_CMD_EDIT_DEPARTMENT = URL_APP + "?cmd=editDepartment";
    public static final String URL_CMD_DELETE_PROGRAMME = URL_APP + "?cmd=deleteProgramme";
    public static final String URL_CMD_DELETE_DEPARTMENT = URL_APP + "?cmd=deleteDepartment";
    public static final String URL_CMD_ADD_DEPARTMENT = URL_APP + "?cmd=addDepartment";
    public static final String URL_CMD_EDIT_INFO = URL_APP + "?cmd=personalInfo";
    public static final String URL_CMD_EDIT_SCHOOL = URL_APP + "?cmd=editSchool";
    public static final String URL_CMD_MY_APPLICATIONS = URL_APP + "?cmd=myApplications";
    public static final String URL_CMD_APPLY_FOR_APPLICATION = URL_APP + "?cmd=apply";
    public static final String URL_CMD_MANAGE_APPLICATIONS = URL_APP + "?cmd=manageApplications";
    public static final String URL_CMD_MANAGE_ONE_APPLICATION = URL_APP + "?cmd=manageOneApplication";
    public static final String URL_CMD_MANAGE_ONE_USER = URL_APP + "?cmd=manageOneUser";
    public static final String URL_CMD_GET_REPORT = URL_APP + "?cmd=getReport";
    public static final String URL_ERROR_PAGE = URL_ROOT + "/error.jsp";
    public static final String URL_FORBIDDEN_PAGE = URL_ROOT + "/forbidden.jsp";

    private PagePath() {

    }

}
