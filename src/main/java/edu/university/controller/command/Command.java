package edu.university.controller.command;


import edu.university.controller.CommandContainer;
import edu.university.controller.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command interface with a single execution method
 * @see CommandContainer
 * @see Controller
 */
public interface Command {
    /**
     * Method wil be executed to process a command
     * @param req HttpServletRequest
     * @param resp HttpServletRespond
     * @return path or link where user will be forwarded (for GET method) or redirected (POST method)
     */
    public String execute(HttpServletRequest req, HttpServletResponse resp);
}
