package edu.university.controller.command;

public enum HttpMethod {
    GET, POST, DELETE, UPDATE
}
