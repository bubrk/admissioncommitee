package edu.university.controller.command;

import edu.university.controller.CommandContainer;
import edu.university.controller.Controller;
import edu.university.entity.Roles;

import java.lang.annotation.*;


/**
 * Indicates class responsible for processing http requests
 * name - name of the command
 * method - {@link HttpMethod}
 * accessList - array of {@link Roles}, indicates roles which have access to given command. If empty - command will be accessible
 * for anyone
 * @see CommandContainer
 * @see Controller
 * @see Command
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RequestHandler {
    String name();

    HttpMethod method();

    Roles[] accessList() default {};
}

