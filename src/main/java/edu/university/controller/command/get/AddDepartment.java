package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Department;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Add new department command
 */
@RequestHandler(name = "addDepartment", method = GET, accessList = {Roles.ADMIN})
public class AddDepartment implements Command {
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: addDepartment, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        int parentId = Integer.parseInt(req.getParameter("parentId"));
        req.setAttribute("parentId", parentId);

        return PAGE_ADD_DEPARTMENT;
    }
}