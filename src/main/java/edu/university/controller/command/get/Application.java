package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.User;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.ServiceException;
import edu.university.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;


/**
 * View an application
 */
@RequestHandler(name = "application", method = GET)
public class Application implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationService applicationService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: application, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        if (user == null) {
            session.setAttribute("backURL",req.getRequestURL()
                    .append("?")
                    .append(req.getQueryString())
                    .toString());
            return PAGE_LOGIN;
        }

        int applicationId = Integer.parseInt(req.getParameter("applicationId"));

        try{
            ApplicationSearchResult result = applicationService.getApplicationById(applicationId, user);

            req.setAttribute("application",result.getApplication());
            req.setAttribute("scores",result.getScores());
            req.setAttribute("currentStatus",result.getCurrentStatus());
            req.setAttribute("programme",result.getProgramme());
            req.setAttribute("statusFlow",result.getStatusFlow());
            req.setAttribute("user",result.getUser());
            return PAGE_APPLICATION;

        } catch (ValidationException e) {
            log.trace("trying to access someone else's application {}", user, e);
            return PAGE_FORBIDDEN;

        } catch (ServiceException e){
            String msg = "Failed to get application";
            log.error(msg, e);
            req.setAttribute(ERROR_MESSAGE,msg);
            return PAGE_ERROR;
        }
    }
}