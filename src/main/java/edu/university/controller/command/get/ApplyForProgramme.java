package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Programme;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;


/**
 * Apply for new application
 */
@RequestHandler(name = "apply", method = GET)
public class ApplyForProgramme implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private UserService userService;
    @InjectByType
    private ProgrammeService programmeService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: apply, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        if (user == null) {
            session.setAttribute("backURL",req.getRequestURL()
                    .append("?")
                    .append(req.getQueryString())
                    .toString());
            return PAGE_LOGIN;
        }

        int programmeId = Integer.parseInt(req.getParameter("programmeId"));

        try{
            //todo refactor this
            userService.checkIfUserFilledAllInfo(user.getId());

            req.setAttribute("programme",programmeService.getProgrammeById(programmeId));
            req.setAttribute("programmeDetails",programmeService.getProgrammeDetails(programmeId));
            req.setAttribute("programmeRequirements",programmeService.getProgrammeRequirements(programmeId));
            return PAGE_APPLY;

        } catch (ValidationException e) {
            log.debug("Validation exception while applying, user: {}", user, e);
            session.setAttribute("errorMessage",e.getMessage());
            return PAGE_EDIT_INFO;

        } catch (ServiceException e){
            String msg = "Failed to get programme";
            log.error(msg, e);
            req.setAttribute("errorMessage",msg);
            return PAGE_ERROR;
        }
    }
}