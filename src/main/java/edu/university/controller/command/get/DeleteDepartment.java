package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

import static edu.university.controller.PagePath.PAGE_DELETE_DEPARTMENT;
import static edu.university.controller.PagePath.PAGE_DELETE_PROGRAMME;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * View page confirmation for deleting department
 */
@RequestHandler(name = "deleteDepartment", method = GET, accessList = {Roles.ADMIN})
public class DeleteDepartment implements Command {
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: deleteDepartment, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        HttpSession session = req.getSession();
        int departmentId = Integer.parseInt(req.getParameter("departmentId"));

        session.setAttribute("confirmationKey", generateKey());
        session.setAttribute("departmentId",departmentId);
        return PAGE_DELETE_DEPARTMENT;
    }

    private String generateKey() {
        return UUID.randomUUID().toString();
    }
}
