package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Programme;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Page confirmation for deleting programme
 */
@RequestHandler(name = "deleteProgramme", method = GET, accessList = {Roles.ADMIN})
public class DeleteProgramme implements Command {
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: deleteProgramme, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        HttpSession session = req.getSession();
        int programmeId = Integer.parseInt(req.getParameter("programmeId"));

        session.setAttribute("confirmationKey", generateKey());
        session.setAttribute("programmeId",programmeId);
        return PAGE_DELETE_PROGRAMME;
    }

    private String generateKey() {
        return UUID.randomUUID().toString();
    }
}
