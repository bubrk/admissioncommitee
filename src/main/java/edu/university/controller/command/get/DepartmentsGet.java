package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Department;
import edu.university.entity.Programme;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * View all departments
 */
@RequestHandler(name = "departments", method = GET)
public class DepartmentsGet implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ProgrammeService programmeService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: departments, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        final int parentId;
        String param = req.getParameter("parentId");

        if (param == null || param.isEmpty()) {
            parentId = 0;
        } else {
            parentId = Integer.parseInt(param);
        }

        try {
            if (parentId > 0) {
                Department parentDepartment = programmeService.getDepartment(parentId);
                req.setAttribute("parentDepartment", parentDepartment);
            }

            List<Department> departments = programmeService.getAllDepartments(parentId);

            List<Programme> programmes;

            if (parentId != 0) {
                programmes = programmeService.getAllProgrammesInDepartment(parentId);
            } else {
                programmes = new ArrayList<>();
            }

            req.setAttribute("departments", departments);
            req.setAttribute("programmes", programmes);

            return PAGE_DEPARTMENTS;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}