package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Department;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Edit department command
 */
@RequestHandler(name = "editDepartment", method = GET, accessList = {Roles.ADMIN})
public class EditDepartment implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    ProgrammeService programmeService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: editDepartment, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        try {
            int departmentId = Integer.parseInt(req.getParameter("departmentId"));

            Department department = programmeService.getDepartment(departmentId);
            req.setAttribute("department",department);

            return PAGE_EDIT_DEPARTMENT;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}