package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Programme;
import edu.university.entity.Roles;
import edu.university.entity.Subject;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import edu.university.service.SubjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Edit programme command
 */
@RequestHandler(name = "editProgramme", method = GET, accessList = {Roles.ADMIN})
public class EditProgramme implements Command {
    @InjectLogger
    private static Logger log;
    @InjectByType
    private ProgrammeService programmeService;
    @InjectByType
    private SubjectService subjectService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: editProgramme, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        try {
            int departmentId = Integer.parseInt(req.getParameter("departmentId"));
            req.setAttribute("departmentId", departmentId);

            //if programme exists
            if (req.getParameter("programmeId") != null &&
                    !"0".equals(req.getParameter("programmeId"))) {
                int programmeId = Integer.parseInt(req.getParameter("programmeId"));
                req.setAttribute("programmeId", programmeId);
                req.setAttribute("editProgramme", programmeService.getProgrammeById(programmeId));
                req.setAttribute("editProgrammeDetails", programmeService.getProgrammeDetails(programmeId));
                req.setAttribute("editProgrammeRequirements", programmeService.getProgrammeRequirements(programmeId));
            }

            String subjects = subjectService.getAllSubjects()
                    .stream()
                    .map(Subject::getName)
                    .map(str -> "\"" + str + "\":null")
                    .collect(Collectors.joining(",", "{", "}"));
            req.setAttribute("subjectList", subjects);

            return PAGE_EDIT_PROGRAMME;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}