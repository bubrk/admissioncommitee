package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.Subject;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.SubjectService;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Edit school info command
 */
@RequestHandler(name = "editSchool", method = GET)
public class EditSchool implements Command {
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: editSchool, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        HttpSession session = req.getSession();
        if (session.getAttribute("loggedUser") == null) {
            return PAGE_LOGIN;
        }

        return PAGE_EDIT_SCHOOL;
    }
}