package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.config.InjectProperty;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Document;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Get uploaded document
 */
@RequestHandler(name = "getDocument", method = GET, accessList = {Roles.USER, Roles.ADMIN})
public class GetDocument implements Command {
    @InjectLogger
    private Logger log;
    @InjectProperty("uploads.location")
    private String uploadsLocation;

    @InjectByType
    UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: getDocument, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        int documentId = Integer.parseInt(req.getParameter("documentId"));

        try {
            if (user.getRole()!=Roles.ADMIN && !userService.isUserOwnerOfTheDocument(user.getId(),documentId)){
                return PAGE_FORBIDDEN;
            }

            Document doc = userService.getUserDocument(documentId);
            String file = userService.getUserDocumentFile(documentId);

            String path = uploadsLocation + File.separator + file;

            if (Files.exists(Paths.get(path))){
                startDownloading(resp, doc.getName(), path);
                return NO_ACTION;
            } else {
                log.error("File des not exist or was deleted: {}", file);
                session.setAttribute(ERROR_MESSAGE, "File does not exist");
                return PAGE_ERROR;
            }
        } catch (ServiceException e) {
            String msg = "Failed to download document";
            log.error(msg, e);
            session.setAttribute("errorMessage", msg);
            return PAGE_ERROR;
        }
    }

    private void startDownloading(HttpServletResponse resp, String fileName, String file) {
        log.debug("Start downloading {}",file);

        resp.setContentType("text/plain");
        resp.setHeader("Content-disposition", String.format("attachment; filename=%s", fileName));

        try (InputStream in = Files.newInputStream(Paths.get(file));
             OutputStream out = resp.getOutputStream()) {

            byte[] buffer = new byte[100];

            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
        } catch (IOException e) {
            log.error("Failed to download {}",file,e);
        }
    }
}