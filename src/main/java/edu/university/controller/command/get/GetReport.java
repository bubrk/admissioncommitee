package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * View report for programme
 */
@RequestHandler(name = "getReport", method = GET, accessList = Roles.ADMIN)
public class GetReport implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    ReportService reportService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: getReport, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        try {
            int reportId = Integer.parseInt(req.getParameter("reportId"));
            req.setAttribute("reportXML", reportService.getReportAsXMLStringByReportId(reportId));
            return PAGE_ONE_REPORT;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}