package edu.university.controller.command.get;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.university.config.CommonStrings;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Login page
 */
@RequestHandler(name = "login", method = GET)
public class Login implements Command {
    @InjectLogger
    private static Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: login, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        return PAGE_LOGIN;
    }
}