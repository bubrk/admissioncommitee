package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static edu.university.controller.PagePath.PAGE_INDEX;
import static edu.university.controller.PagePath.PAGE_LOGIN;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Logout command
 */
@RequestHandler(name = "logout", method = GET)
public class Logout implements Command {
    @InjectLogger
    private Logger log;
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: logout, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();

        session.invalidate();

        return PAGE_LOGIN;
    }
}