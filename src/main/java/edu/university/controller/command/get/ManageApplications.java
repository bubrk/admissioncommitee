package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.ServiceException;
import edu.university.service.ApplicationService;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.university.exception.ValidationException;
import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Manage applications command
 */
@RequestHandler(name = "manageApplications", method = GET, accessList = {Roles.ADMIN})
public class ManageApplications implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationService applicationService;
    @InjectByType
    private ProgrammeService programmeService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: manageApplications, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        try {
            List<ApplicationSearchResult> result = applicationService.getAllApplications();
            req.setAttribute("searchResult", result);
            req.setAttribute("allProgrammes", programmeService.getAllProgrammes());

            return PAGE_MANAGE_APPLICATIONS;

        } catch (ServiceException e) {
            log.error("Failed to find applications", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}