package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.ServiceException;
import edu.university.service.ApplicationService;
import edu.university.service.ProgrammeService;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Manage one application
 */
@RequestHandler(name = "manageOneApplication", method = GET, accessList = {Roles.ADMIN})
public class ManageOneApplication implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationService applicationService;
    @InjectByType
    private ProgrammeService programmeService;
    @InjectByType
    private UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: manageOneApplication, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");

        int applicationId = Integer.parseInt(req.getParameter("applicationId"));

        try{
            ApplicationSearchResult applicationInfo = applicationService.getApplicationById(applicationId, user);

            req.setAttribute("application",applicationInfo.getApplication());
            req.setAttribute("scores",applicationInfo.getScores());
            req.setAttribute("currentStatus",applicationInfo.getCurrentStatus());
            req.setAttribute("programme",applicationInfo.getProgramme());
            req.setAttribute("statusFlow",applicationInfo.getStatusFlow());
            req.setAttribute("user",applicationInfo.getUser());

            req.setAttribute("programmeRequirements", programmeService.getProgrammeRequirements(applicationInfo.getProgramme().getId()));

            req.setAttribute("userDocuments", userService.findUserDocuments(applicationInfo.getUser().getId()));
            req.setAttribute("userDetails", userService.getUserDetails(applicationInfo.getUser().getId())
                    .orElseThrow(() -> new ServiceException("Bad application< there is no user datails in database")));

            return PAGE_MANAGE_ONE_APPLICATION;

        } catch (ValidationException e) {
            log.trace("trying to access someone else's application {}", user, e);
            return PAGE_FORBIDDEN;

        } catch (ServiceException e){
            String msg = "Failed to get application";
            log.error(msg, e);
            req.setAttribute("errorMessage",msg);
            return PAGE_ERROR;
        }
    }
}