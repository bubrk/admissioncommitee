package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.ServiceException;
import edu.university.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Manage one user
 */
@RequestHandler(name = "manageOneUser", method = GET, accessList = {Roles.ADMIN})
public class ManageOneUser implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private UserService userService;
    @InjectByType
    private ApplicationService applicationService;
    @InjectByType
    private UserBlacklistService blacklistService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: manageOneUser, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        int userId = Integer.parseInt(req.getParameter("userId"));

        try {
            req.setAttribute("user", userService.getUserById(userId));
            req.setAttribute("userDetails", userService.getUserDetails(userId)
                    .orElse(null));
            req.setAttribute("userDocuments", userService.findUserDocuments(userId));

            boolean isBlocked = blacklistService.isUserInBlackList(userId);
            req.setAttribute("isBlocked", isBlocked);
            if (isBlocked){
                req.setAttribute("comment", blacklistService.getBlacklistComment(userId));
            }

            List<ApplicationSearchResult> applications = applicationService.getApplicationsByUserId(userId);
            req.setAttribute("applications", applications);

            return PAGE_MANAGE_ONE_USER;

        } catch (ServiceException e) {
            String msg = "Failed to get information about user, userId = " + userId;
            log.error(msg, e);
            req.setAttribute("errorMessage", msg);
            return PAGE_ERROR;
        }
    }
}