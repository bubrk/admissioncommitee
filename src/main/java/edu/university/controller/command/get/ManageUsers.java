package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.university.exception.ValidationException;
import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Manage users
 */
@RequestHandler(name = "manageUsers", method = GET, accessList = {Roles.ADMIN})
public class ManageUsers implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: manageUsers, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        try {
            List<User> users = userService.searchUsers();
            req.setAttribute("users",users);

            return PAGE_MANAGE_USERS;

        } catch (ValidationException | ServiceException e) {
            log.error("Failed to find applications", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}