package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.User;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.ServiceException;
import edu.university.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;


@RequestHandler(name = "myApplications", method = GET)
public class MyApplications implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationService applicationService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: myApplications, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        if (user == null) {
            session.setAttribute("backURL",req.getRequestURL()
                    .append("?")
                    .append(req.getQueryString())
                    .toString());
            return PAGE_LOGIN;
        }

        try{
            List<ApplicationSearchResult> result = applicationService.getApplicationsByUserId(user.getId());
            req.setAttribute("searchResults",result);

            return PAGE_MY_APPLICATIONS;

        } catch (ServiceException e){
            String msg = "Failed to get applications";
            log.error(msg, e);
            req.setAttribute("errorMessage",msg);
            return PAGE_ERROR;
        }
    }
}