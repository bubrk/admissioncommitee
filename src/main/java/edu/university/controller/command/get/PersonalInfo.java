package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Document;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.List;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * View personal information
 */
@RequestHandler(name = "personalInfo", method = GET)
public class PersonalInfo implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: personalInfo, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");

        if (user == null) {
            return PAGE_LOGIN;
        }

        if (session.getAttribute("userDetails") != null &&
                session.getAttribute("userDocuments") != null) {
            return PAGE_EDIT_INFO;
        }

        try {
            if (session.getAttribute("userDetails") == null){
                setUserDetailsToSession(user,session);
            }

            if (session.getAttribute("userDocuments") == null){
                setUserDocumentsToSession(user,session);
            }
            return PAGE_EDIT_INFO;

        } catch (ServiceException e) {
            log.error("Service error, failed to find personal info", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }

    private void setUserDocumentsToSession(User user, HttpSession session) throws ServiceException {
        List<Document> documents = userService.findUserDocuments(user.getId());

        if (documents != null && !documents.isEmpty()) {
            session.setAttribute("userDocuments", documents);
        }
    }

    private void setUserDetailsToSession(User user, HttpSession session) throws ServiceException {
        userService.getUserDetails(user.getId())
                .ifPresent(userDetails ->
                        session.setAttribute("userDetails", userDetails));
    }
}