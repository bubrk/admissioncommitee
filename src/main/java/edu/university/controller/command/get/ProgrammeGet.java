package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.*;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import edu.university.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.Doc;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Comparator;
import java.util.Date;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * View programme
 */
@RequestHandler(name = "programme", method = GET)
public class ProgrammeGet implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    ProgrammeService programmeService;
    @InjectByType
    ReportService reportService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: programme, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        User user = (User) req.getSession().getAttribute("loggedUser");

        try {
            int programmeId = Integer.parseInt(req.getParameter("programmeId"));

            Programme programme = programmeService.getProgrammeById(programmeId);
            ProgrammeDetails programmeDetails = programmeService.getProgrammeDetails(programmeId);

            req.setAttribute("programme", programme);
            req.setAttribute("programmeDetails", programmeDetails);
            req.setAttribute("programmeRequirements", programmeService.getProgrammeRequirements(programmeId));
            req.setAttribute("department", programmeService.getDepartment(programme.getDepartmentId()));

            Date now = new Date();
            if (now.compareTo(programmeDetails.getAdmissionsEndDate()) > 0) {
                req.setAttribute("applicationsClosed", true);
                if (user != null && user.getRole() == Roles.ADMIN) {
                    req.setAttribute("reports", reportService.getReportsByProgrammeId(programmeId));
                }
            } else {
                if (now.compareTo(programmeDetails.getAdmissionsStartDate()) < 0) {
                    req.setAttribute("applicationsNotOpenedYet", true);
                } else {
                    req.setAttribute("applicationsNow", true);
                }
            }
            return PAGE_PROGRAMME;

        } catch (NumberFormatException e) {
            String msg = "Wrong or incorrect programme id";
            log.error(msg, e);
            req.setAttribute("errorMessage", msg);
            return PAGE_ERROR;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}