package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static edu.university.controller.PagePath.PAGE_DEPARTMENTS;
import static edu.university.controller.PagePath.PAGE_REGISTRATION;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Open registration page
 */
@RequestHandler(name = "register", method = GET)
public class Register implements Command {
    @InjectLogger
    private static Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: register, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        return PAGE_REGISTRATION;
    }
}