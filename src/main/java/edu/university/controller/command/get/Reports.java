package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;


import edu.university.entity.Document;
import edu.university.entity.Programme;
import edu.university.entity.Report;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.Doc;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;
import java.util.stream.Collectors;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * View all reports
 */
@RequestHandler(name = "reports", method = GET, accessList = Roles.ADMIN)
public class Reports implements Command {
    @InjectLogger
    private Logger log;

    @InjectByType
    private ReportService reportService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: reports, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        try {
            Map<Programme, Document> reports = reportService.getAllReports();

            LinkedHashMap<Programme, Document> sortedReports = reports.entrySet().stream()
                    .sorted(Comparator.comparing(e->e.getValue().getId()))
                    .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,(e1,e2)->e1,LinkedHashMap::new));

            req.setAttribute("reports", sortedReports);
            return PAGE_REPORTS;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }
}