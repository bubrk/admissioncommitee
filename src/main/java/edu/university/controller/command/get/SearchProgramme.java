package edu.university.controller.command.get;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Programme;
import edu.university.entity.ProgrammeDetails;
import edu.university.entity.dto.Paginator;
import edu.university.entity.dto.Tuple2;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Search programme page
 */
@RequestHandler(name = "searchProgramme", method = GET)
public class SearchProgramme implements Command {
    @InjectLogger
    private static Logger log;

    @InjectByType
    private ProgrammeService programmeService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: searchProgramme, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        String programmeName = req.getParameter("programme");
        String sortingType = req.getParameter("sort");
        String dateQuery = req.getParameter("dates");

        try {
            Map<String,String> props = new HashMap<>();
            props.put("programmeName", programmeName);
            props.put("sortingType", sortingType);
            props.put("dateQuery",dateQuery);

            Paginator paginator = getPaginator(req);

            List<Tuple2<Programme, ProgrammeDetails>> programmes = programmeService.searchProgrammesWithFilter(props, paginator);
            if (!programmes.isEmpty()){
                req.setAttribute("programmes",programmes);
            }

            req.setAttribute("paginator", paginator);
            req.setAttribute("programmeName",programmeName);
            req.setAttribute("sortingType",sortingType);
            req.setAttribute("dateQuery",dateQuery);
            req.setAttribute("SELF",req.getRequestURL()
                            .append("?")
                            .append(req.getQueryString())
                            .toString());

            return PAGE_SEARCH_PROGRAMME;

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.setAttribute("errorMessage", e.getMessage());
            return PAGE_ERROR;
        }
    }

    private Paginator getPaginator(HttpServletRequest req) {
        Paginator paginator = new Paginator();

        String page = req.getParameter("page");
        if (page!=null && !page.isEmpty()){
            paginator.setCurrentPage(Integer.parseInt(page));
        }

        String pageSize = req.getParameter("pageSize");
        if (pageSize!=null && !pageSize.isEmpty()){
            paginator.setPageSize(Integer.parseInt(pageSize));
        }
        return paginator;
    }
}