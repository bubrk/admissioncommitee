package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.config.InjectProperty;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.service.RandomCodeGeneratorService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Calendar;
import java.util.Date;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Validation for registration page
 */
@RequestHandler(name = "validate", method = GET)
public class Validate implements Command {
    @InjectLogger
    private Logger log;
    @InjectProperty("validation.time_limit")
    private int TIME_LIMIT;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: validate, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();

        session.setAttribute("validationCode", RandomCodeGeneratorService.generateCode());
        session.setAttribute("validationTimeLimit", TIME_LIMIT);

        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        session.setAttribute("validationValidUntil", new Date(t + TIME_LIMIT));

        return PAGE_VALIDATION;
    }
}