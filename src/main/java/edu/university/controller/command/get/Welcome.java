package edu.university.controller.command.get;

import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;

/**
 * Open welcome page
 */
@RequestHandler(name = "welcome", method = GET)
public class Welcome implements Command {
    @InjectLogger
    private Logger log;
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: welcome, method: GET");
        log.debug("Parameters: {}",req.getParameterMap());

        return PAGE_WELCOME;
    }
}