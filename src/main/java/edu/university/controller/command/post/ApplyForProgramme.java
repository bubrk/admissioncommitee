package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.*;
import edu.university.exception.ServiceException;
import edu.university.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import java.util.*;
import java.util.stream.Collectors;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Apply for programme command
 * Subject ids and scores come in the following form:
 * group_subject_xx=id & group_score_xx=score - where xx is number of the group
 */
@RequestHandler(name = "apply", method = POST, accessList = {Roles.USER, Roles.ADMIN})
public class ApplyForProgramme implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationService applicationService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: apply, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        int programmeId = Integer.parseInt(req.getParameter("programmeId"));

        try {
            Map<Subject, Score> scores = extractScores(req.getParameterMap());

            if (applicationService.hasActiveApplication(user.getId(), programmeId)){
                throw new ValidationException("You already have active application for this programme!");
            }

            boolean isSuccessfullySaved = applicationService.saveApplication(new Application.Builder()
                            .userId(user.getId())
                            .programmeId(programmeId)
                            .scores(scores)
                            .build(),
                    scores);

            if (!isSuccessfullySaved){
                log.error("not saved");
                throw new ServiceException("Unknown error occurred whale saving new application");
            }
            return URL_CMD_MY_APPLICATIONS;

        } catch (ValidationException e){
            log.debug("Parameters violation: ",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_CMD_APPLY_FOR_APPLICATION+"&programmeId="+programmeId;

        } catch (ServiceException e){
            log.error("Service error",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_ERROR_PAGE;
        }
    }


    private Map<Subject, Score> extractScores(Map<String, String[]> parameterMap) throws ValidationException {

        try {
            return parameterMap.entrySet()
                    .stream()
                    .filter(entry -> entry.getKey().matches("group_subject_\\d+"))
                    .collect(Collectors.toMap(
                            entry -> extractSubject(entry.getValue()[0]),
                            entry -> extractScore(entry.getKey(), parameterMap)));
        } catch (NumberFormatException | NullPointerException e) {
            throw new ValidationException("Illegal data format");
        }
    }

    private Score extractScore(String subjectGroupName, Map<String, String[]> parameterMap) {
        String scoreName = subjectGroupName.replace("subject", "score");
        int value = Integer.parseInt(parameterMap.get(scoreName)[0]);

        return new Score(value);
    }

    private Subject extractSubject(String parameter) {
        return new Subject.Builder()
                .id(Integer.parseInt(parameter))
                .build();
    }
}
