package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserBlacklistService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command block user
 */
@RequestHandler(name = "blockUser", method = POST, accessList = {Roles.ADMIN})
public class BlockUser implements Command {
    @InjectLogger
    private Logger log;

    @InjectByType
    private UserBlacklistService blacklistService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: blockUser, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User adminUser = (User) session.getAttribute("loggedUser");
        int userId = Integer.parseInt(req.getParameter("userId"));
        String comment = req.getParameter("comment");

        try {
            if (adminUser.getId()==userId){
                throw new ValidationException("Sorry, but you can't blacklist yourself");
            }
            if (blacklistService.addUserToBlackList(userId, comment)){
                return URL_CMD_MANAGE_ONE_USER+"&userId="+userId;
            } else{
                throw new ServiceException("Failed to add user to blacklist");
            }

        } catch (ValidationException e){
            log.debug("Parameters violation: ",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_CMD_MANAGE_ONE_USER+"&userId="+userId;

        } catch (ServiceException e){
            log.error("Service error",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_ERROR_PAGE;
        }
    }
}
