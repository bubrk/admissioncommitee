package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Department;
import edu.university.entity.Programme;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command delete department
 */
@RequestHandler(name = "deleteDepartment", method = POST, accessList = {Roles.ADMIN})
public class DeleteDepartment implements Command {
    @InjectByType
    private ProgrammeService programmeService;
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: deleteDepartment, method: POST");
        log.debug("Parameters: {}", req.getParameterMap());

        HttpSession session = req.getSession();
        int departmentId = Integer.parseInt(req.getParameter("departmentId"));
        String confirmationKey = req.getParameter("confirmationKey");

        String storedKey = (String) session.getAttribute("confirmationKey");
        session.removeAttribute("confirmationKey");

        try {
            if (!programmeService.getAllProgrammesInDepartment(departmentId).isEmpty() ||
                    !programmeService.getAllDepartments(departmentId).isEmpty()) {
                log.debug("Try to delete not empty department");
                throw new ValidationException("Department is not empty");
            }

            if (confirmationKey == null || confirmationKey.isEmpty() ||
                    storedKey == null || storedKey.isEmpty() ||
                    !confirmationKey.equals(storedKey)) {
                throw new ServiceException("Invalid confirmation key");
            }

            Department department = programmeService.getDepartment(departmentId);
            if (programmeService.deleteDepartment(departmentId)) {
                return URL_CMD_MANAGE_DEPARTMENTS + "&parentId=" + department.getParentId();
            } else {
                throw new ServiceException("Failed to delete department id=" + departmentId);
            }
        } catch (ValidationException e) {
            session.setAttribute("errorMessage", "Failed to delete department." +
                    "Make sure department does not contain any programme or other departments. You can delete only an empty department.");
            return URL_CMD_DELETE_DEPARTMENT+"&departmentId="+departmentId;

        } catch (ServiceException e) {
            String msg = "Failed to delete department";
            log.error(msg, e);
            session.setAttribute("errorMessage", msg);
            return URL_ERROR_PAGE;
        }
    }
}
