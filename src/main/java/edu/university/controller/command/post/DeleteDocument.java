package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command delete document
 */
@RequestHandler(name = "deleteDocument", method = POST, accessList = {Roles.USER, Roles.ADMIN})
public class DeleteDocument implements Command {
    @InjectLogger
    private Logger log;

    @InjectByType
    private UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: deleteDocument, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        int documentId = Integer.parseInt(req.getParameter("documentId"));

        try {
            userService.deleteUserDocument(user, documentId);
            session.removeAttribute("userDocuments");
            return NO_ACTION;

        } catch (ValidationException e) {
            return URL_FORBIDDEN_PAGE;

        } catch (ServiceException e) {
            String msg = "Failed to delete document";
            log.error(msg, e);
            session.setAttribute("errorMessage", msg);
            return URL_ERROR_PAGE;
        }
    }
}
