package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Programme;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import edu.university.service.UserService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import java.util.UUID;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command delete programme
 */
@RequestHandler(name = "deleteProgramme", method = POST, accessList = {Roles.ADMIN})
public class DeleteProgramme implements Command {
    @InjectByType
    private ProgrammeService programmeService;
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: deleteProgramme, method: POST");
        log.debug("Parameters: {}", req.getParameterMap());

        HttpSession session = req.getSession();
        int programmeId = Integer.parseInt(req.getParameter("programmeId"));
        String confirmationKey = req.getParameter("confirmationKey");

        String storedKey = (String) session.getAttribute("confirmationKey");
        session.removeAttribute("confirmationKey");

        try {
            if (confirmationKey == null || confirmationKey.isEmpty() ||
                    storedKey == null || storedKey.isEmpty() ||
                    !confirmationKey.equals(storedKey)) {
                throw new ServiceException("Invalid confirmation key");
            }

            Programme programme = programmeService.getProgrammeById(programmeId);
            if (programmeService.deleteProgramme(programmeId)) {
                return URL_CMD_MANAGE_DEPARTMENTS + "&parentId=" + programme.getDepartmentId();
            } else {
                throw new ServiceException("Failed to delete programme id=" + programmeId);
            }
        } catch (ServiceException e) {
            String msg = "Failed to delete programme";
            log.error(msg, e);
            session.setAttribute("errorMessage", msg);
            return URL_ERROR_PAGE;
        }
    }
}
