package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Department;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command edit department
 */
@RequestHandler(name = "editDepartment", method = POST, accessList = {Roles.ADMIN})
public class EditDepartment implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    ProgrammeService programmeService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: editDepartment, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        String departmentName = req.getParameter("name");
        String departmentDescription = req.getParameter("description");
        int departmentId = Integer.parseInt(req.getParameter("departmentId"));
        int parentId = Integer.parseInt(req.getParameter("parentId"));

        try {
            //TODO validation

            Department department = new Department.Builder()
                    .id(departmentId)
                    .name(departmentName)
                    .description(departmentDescription)
                    .parentId(parentId)
                    .build();

            programmeService.saveDepartment(department);
            return URL_CMD_MANAGE_DEPARTMENTS+"&parentId="+parentId;

        } catch (ValidationException e){
            session.setAttribute(ERROR_MESSAGE,e.getMessage());
            session.setAttribute("name",departmentName);
            session.setAttribute("description",departmentDescription);
            session.setAttribute("parentId",parentId);
            session.setAttribute("departmentId",departmentId);
            return URL_CMD_EDIT_DEPARTMENT+"&departmentId="+departmentId;

        } catch (ServiceException e){
            log.error("Service exception while saving department id = {}",departmentId,e);
            session.setAttribute(ERROR_MESSAGE, "Failed to save department");
            return URL_ERROR_PAGE;
        }
    }
}
