package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.entity.UserDetails;
import edu.university.exception.ServiceException;
import edu.university.exception.ValidationException;
import edu.university.service.UserService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command edit school
 */
@RequestHandler(name = "editSchool", method = POST, accessList = {Roles.USER, Roles.ADMIN})
public class EditSchool implements Command {
    @InjectLogger
    private Logger log;

    @InjectByType
    private UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: editSchool, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loggedUser");
        UserDetails currentUserDetails = (UserDetails) session.getAttribute("userDetails");

        try {
            String region = req.getParameter("region");
            String city = req.getParameter("city");
            String school = req.getParameter("school");

            UserDetails updatedDetails = new UserDetails.Builder()
                    .userId(user.getId())
                    .region(region)
                    .city(city)
                    .school(school)
                    .build();

            //TODO validation

            log.debug("current user details {}", currentUserDetails);
            log.debug("updated user details {}", updatedDetails);

            if (userService.saveUserDetails(updatedDetails)) {
                session.setAttribute("userDetails", updatedDetails);
            }
            return URL_CMD_EDIT_INFO;
        } catch (ValidationException e){
            session.setAttribute(ERROR_MESSAGE,e.getMessage());
            return URL_CMD_EDIT_SCHOOL;
        } catch (NumberFormatException | NullPointerException | ServiceException e){
            String msg = "Received wrong data";
            log.error(msg,e);
            session.setAttribute(ERROR_MESSAGE, msg);
            return URL_ERROR_PAGE;
        }
    }
}
