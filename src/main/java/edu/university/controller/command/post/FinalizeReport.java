package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Application;
import edu.university.entity.Document;
import edu.university.entity.Report;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import edu.university.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.GET;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command finalize report
 */
@RequestHandler(name = "finalize", method = POST)
public class FinalizeReport implements Command {
    @InjectLogger
    private static Logger log;

    @InjectByType
    private ReportService reportService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: finalize, method: GET");
        log.debug("Parameters: {}", req.getParameterMap());

        int programmeId = Integer.parseInt(req.getParameter("programmeId"));

        try {
            if (!reportService.getReportsByProgrammeId(programmeId).isEmpty()){
                throw new ServiceException("Report exists already");
            }
            Report report = reportService.generateReport(programmeId);

            Document document = reportService.saveReport(report, "Report " + new Date())
                    .orElseThrow(()->new ServiceException("Failed to save report"));

            return URL_CMD_GET_REPORT+"&reportId="+document.getId();

        } catch (ServiceException e) {
            log.error("Service error", e);
            req.getSession()
                    .setAttribute("errorMessage", e.getMessage());
            return URL_ERROR_PAGE;
        }
    }

}