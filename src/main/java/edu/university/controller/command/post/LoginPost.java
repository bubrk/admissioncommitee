package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import edu.university.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Login command
 */
@RequestHandler(name = "login", method = POST)
public class LoginPost implements Command {
    private static final Logger log = LoggerFactory.getLogger(LoginPost.class);

    @InjectByType
    private UserService userService;
    @InjectByType
    private SecurityService securityService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: login, method: POST");

        HttpSession session = req.getSession();
        String email = req.getParameter("email");
        String password = securityService.getHashedPassword(req.getParameter("password"));
        User user = null;
        try {
            user = userService.getUser(email, password);

        } catch (ServiceException e) {
            log.debug("Failed to login", e);

            session.setAttribute("errorMessage", e.getMessage());
            session.setAttribute("loginEmail", email);

            return URL_CMD_LOGIN;
        }

        session.setAttribute("loggedUser", user);

        String backUrl = (String) session.getAttribute("backUrl");
        if (backUrl!=null){
            session.removeAttribute("backUrl");
            return backUrl;
        }
        return URL_CMD_DEPARTMENTS;
    }
}
