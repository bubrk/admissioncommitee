package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.service.SecurityService;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import edu.university.exception.ValidationException;

import java.util.Set;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Registration command
 */
@RequestHandler(name = "register", method = POST)
public class RegisterPost implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private SecurityService securityService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: register, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        final String password = req.getParameter("userPassword");
        final String email = req.getParameter("userEmail");
        final String firstName = req.getParameter("firstName");
        final String lastName = req.getParameter("lastName");
        final String middleName = req.getParameter("middleName");

        //TODO refactor this
        try{
            //security service
            User newUser = new User.Builder()
                    .email(email)
                    .firstName(firstName)
                    .lastName(lastName)
                    .middleName(middleName)
                    .password(securityService.getHashedPassword(password))
                    .role(Roles.USER)
                    .build();
            log.debug("new user: {}",newUser);
            session.setAttribute("newUser",newUser);

            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            Set<ConstraintViolation<User>> violations = validator.validate(newUser);

            if (violations.isEmpty()){
                return URL_CMD_VALIDATE;
            }

            throw new ValidationException(violations.iterator()
                    .next().getMessage());

        } catch (ValidationException e){
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_CMD_REGISTER;
        }


    }
}
