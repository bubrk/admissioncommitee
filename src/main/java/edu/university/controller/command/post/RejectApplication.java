package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command reject application
 */
@RequestHandler(name = "rejectApplication", method = POST, accessList = {Roles.ADMIN})
public class RejectApplication implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationService applicationService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: rejectApplication, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        int applicationId = Integer.parseInt(req.getParameter("applicationId"));
        String comment = req.getParameter("comment");

        try {
            if (applicationService.rejectApplication(applicationId, comment)){
                return URL_CMD_MANAGE_APPLICATIONS;
            } else{
                throw new ServiceException("Failed to change status for application");
            }

        } catch (ValidationException e){
            log.debug("Parameters violation: ",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_CMD_MANAGE_ONE_APPLICATION+"&applicationId="+applicationId;

        } catch (ServiceException e){
            log.error("Service error",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_ERROR_PAGE;
        }
    }
}
