package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.dao.mysql.MysqlDepartmentDAO;
import edu.university.entity.*;
import edu.university.exception.ServiceException;
import edu.university.service.ProgrammeService;
import edu.university.service.SubjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command save programme
 */
@RequestHandler(name = "saveProgramme", method = POST, accessList = {Roles.ADMIN})
public class SaveProgramme implements Command {
    @InjectByType
    private SubjectService subjectService;
    @InjectByType
    private ProgrammeService programmeService;
    @InjectLogger
    private Logger log;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: saveProgramme, method: POST");

        Map<String, String[]> params = req.getParameterMap();
        log.debug("Parameters: {}", params);

        HttpSession session = req.getSession();
        int programmeId = 0;
        if (req.getParameter("programmeId") != null) {
            programmeId = Integer.parseInt(req.getParameter("programmeId"));
        }
        int departmentId = Integer.parseInt(req.getParameter("departmentId"));

        try {
            Programme programme = extractProgramme(params);
            ProgrammeDetails programmeDetails = extractProgrammeDetails(params);
            List<SubjectRequirements> programmeRequirements = extractProgrammeRequirements(params);

            Programme savedProgramme = programmeService.saveProgramme(programme, programmeDetails, programmeRequirements);

            return URL_CMD_PROGRAMME + "&programmeId=" + savedProgramme.getId();

        } catch (NumberFormatException | NullPointerException e) {
            final String msg = "Received invalid data";
            log.debug(msg, e);
            session.setAttribute(ERROR_MESSAGE, msg);
            session.setAttribute("editProgrammeParams", params);
            return URL_CMD_EDIT_PROGRAMME + "&programmeId=" + programmeId + "&departmentId=" + departmentId;

        } catch (ValidationException e) {
            log.debug("Validation exception", e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            session.setAttribute("editProgrammeParams", params);
            return URL_CMD_EDIT_PROGRAMME + "&programmeId=" + programmeId + "&departmentId=" + departmentId;

        } catch (ServiceException e) {
            log.error("Failed to save programme", e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_ERROR_PAGE;
        }
    }

    private Programme extractProgramme(Map<String, String[]> params) throws ValidationException {
        return new Programme.Builder()
                .id(params.get("programmeId") == null ? 0 : Integer.parseInt(params.get("programmeId")[0]))
                .departmentId(Integer.parseInt(params.get("departmentId")[0]))
                .name(params.get("programmeName")[0])
                .shortDescription(params.get("shortDescription")[0])
                .build();
    }

    private ProgrammeDetails extractProgrammeDetails(Map<String, String[]> params) throws ValidationException {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            return new ProgrammeDetails.Builder()
                    .description(params.get("programmeDescription")[0])
                    .budgetPlaces(Integer.parseInt(params.get("budgetPlaces")[0]))
                    .totalPlaces(Integer.parseInt(params.get("totalPlaces")[0]))
                    .admissionsStartDate(dateFormat.parse(params.get("admissionsStartDate")[0]))
                    .admissionsEndDate(dateFormat.parse(params.get("admissionsEndDate")[0]))
                    .build();
        } catch (ParseException e) {
            final String msg = "Wrong date format";
            log.error(msg, e);
            throw new ValidationException(msg);
        }
    }

    private List<SubjectRequirements> extractProgrammeRequirements(Map<String, String[]> params) throws ServiceException, ValidationException {
        List<SubjectRequirements> requirements = new ArrayList<>();
        int id = 1;
        while (params.get("subjects_group_" + id) != null) {
            requirements.add(extractSubjectRequirements(params, id));
            id++;
        }
        return requirements;
    }

    private SubjectRequirements extractSubjectRequirements(Map<String, String[]> params, int groupId) throws ValidationException, ServiceException {

        List<Subject> subjects = extractSubjectsForGroup(params, groupId);
        return new SubjectRequirements.Builder()
                .subjects(subjects)
                .minScore(Integer.parseInt(params.get("score_group_" + groupId)[0]))
                .coefficient(Double.parseDouble(params.get("coef_group_" + groupId)[0]))
                .build();
    }


    private List<Subject> extractSubjectsForGroup(Map<String, String[]> params, int groupId) throws ServiceException, ValidationException {
        try {
            List<Subject> allSubjects = subjectService.getAllSubjects();
            return Arrays.stream(params.get("subjects_group_" + groupId))
                    .filter(Objects::nonNull)
                    .map(String::trim)
                    .filter(s -> !s.isEmpty())
                    .map(s -> getSubjectOrCreate(s, allSubjects))
                    .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            final String msg = "Failed to process data";
            log.debug(msg, e);
            throw new ValidationException(msg);
        }
    }

    private Subject getSubjectOrCreate(String subjectName, List<Subject> allSubjects) {
        try {
            Optional<Subject> optionalSubject = allSubjects.stream()
                    .filter(subject -> subject.getName().equalsIgnoreCase(subjectName))
                    .findAny();

            if (optionalSubject.isPresent()) {
                return optionalSubject.get();
            }

            return subjectService.saveSubject(subjectName);

        } catch (ServiceException e) {
            final String msg = "Failed to save subject";
            log.error(msg, e);
            throw new IllegalArgumentException(msg);
        }
    }
}
