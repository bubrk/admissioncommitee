package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.exception.ServiceException;
import edu.university.service.UserBlacklistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.URL_CMD_MANAGE_ONE_USER;
import static edu.university.controller.PagePath.URL_ERROR_PAGE;
import static edu.university.controller.command.HttpMethod.POST;


@RequestHandler(name = "unblockUser", method = POST, accessList = {Roles.ADMIN})
public class UnblockUser implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    UserBlacklistService blacklistService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: unblockUser, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        int userId = Integer.parseInt(req.getParameter("userId"));

        try {
            if (blacklistService.removeUserFromBlackList(userId)){
                return URL_CMD_MANAGE_ONE_USER+"&userId="+userId;
            } else{
                throw new ServiceException("Failed to remove user from blacklist");
            }

        } catch (ServiceException e){
            log.error("Service error",e);
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_ERROR_PAGE;
        }
    }
}
