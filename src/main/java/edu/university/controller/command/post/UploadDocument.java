package edu.university.controller.command.post;

import edu.university.config.Config;
import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.config.InjectProperty;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Document;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import edu.university.exception.ValidationException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.UUID;

import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command upload document
 */
@RequestHandler(name = "uploadDocument", method = POST, accessList = {Roles.USER, Roles.ADMIN})
public class UploadDocument implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private UserService userService;
    @InjectProperty("uploads.location")
    private String uploadsLocation;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: uploadDocument, method: POST");
        log.debug("Parameters: {}",req.getParameterMap());

        HttpSession session = req.getSession();
        User user =(User) session.getAttribute("loggedUser");
        try {
            Part filePart = req.getPart("fileName");
            String fileName = Paths.get(filePart.getSubmittedFileName())
                    .getFileName().toString()
                    .trim();

            String nameOnServer = UUID.randomUUID().toString();
            String path = uploadsLocation + File.separator + nameOnServer;

            log.debug("Uploading document: {}", fileName);
            log.debug("path: {}",path);
            filePart.write(path);

            Document doc = userService.saveUserDocument(user.getId(),fileName,nameOnServer);

            log.debug("Uploaded successfully: {}",doc);
            session.removeAttribute("userDocuments");

            return URL_CMD_EDIT_INFO;

        } catch (ValidationException e){;
            log.error("Validation exception: ", e);
            session.setAttribute("errorMessage", e.getMessage());
            return URL_CMD_EDIT_INFO;

        } catch (ServletException | IOException | ServiceException e) {
            String msg = "Failed to upload document";
            log.error(msg, e);
            session.setAttribute("errorMessage", msg);
            return URL_ERROR_PAGE;
        }
    }
}
