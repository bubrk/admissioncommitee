package edu.university.controller.command.post;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.controller.command.Command;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.User;
import edu.university.exception.ServiceException;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.university.exception.ValidationException;

import java.util.Date;

import static edu.university.config.CommonStrings.ERROR_MESSAGE;
import static edu.university.controller.PagePath.*;
import static edu.university.controller.command.HttpMethod.POST;

/**
 * Command validate user
 */
@RequestHandler(name = "validate", method = POST)
public class ValidatePost implements Command {
    @InjectLogger
    private Logger log;
    @InjectByType
    private UserService userService;

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        log.debug("Command: validate, method: POST");
        log.debug("Parameters: {}", req.getParameterMap());

        HttpSession session = req.getSession();
        String userCode = req.getParameter("userCode").trim();
        Date now = new Date();

        String code = (String) session.getAttribute("validationCode");
        Date validUntil = (Date) session.getAttribute("validationValidUntil");

        try {
            if (!code.equals(userCode)) {
                throw new ValidationException("Code is wrong, please try again");
            }

            if (now.compareTo(validUntil) > 0) {
                throw new ValidationException("Unfortunately code has already expired, please try again");
            }
            log.debug("Successful verification");
            User newUser = (User) session.getAttribute("newUser");
            session.removeAttribute("newUser");

            User savedUser = userService.saveUser(newUser);
            session.setAttribute("loggedUser", savedUser);
            session.setAttribute("newUser", true);

            return URL_CMD_EDIT_SCHOOL;

        } catch (ValidationException e) {
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_CMD_VALIDATE;

        } catch (ServiceException e) {
            session.setAttribute(ERROR_MESSAGE, e.getMessage());
            return URL_ERROR_PAGE;
        }


//        if (code.equals(userCode)) {
//            if (now.compareTo(validUntil) < 0) {
//                log.debug("Successful verification");
//                try {
//                    Map<String, String> regParams = (Map<String, String>) session.getAttribute("registrationParams");
//                    log.debug("Registration parameters: {}", regParams);
//
//                    assert regParams != null;
//                    User savedUser = saveUser(regParams);
//                    session.setAttribute("loggedUser", savedUser);
//                    session.setAttribute("newUser", true);
//
//                    return URL_CMD_EDIT_SCHOOL;
//
//                } catch (IllegalArgumentException e) {
//                    session.setAttribute("errorMessage", "Illegal arguments");
//                    //TODO Test this
//                    return URL_ERROR_PAGE;
//                } catch (ServiceException e) {
//                    session.setAttribute(REGISTRATION_ERROR, e.getMessage());
//                    return URL_CMD_REGISTER;
//                }
//            }
//            session.setAttribute(VALIDATION_ERROR, "Unfortunately code has already expired, please try again");
//        } else {
//            session.setAttribute(VALIDATION_ERROR, "Code is wrong, please try again");
//        }
//        log.debug("Validation unsuccessful");
//        return URL_CMD_VALIDATE;
//    }
    }
}
