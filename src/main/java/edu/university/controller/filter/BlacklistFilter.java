package edu.university.controller.filter;

import edu.university.config.ObjectFactory;
import edu.university.controller.CommandContainer;
import edu.university.controller.PagePath;
import edu.university.controller.command.HttpMethod;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.service.UserBlacklistService;
import edu.university.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Filter checks if logged user is in the blacklist and if so, then redirects to <code>PAGE_BLACKLISTED</code> page
 */
@WebFilter(urlPatterns = {
        "/*"
})
public class BlacklistFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(BlacklistFilter.class);
    private UserBlacklistService blacklistService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.blacklistService = ObjectFactory.getInstance()
                .getObject(UserBlacklistService.class);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain next) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        User loggedUser = (User) req.getSession()
                .getAttribute("loggedUser");

        if (loggedUser == null || isUserNotInTheBlacklist(loggedUser)) {
            log.trace("Filter in action, user is decent");
            next.doFilter(request, response);

        } else{
            log.debug("Filter in action, user is in the black list. User id = {}",loggedUser.getId());
            String comment = blacklistService.getBlacklistComment(loggedUser.getId());
            request.setAttribute("commentMessage",comment);
            request.getRequestDispatcher(PagePath.PAGE_BLACKLISTED).forward(request, response);
        }
    }

    private boolean isUserNotInTheBlacklist(User loggedUser) {
        return !blacklistService.isUserInBlackList(loggedUser.getId());
    }

    @Override
    public void destroy() {
        //no need to use
    }
}