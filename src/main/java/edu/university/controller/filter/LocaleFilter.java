package edu.university.controller.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Filter checks for parameter <code>LANGUAGE</code>, if received - changes setting for translation,
 * save them to session and create a cookie
 */
@WebFilter(urlPatterns = {"/*"})
public class LocaleFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(LocaleFilter.class);

    private static final String LANGUAGE = "lang";

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain next)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;

        String language = req.getParameter(LANGUAGE);

        //if locale was changed - save it to session and create a cookie
        if (language != null && !language.isEmpty()) {
            setLanguage((HttpServletResponse) response, req, language);
        }

        //if locale is not in the session yet, try to load it from cookie
        if (req.getSession().getAttribute(LANGUAGE) == null) {
            readLanguageFromCookie(request)
                    .ifPresent(lang -> req.setAttribute(LANGUAGE, lang));
        }

        next.doFilter(request, response);
    }

    private void setLanguage(HttpServletResponse resp, HttpServletRequest req, String newLanguage) {
        req.getSession().setAttribute(LANGUAGE, newLanguage);
        Cookie languageCookie = new Cookie(LANGUAGE, newLanguage);
        resp.addCookie(languageCookie);
        log.debug("Language has been changed to {}", newLanguage);
    }

    private Optional<String> readLanguageFromCookie(ServletRequest req) {
        Cookie[] cookies = ((HttpServletRequest) req).getCookies();
        if (cookies == null || cookies.length == 0) {
            return Optional.empty();
        }

        return Arrays.stream(cookies)
                .filter(c -> LANGUAGE.equals(c.getName()))
                .map(Cookie::getValue)
                .findAny();
    }

    public void destroy() {
    }

    public void init(FilterConfig config) throws ServletException {
    }

}
