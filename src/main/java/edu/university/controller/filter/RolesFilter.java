package edu.university.controller.filter;

import edu.university.controller.CommandContainer;
import edu.university.controller.PagePath;
import edu.university.controller.command.Command;
import edu.university.controller.command.HttpMethod;
import edu.university.controller.command.RequestHandler;
import edu.university.entity.Roles;
import edu.university.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static edu.university.controller.PagePath.PAGE_FORBIDDEN;
import static edu.university.controller.PagePath.PAGE_LOGIN;

/**
 * Filter checks if user's role allows him to have access for requested command
 *
 * @see Roles
 * @see Command
 * @see RequestHandler
 */
@WebFilter(urlPatterns = {
        "/*"
})
public class RolesFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(RolesFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //no need to use
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain next) throws IOException, ServletException {
        String cmd = request.getParameter("cmd");

        //if access is granted - proceed
        if (cmd == null || isAccessGranted(request, cmd)) {
            log.trace("Filter in action, access permitted");
            next.doFilter(request, response);
            return;
        }

        //access not granted, but maybe user is not logged in
        if (request.getAttribute("loggedUser") == null) {
            log.trace("Filter in action, access denied - user is not logged in");
            request.getRequestDispatcher(PAGE_LOGIN)
                    .forward(request, response);
            return;
        }

        //access denied
        log.debug("Filter in action, access denied");
        request.getRequestDispatcher(PAGE_FORBIDDEN)
                .forward(request, response);
    }

    @Override
    public void destroy() {
        //no need to use
    }

    private boolean isAccessGranted(ServletRequest request, String cmd) {
        HttpServletRequest req = (HttpServletRequest) request;

        Optional<Roles[]> optionalRoles = CommandContainer.getAccessList(HttpMethod.valueOf(req.getMethod()), cmd);

        if (!optionalRoles.isPresent()) {
            return true; //no roles in description, access allowed
        }

        Roles[] roles = optionalRoles.get();
        if (roles.length == 0) {
            return true; //no roles in description, access allowed
        }

        User loggedUser = (User) req.getSession()
                .getAttribute("loggedUser");

        if (loggedUser == null) {
            return false; //user is not logged in, access denied
        }

        return roleMatches(loggedUser.getRole(), roles); // access allowed if roles are matching
    }

    private boolean roleMatches(Roles userRole, Roles[] roles) {
        for (Roles role : roles) {
            if (userRole.equals(role)) {
                return true;
            }
        }
        return false;
    }
}