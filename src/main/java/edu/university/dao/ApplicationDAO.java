package edu.university.dao;

import edu.university.entity.*;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.DAOException;
import edu.university.exception.ValidationException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Interface for accessing users applications for educational programmes
 */
public interface ApplicationDAO {
    /**
     * Creates records associated with given application in database
     * @param application user's application
     * @param scores user's scores in format <code>Map<Subject, Score></code>
     * @return <code>Optional<Application></code> with generated id
     * @throws DAOException if error occurred while working with database
     */
    public Optional<Application> createApplication(Application application, Map<Subject, Score> scores) throws DAOException;

    /**
     * Searches for user's applications
     * @param userId id of the user
     * @return List of <code>ApplicationSearchResult</code> or empty list if nothing found
     * @throws DAOException if error occurred while working with database
     */
    public List<ApplicationSearchResult> getUsersApplications(int userId) throws DAOException;

    /**
     * Returns application by given id
     * @param applicationId id of the application
     * @return application with linked data in <code>Optional<ApplicationSearchResult></code>
     * @throws DAOException if error occurred while working with database
     */
    public Optional<ApplicationSearchResult> getApplication(int applicationId) throws DAOException;

    /**
     * Returns user scores in application with given id
     * @param applicationId id of the application
     * @return map of subjects with scores <code>Map<Subject,Score></code>
     * @throws DAOException if error occurred while working with database
     */
    public Map<Subject,Score> getApplicationScores(int applicationId) throws DAOException;

    /**
     * Returns all stored applications
     * @return List of <code>ApplicationSearchResult</code> or empty list if nothing found
     * @throws DAOException if error occurred while working with database
     */
    public List<ApplicationSearchResult> getAllApplications() throws DAOException;

    /**
     * Creates record with status for application with given id
     * @param applicationId id of the application
     * @param status current status of the application
     * @param comment comment for current status
     * @return <code>true</code> if saved successfully
     * @throws DAOException if error occurred while working with database
     * @see ApplicationStatus
     */
    boolean changeApplicationStatus(int applicationId, ApplicationStatus.Statuses status, String comment) throws DAOException;

    /**
     * Returns all the accepted applications for programme with given id
     * @param programmeId id of the programme
     * @return List of applications or empty list if nothing found
     * @throws DAOException if error occurred while working with database
     */
    List<Application> getAcceptedApplicationsForProgramme(int programmeId) throws DAOException;
}
