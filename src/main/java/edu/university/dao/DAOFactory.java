package edu.university.dao;


import edu.university.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public abstract class DAOFactory {

    private static final Logger log = LoggerFactory.getLogger(DAOFactory.class);

    private static DAOFactory instance;

    protected DAOFactory() {
    }

    public static synchronized DAOFactory getInstance() {
        if (instance == null) {
            try {
                String daoFactoryImp = Config.getProperty(Config.DAO_FACTORY);

                if (daoFactoryImp == null || daoFactoryImp.isEmpty()){
                    String msg = "daoFactoryImpl can't be null, check app settings";
                    log.error(msg);
                    throw new IllegalStateException(msg);
                }

                instance = (DAOFactory)Class.forName(daoFactoryImp)
                        .getDeclaredConstructor()
                        .newInstance();

            } catch (ReflectiveOperationException e) {
                log.error("Cannot instantiate DAOFactory",e);
                throw new IllegalStateException("Cannot instantiate DAOFactory");
            }
        }
        return instance;
    }

    public abstract UserDAO getUserDAO();

    public abstract DepartmentDAO getDepartmentDAO();

    public abstract ProgrammeDAO getProgrammeDAO();

    public abstract SubjectDAO getSubjectDAO();

    public abstract ApplicationDAO getApplicationDAO();

    public abstract ReportDAO getReportsDAO();
}
