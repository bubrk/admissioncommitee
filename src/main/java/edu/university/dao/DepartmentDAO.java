package edu.university.dao;

import edu.university.entity.Department;
import edu.university.exception.DAOException;

import java.util.List;
import java.util.Optional;

/**
 * Interface to access data that describes departments
 */
public interface DepartmentDAO {
    /**
     * Returns department by given id
     * @param departmentId id of the department
     * @return <code>Optional.of(Department)</code>
     * @throws DAOException if an error occurred while working with database
     * @see Department
     */
    public Optional<Department> getDepartment(int departmentId) throws DAOException;

    /**
     * Returns all departments in database
     * @return List of {@link Department} or empty list if nothing found
     * @throws DAOException if an error occurred while working with database
     * @see Department
     */
    public List<Department> getAllDepartments() throws DAOException;

    /**
     * Returns nested departments inside department with given id
     * @param parentId id of department containing nested departments
     * @return List of {@link Department} or empty list if nothing found
     * @throws DAOException if an error occurred while working with database
     * @see Department
     */
    public List<Department> getAllSubDepartments(int parentId) throws DAOException;

    /**
     * Creates record of new department in database
     * @param department given new department
     * @return <code>Optional.of(Department)</code> with generated id
     * @throws DAOException if an error occurred while working with database
     * @see Department
     */
    public Optional<Department> createDepartment(Department department) throws DAOException;

    /**
     * Updates information about department in the database
     * @param department updated department
     * @return true if updated successfully
     * @throws DAOException if an error occurred while working with database
     * @see Department
     */
    public boolean updateDepartment(Department department) throws DAOException;

    /**
     * Delete all the information associated with department with given id. {@link DAOException} will be thrown if try
     * to delete department containing educational programmes or other departments
     * @param departmentId id of the department
     * @return true if deleted successfully
     * @throws DAOException if an error occurred while working with database
     * @see Department
     */
    public boolean deleteDepartment(int departmentId) throws DAOException;
}
