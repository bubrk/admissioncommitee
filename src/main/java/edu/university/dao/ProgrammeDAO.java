package edu.university.dao;

import edu.university.entity.*;
import edu.university.entity.dto.Tuple3;
import edu.university.exception.DAOException;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Interface to access data describing educational programmes
 */
public interface ProgrammeDAO {

    /**
     * Searches programme by its <code>id</code> value.
     * @param programmeId <code>id</code> value of the programme
     * @return <code>Optional.of(Programme)</code>
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     */
    public Optional<Programme> getProgrammeById(int programmeId) throws DAOException;

    /**
     * Returns detailed information about programme with given id.
     * @param programmeId <code>id</code> value of the programme
     * @return <code>Optional.of(ProgrammeDetails)</code>
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see ProgrammeDetails
     */
    public Optional<ProgrammeDetails> getProgrammeDetails(int programmeId) throws DAOException;

    /**
     * Searches requirements for programme with given id.
     * @param programmeId <code>id</code> value of the programme
     * @return List of {@link SubjectRequirements} or empty list if nothing found
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see SubjectRequirements
     */
    public List<SubjectRequirements> getProgrammeRequirements(int programmeId) throws DAOException;

    /**
     * Searches all programmes in given department
     * @param departmentId <code>id</code> value of the department
     * @return List of {@link Programme} or empty list if nothing found
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see Department
     */
    public List<Programme> getProgrammesInDepartment(int departmentId) throws DAOException;


    /**
     * Searches all programmes in database
     * @return List of {@link Programme} or empty list if nothing found
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     */
    public List<Programme> getAllProgrammes() throws DAOException;

    /**
     * Searches and returns all programmes with details in database
     * @return Map of <code>Entry<{@link Programme},{@link ProgrammeDetails}></code> or empty map if nothing found
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see ProgrammeDetails
     */
    public Map<Programme,ProgrammeDetails> getAllProgrammesWithDetails() throws DAOException;

    /**
     * Searches for programmes filtering by programme name and returns them with details
     * @param name full or partial name of the programme
     * @return Map of <code>Entry<{@link Programme},{@link ProgrammeDetails}></code> or empty map if nothing found
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see ProgrammeDetails
     */
    public Map<Programme,ProgrammeDetails> getAllProgrammesWithDetailsFiltering(String name) throws DAOException;

    /**
     * Method erases programme with given id from database.
     * Attention! All the information that linked with given programme (including programme details, requirement and
     * applications) will be also removed from database
     * @param programmeId <code>id</code> value of the programme
     * @return <code>true</code> if deleted successfully
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     */
    public boolean deleteProgramme(int programmeId) throws DAOException;

    /**
     * Method saves given programme linked with programme details and requirements in the database.
     * @param programme given programme
     * @param programmeDetails given details of the programme
     * @param programmeRequirements given requirements of the programme
     * @return <code>Optional<Programme></code> with valid programme id if saved successfully
     * @throws ValidationException if any of the parameters doesn't pass validation
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see ProgrammeDetails
     * @see SubjectRequirements
     */
    public Optional<Programme> createProgramme(Programme programme, ProgrammeDetails programmeDetails, List<SubjectRequirements> programmeRequirements) throws DAOException;

    /**
     * Method updates given programme together with programme details and requirements in the database.
     * @param programme given programme
     * @param programmeDetails given details of the programme
     * @param programmeRequirements given requirements of the programme
     * @return <code>true</code> if updated successfully
     * @throws ValidationException if any of the parameters doesn't pass validation
     * @throws DAOException if any error occurred while working with database
     * @see Programme
     * @see ProgrammeDetails
     * @see SubjectRequirements
     */
    public boolean updateProgramme(Programme programme, ProgrammeDetails programmeDetails, List<SubjectRequirements> programmeRequirements) throws DAOException;

}
