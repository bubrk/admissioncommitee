package edu.university.dao;

import edu.university.entity.Document;
import edu.university.entity.Programme;
import edu.university.exception.DAOException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
/**
 * Interface to access reports for educational programmes
 */
public interface ReportDAO {
    /**
     * Returns report document for given id
     * @param reportId id of the report
     * @return <code>Optional.of(Document)</code>
     * @throws DAOException if an error occurred while working with database
     * @see Document
     */
    Optional<Document> getReportById(int reportId) throws DAOException;

    /**
     * Searches for reports for programme with given id
     * @param programmeId id of the programme
     * @return List of {@link Document} or empty list if nothing found
     * @throws DAOException if an error occurred while working with database
     */
    List<Document> getReportsByProgrammeId(int programmeId) throws DAOException;

    /**
     * Searches for all reports in database
     * @return Map of entries with {@link Programme} as a key and {@link Document} as a value
     * @throws DAOException if an error occurred while working with database
     */
    Map<Programme,Document> getAllReports() throws DAOException;

    /**
     * Creates record with information about report document in database
     * @param programmeId id of the programme
     * @param reportName name of the document
     * @param fileName unique name of the file on disk
     * @return <code>Optional.of(Document)</code> if saved successfully
     * @throws DAOException if an error occurred while working with database
     */
    Optional<Document> saveReport(int programmeId, String reportName, String fileName) throws DAOException;
}
