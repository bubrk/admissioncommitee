package edu.university.dao;

import edu.university.entity.Subject;
import edu.university.exception.DAOException;

import java.util.List;
import java.util.Optional;

/**
 * Interface to access subjects
 */
public interface SubjectDAO {
    /**
     * Returns all saved subjects
     * @return List of {@link Subject} or empty list if nothing found
     * @throws DAOException if an error occurred while working with database
     */
    public List<Subject> getAllSubjects() throws DAOException;

    /**
     * Returns {@link Subject} by given id
     * @param subjectId id of the subject
     * @return <code>Optional.of(Subject)</code>
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<Subject> getSubjectById(int subjectId) throws DAOException;

    /**
     * Saves new subject to database
     * @param name name of the subject
     * @return <code>Optional.of(Subject)</code> with generated id
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<Subject> createSubject(String name) throws DAOException;

    /**
     * Updates subject in database
     * @param subject {@link Subject} with updated name
     * @return true if updated successfully
     * @throws DAOException if an error occurred while working with database
     */
    public boolean updateSubject(Subject subject) throws DAOException;

    /**
     * Delete subject with given id from the database
     * @param subjectId id of the subject
     * @return true if deleted successfully
     * @throws DAOException if an error occurred while working with database
     */
    public boolean deleteSubject(int subjectId) throws DAOException;
}
