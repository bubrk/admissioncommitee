package edu.university.dao;

import edu.university.entity.Document;
import edu.university.entity.User;
import edu.university.entity.UserDetails;
import edu.university.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface UserDAO {

    /**
     * Searches {@link User} by given id
     * @param userId id of the user
     * @return <code>Optional.of(User)</code>
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<User> getUser(int userId) throws DAOException;

    /**
     * Searches user by email address
     * @param userEmail String with an email address
     * @return <code>Optional.of(User)</code>
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<User> getUserByEmail(String userEmail) throws DAOException;

    /**
     * Creates record with new user in database
     * @param user new user
     * @return <code>Optional.of(User)</code> with generated id
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<User> createUser(User user) throws DAOException;

    /**
     * Returns detailed information about user with given id
     * @param userId id of the user
     * @return <code>Optional.of(UserDetails)</code>
     * @throws DAOException if an error occurred while working with database
     * @see UserDetails
     */
    public Optional<UserDetails> getUserDetails(int userId) throws DAOException;

    /**
     * Searches all users filtering by name, email
     * @param email String with email address of the user
     * @param firstName first name of the user
     * @param lastName last name of the user
     * @return List of {@link User} or empty list if nothing found
     * @throws DAOException if an error occurred while working with database
     */
    List<User> getAllUsersFiltering(String email, String firstName, String lastName) throws DAOException;

    /**
     * Creates record with detailed information about the user
     * @param userDetails detailed information
     * @return true if saved successfully
     * @throws DAOException if an error occurred while working with database
     */
    public boolean saveUserDetails(UserDetails userDetails) throws DAOException;

    /**
     * Returns list of uploaded documents by user with given id
     * @param userId id of the user
     * @return List of {@link Document} or empty list if nothing found
     * @throws DAOException if an error occurred while working with database
     */
    public List<Document> getUserDocuments(int userId) throws DAOException;

    /**
     * Returns name of the uploaded file by given document id
     * @param documentId id of the document
     * @return <code>Optional.of(String)</code> name of the file on disk
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<String> getUserDocumentFile(int documentId) throws DAOException;

    /**
     * Returns uploaded {@link Document} by given id
     * @param documentId id of the document
     * @return <code>Optional.of(Document)</code>
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<Document> getUserDocument(int documentId) throws DAOException;

    /**
     * Creates record about uploaded document in database
     * @param userId id of the user
     * @param name name of the document
     * @param fileName unique name of the file on disk
     * @return <code>Optional.of(Document)</code> with generated id
     * @throws DAOException if an error occurred while working with database
     */
    public Optional<Document> saveUserDocument(int userId, String name, String fileName) throws DAOException;

    /**
     * Deletes record about uploaded document from the database
     * @param documentId id of the document
     * @return true if deleted successfully
     * @throws DAOException if an error occurred while working with database
     */
    public boolean deleteUserDocument(int documentId) throws DAOException;

    /**
     * Creates record in the blacklist about the user with given id
     * @param userId id of the user
     * @param comment String with comment
     * @return true if saved successfully
     * @throws DAOException if an error occurred while working with database
     */
    public boolean blockUser(int userId, String comment) throws DAOException;

    /**
     * Remove user with given id from the blacklist
     * @param userId id of the user
     * @return true if removed successfully
     * @throws DAOException if an error occurred while working with database
     */
    public boolean unblockUser(int userId) throws DAOException;

    /**
     * Checks if user with given id is in the blacklist
     * @param userId id of the user
     * @return true if userId found in the blacklist
     * @throws DAOException if an error occurred while working with database
     */
    public boolean isUserBlocked(int userId) throws DAOException;

    /**
     * Returns list of users from the blacklist
     * @return List of {@User} which are currently in the blacklist
     * @throws DAOException if an error occurred while working with database
     */
    List<User> getBlackListedUsers() throws DAOException;

    /**
     * Returns String with comment for the blacklisted user
     * @param userId id of the user
     * @return <code>Optional.of(String)</code> with comment
     * @throws DAOException if an error occurred while working with database
     */
    Optional<String> getBlacklistComment(int userId) throws DAOException;
}
