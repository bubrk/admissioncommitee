package edu.university.dao.mysql;

import edu.university.config.ObjectFactory;
import edu.university.exception.DAOException;

import java.sql.Connection;

/**
 * ConnectionBuilder interface
 */
interface ConnectionBuilder {
    public Connection getConnection() throws DAOException;
}
