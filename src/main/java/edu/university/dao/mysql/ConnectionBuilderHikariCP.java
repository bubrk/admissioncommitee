package edu.university.dao.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.university.config.Config;

import edu.university.config.InjectLogger;
import edu.university.config.InjectProperty;
import edu.university.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.sql.Connection;
import java.sql.SQLException;

import static edu.university.config.Config.*;


/**
 * Implementation of {@link ConnectionBuilder} interface <br>
 * This connection builder based on <a href="https://github.com/brettwooldridge/HikariCP">HikariCP JDBC connection pool project</a>
 *
 * @see ConnectionBuilderHikariCP#getConnection()
 * @see ConnectionBuilder
 * @see Config
 * @see DAOException
 */
public class ConnectionBuilderHikariCP implements ConnectionBuilder {
    @InjectLogger
    private static Logger log;
    @InjectProperty(DB_CONNECTION_URL)
    private static String url;
    @InjectProperty(DB_USER_NAME)
    private static String userName;
    @InjectProperty(DB_CONNECTION_PASSWORD)
    private static String password;
    @InjectProperty(DB_CONNECTION_DRIVER)
    private static String driver;
    private static HikariDataSource ds;


    public Connection getConnection() throws DAOException {
        try {
            if (ds == null) {
                ds = getDataSource();
            }

            Connection connection = ds.getConnection();

            log.trace("Got connection: {}", connection);

            return connection;

        } catch (SQLException e) {
            log.error("Can't get connection to DB", e);
            throw new DAOException("Failed to get connection with database");
        }
    }

    private static HikariDataSource getDataSource() {
        log.debug("Data source is being configured");

        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(url);
        config.setUsername(userName);
        config.setPassword(password);
        config.setDriverClassName(driver);

        return new HikariDataSource(config);
    }
}
