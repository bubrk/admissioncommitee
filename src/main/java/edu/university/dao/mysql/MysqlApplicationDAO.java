package edu.university.dao.mysql;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.ApplicationDAO;
import edu.university.entity.*;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.DAOException;
import org.slf4j.Logger;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MysqlApplicationDAO implements ApplicationDAO {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ConnectionBuilder connectionBuilder;

    private static final String FIELD_PROGRAMME_ID = "programme_id";
    private static final String FIELD_APPLICATION_ID = "application_id";
    private static final String FIELD_USER_ID = "user_id";
    private static final String FIND_USERS_APPLICATIONS = "SELECT ap.application_id, ap.user_id, ap.programme_id,\n" +
            "       p.name as programme_name, p.department_id, p.short_description,\n" +
            "       sff.status_flow_id, sff.status_id,sff.comment,sff.create_datetime,\n" +
            "       s.name as status_name\n" +
            "FROM applications as ap\n" +
            "         JOIN programmes p on ap.programme_id = p.programme_id\n" +
            "         JOIN(select sf.status_flow_id, sf.application_id, sf.status_id, sf.comment, sf.create_datetime\n" +
            "              from (select status_flow_id, status_id,application_id,comment,create_datetime,\n" +
            "                           max(create_datetime) over (partition by application_id) as latest\n" +
            "                    from status_flow) sf\n" +
            "              where sf.create_datetime = sf.latest) sff on ap.application_id = sff.application_id\n" +
            "         JOIN statuses s on sff.status_id = s.status_id\n" +
            "WHERE user_id = ?\n" +
            "ORDER BY application_id DESC";
    private static final String FIELD_PROGRAMME_NAME = "programme_name";
    private static final String FIELD_DEPARTMENT_ID = "department_id";
    private static final String FIELD_PROGRAMME_SHORT_DESCRIPTION = "short_description";
    private static final String FIELD_DATE_OF_CREATION = "create_datetime";
    private static final String FIELD_STATUS_FLOW_ID = "status_flow_id";
    private static final String FIELD_STATUS_NAME = "status_name";
    private static final String FIELD_STATUS_COMMENT = "comment";
    private static final String FIELD_USER_EMAIL = "email";
    private static final String FIELD_USER_FIRST_NAME = "first_name";
    private static final String FIELD_USER_LAST_NAME = "last_name";
    private static final String FIELD_USER_MIDDLE_NAME = "middle_name";
    private static final String FIELD_SUBJECT_ID = "subject_id";
    private static final String FIELD_SUBJECT_NAME = "subject_name";
    private static final String FILED_SUBJECT_SCORE = "score";
    private static final String SQL_CREATE_APPLICATION = "INSERT INTO applications VALUE (DEFAULT, ?, ?)";
    private static final String SQL_CREATE_STATUS_FLOW_RECORD = "INSERT INTO status_flow VALUE (DEFAULT,?,(SELECT status_id FROM statuses WHERE name=?),?,?)";
    private static final String SQL_SAVE_SCORE_PREF = "INSERT INTO application_subject_scores VALUES ";
    private static final String SQL_SAVE_SCORE_ARGS = "(?, ?, ?)";
    private static final String SQL_FIND_APPLICATION = "SELECT ap.application_id, ap.user_id, ap.programme_id,\n" +
            "       u.first_name, u.last_name, u.middle_name, u.email,\n" +
            "       p.department_id, p.name as programme_name, p.short_description,\n" +
            "       sf.status_flow_id, sf.status_id, sf.comment, sf.create_datetime,\n" +
            "       s.name as status_name\n" +
            "FROM applications AS ap\n" +
            "JOIN users u on ap.user_id = u.user_id\n" +
            "JOIN programmes p on p.programme_id = ap.programme_id\n" +
            "JOIN status_flow sf on ap.application_id = sf.application_id\n" +
            "JOIN statuses s on sf.status_id = s.status_id\n" +
            "WHERE ap.application_id=?\n" +
            "ORDER BY create_datetime DESC";
    private static final String SQL_FIND_ALL_APPLICATIONS = "SELECT ap.application_id, ap.user_id, ap.programme_id,\n" +
            "                   u.email,u.first_name,u.last_name,u.middle_name,\n" +
            "                   p.name as programme_name, p.department_id, p.short_description,\n" +
            "                   sff.status_flow_id, sff.status_id,sff.comment,sff.create_datetime,\n" +
            "                   s.name as status_name\n" +
            "            FROM applications as ap\n" +
            "                     JOIN users u on ap.user_id = u.user_id\n" +
            "                     JOIN programmes p on ap.programme_id = p.programme_id\n" +
            "                     JOIN(select sf.status_flow_id, sf.application_id, sf.status_id, sf.comment, sf.create_datetime\n" +
            "                          from (select status_flow_id, status_id,application_id,comment,create_datetime,\n" +
            "                                       max(create_datetime) over (partition by application_id) as latest\n" +
            "                                from status_flow) sf\n" +
            "                          where sf.create_datetime = sf.latest) sff on ap.application_id = sff.application_id\n" +
            "                     JOIN statuses s on sff.status_id = s.status_id\n" +
            "            ORDER BY create_datetime DESC";
    private static final String SQL_FIND_APPLICATION_SCORES = "SELECT ass.application_id, ass.subject_id, s.name as subject_name, ass.score FROM application_subject_scores AS ass\n" +
            "JOIN subjects AS s ON ass.subject_id = s.subject_id\n" +
            "WHERE ass.application_id=?";
    private static final String SQL_FIND_ACCEPTED_APPLICATIONS_FOR_PROGRAMME = "select application_id, ap.user_id, ap.programme_id, email, first_name, last_name, middle_name, applied_date, accepted_date\n" +
            "from applications as ap\n" +
            "         join (select application_id as id, create_datetime as accepted_date\n" +
            "               from status_flow\n" +
            "                        join statuses s on status_flow.status_id = s.status_id\n" +
            "               where name = 'ACCEPTED') as accepted_applications on application_id = accepted_applications.id\n" +
            "         join (select application_id as id, create_datetime as applied_date\n" +
            "               from status_flow\n" +
            "                        join statuses s on status_flow.status_id = s.status_id\n" +
            "               where name = 'NEW') as applied_applications on application_id = applied_applications.id\n" +
            "join users u on ap.user_id = u.user_id\n" +
            "join programme_details pd on ap.programme_id = pd.programme_id\n" +
            "where ap.programme_id = ?\n" +
            "and applied_date > pd.admissions_start_date\n" +
            "and applied_date < pd.admissions_end_date";


    @Override
    public Optional<Application> createApplication(Application application, Map<Subject, Score> scores) throws DAOException {
        if (application == null || scores == null || scores.isEmpty()) {
            return Optional.empty();
        }

        Connection conn = null;

        try {
            conn = connectionBuilder.getConnection();
            conn.setAutoCommit(false);

            int applicationId = createApplication(application.getUserId(), application.getProgrammeId(), conn)
                    .orElseThrow(() -> new SQLException("Unable to save application"));

            int statusFlowId = createStatusFlowRecord(applicationId, ApplicationStatus.Statuses.NEW, "", conn);

            if (statusFlowId <= 0 || !areScoresSaved(applicationId, scores, conn)) {
                throw new SQLException("Failed to save scores for application");
            }

            conn.commit();

            return Optional.of(new Application.Builder()
                    .id(applicationId)
                    .userId(application.getUserId())
                    .programmeId(application.getProgrammeId())
                    .scores(application.getScores())
                    .build());

        } catch (SQLException e) {
            String msg = "Failed to create new application in database";
            log.error(msg, e);

            rollBack(conn);

            throw new DAOException(msg);
        } finally {
            closeConnection(conn);
        }
    }

    @Override
    public List<ApplicationSearchResult> getUsersApplications(int userId) throws DAOException {
        List<ApplicationSearchResult> result = new ArrayList<>();
        if (userId <= 0) {
            return result;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(FIND_USERS_APPLICATIONS)) {

            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ApplicationSearchResult asr = new ApplicationSearchResult();
                asr.setApplication(extractApplication(rs));
                asr.setCurrentStatus(extractStatus(rs));
                asr.setProgramme(extractProgramme(rs));
                result.add(asr);
            }
            rs.close();
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find the user applications, userId = " + userId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Optional<ApplicationSearchResult> getApplication(int applicationId) throws DAOException {
        if (applicationId <= 0) {
            return Optional.empty();
        }
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_APPLICATION)) {

            stmt.setInt(1, applicationId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                ApplicationSearchResult asr = new ApplicationSearchResult();
                asr.setApplication(extractApplication(rs));
                asr.setCurrentStatus(extractStatus(rs));
                asr.setProgramme(extractProgramme(rs));
                asr.setUser(extractUser(rs));
                asr.setStatusFlow(extractStatusFlow(rs));
                return Optional.of(asr);
            }
            return Optional.empty();

        } catch (SQLException e) {
            final String msg = "Failed to find application id = " + applicationId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }


    @Override
    public Map<Subject, Score> getApplicationScores(int applicationId) throws DAOException {
        Map<Subject, Score> result = new HashMap<>();
        if (applicationId <= 0) {
            return result;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_APPLICATION_SCORES)) {

            stmt.setInt(1, applicationId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.put(extractSubject(rs), extractScore(rs));
            }
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find application scores, id = " + applicationId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }


    @Override
    public List<ApplicationSearchResult> getAllApplications() throws DAOException {
        List<ApplicationSearchResult> result = new ArrayList<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_APPLICATIONS)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ApplicationSearchResult asr = new ApplicationSearchResult();
                asr.setApplication(extractApplication(rs));
                asr.setUser(extractUser(rs));
                asr.setCurrentStatus(extractStatus(rs));
                asr.setProgramme(extractProgramme(rs));
                result.add(asr);
            }
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find the all applications";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }


    @Override
    public boolean changeApplicationStatus(int applicationId, ApplicationStatus.Statuses status, String comment) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection()) {

            return createStatusFlowRecord(applicationId, status, comment, conn) > 0;

        } catch (SQLException e) {
            String msg = "Failed to change status";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public List<Application> getAcceptedApplicationsForProgramme(int programmeId) throws DAOException {
        List<Application> result = new ArrayList<>();

        if (programmeId <= 0) {
            return result;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ACCEPTED_APPLICATIONS_FOR_PROGRAMME)) {

            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Application application = extractApplicationWithScores(rs);
                result.add(application);
            }
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find applications, programmeId = " + programmeId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private Score extractScore(ResultSet rs) throws SQLException {
        return new Score(rs.getInt(FILED_SUBJECT_SCORE));
    }

    private Subject extractSubject(ResultSet rs) throws SQLException {
        return new Subject.Builder()
                .id(rs.getInt(FIELD_SUBJECT_ID))
                .name(rs.getString(FIELD_SUBJECT_NAME))
                .build();
    }


    private List<ApplicationStatus> extractStatusFlow(ResultSet rs) throws SQLException {
        List<ApplicationStatus> result = new ArrayList<>();
        result.add(extractStatus(rs));
        while (rs.next()) {
            result.add(extractStatus(rs));
        }
        return result;
    }

    private User extractUser(ResultSet rs) throws SQLException {
        return new User.Builder()
                .id(rs.getInt(FIELD_USER_ID))
                .email(rs.getString(FIELD_USER_EMAIL))
                .firstName(rs.getString(FIELD_USER_FIRST_NAME))
                .middleName(rs.getString(FIELD_USER_MIDDLE_NAME))
                .lastName(rs.getString(FIELD_USER_LAST_NAME))
                .build();
    }

    private Programme extractProgramme(ResultSet rs) throws SQLException {
        return new Programme.Builder()
                .id(rs.getInt(FIELD_PROGRAMME_ID))
                .name(rs.getString(FIELD_PROGRAMME_NAME))
                .departmentId(rs.getInt(FIELD_DEPARTMENT_ID))
                .shortDescription(rs.getString(FIELD_PROGRAMME_SHORT_DESCRIPTION))
                .build();
    }

    private Application extractApplication(ResultSet rs) throws SQLException {
        return new Application.Builder()
                .id(rs.getInt(FIELD_APPLICATION_ID))
                .programmeId(rs.getInt(FIELD_PROGRAMME_ID))
                .userId(rs.getInt(FIELD_USER_ID))
                .build();
    }

    private Application extractApplicationWithScores(ResultSet rs) throws SQLException, DAOException {
        int applicationId = rs.getInt(FIELD_APPLICATION_ID);

        return new Application.Builder()
                .id(applicationId)
                .programmeId(rs.getInt(FIELD_PROGRAMME_ID))
                .userId(rs.getInt(FIELD_USER_ID))
                .scores(getApplicationScores(applicationId))
                .build();
    }

    private ApplicationStatus extractStatus(ResultSet rs) throws SQLException {
        return new ApplicationStatus.Builder()
                .id(rs.getInt(FIELD_STATUS_FLOW_ID))
                .status(ApplicationStatus.Statuses
                        .valueOf(rs.getString(FIELD_STATUS_NAME)))
                .dateOfCreation(rs.getTimestamp(FIELD_DATE_OF_CREATION))
                .comment(rs.getString(FIELD_STATUS_COMMENT))
                .build();
    }

    private boolean areScoresSaved(int applicationId, Map<Subject, Score> scores, Connection conn) throws DAOException {
        if (scores == null || scores.isEmpty()) {
            return false;
        }

        String statement = IntStream.rangeClosed(1, scores.size())
                .mapToObj(i -> SQL_SAVE_SCORE_ARGS)
                .collect(Collectors.joining(", ", SQL_SAVE_SCORE_PREF, ";"));

        try (PreparedStatement stmt = conn.prepareStatement(statement)) {

            int k = 0;
            for (Map.Entry<Subject, Score> entry : scores.entrySet()) {
                stmt.setInt(++k, applicationId);
                stmt.setInt(++k, entry.getKey().getId());
                stmt.setInt(++k, entry.getValue().getScore());
            }

            if (stmt.executeUpdate() == scores.size()) {
                log.debug("Saved {} score results", scores.size());
                return true;
            }

            return false;

        } catch (SQLException e) {
            String msg = "Failed to save score results";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private void rollBack(Connection conn) {
        try {
            log.warn("Rolling back changes in database...");
            conn.rollback();
            log.warn("Rolled back successfully");
        } catch (SQLException e) {
            log.error("Unable to roll back changes");
        }
    }

    private Optional<Integer> createApplication(int userId, int programmeId, Connection conn) throws DAOException, SQLException {
        ResultSet rs = null;

        try (PreparedStatement stmt = conn.prepareStatement(SQL_CREATE_APPLICATION,
                Statement.RETURN_GENERATED_KEYS)) {

            int k = 0;
            stmt.setInt(++k, userId);
            stmt.setInt(++k, programmeId);

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int applicationId = rs.getInt(1);
                    log.debug("New application saved, id = {}", applicationId);

                    return Optional.of(applicationId);
                }
            }

            return Optional.empty();

        } catch (SQLException e) {
            String msg = "Failed to save application";
            log.error(msg, e);
            throw new DAOException(msg);

        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    private int createStatusFlowRecord(int applicationId, ApplicationStatus.Statuses status, String comment, Connection conn) throws DAOException, SQLException {
        ResultSet rs = null;

        try (PreparedStatement stmt = conn.prepareStatement(SQL_CREATE_STATUS_FLOW_RECORD,
                Statement.RETURN_GENERATED_KEYS)) {

            int k = 0;
            stmt.setInt(++k, applicationId);
            stmt.setString(++k, status.name());
            stmt.setString(++k, comment);
            stmt.setTimestamp(++k, new Timestamp(System.currentTimeMillis()));

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int statusFlowId = rs.getInt(1);
                    log.debug("New status flow record saved, id = {}", statusFlowId);

                    return statusFlowId;
                }
            }
            return 0;

        } catch (SQLException e) {
            String msg = "Failed to save status flow record";
            log.error(msg, e);
            throw new DAOException(msg);

        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    private void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.error("Failed to close connection", e);
            }
        }
    }
}
