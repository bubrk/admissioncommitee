package edu.university.dao.mysql;

import edu.university.dao.*;

@Deprecated
public class MysqlDAOFactory extends DAOFactory {

    @Override
    public UserDAO getUserDAO() {
        return new MysqlUserDAO();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new MysqlDepartmentDAO();
    }

    @Override
    public ProgrammeDAO getProgrammeDAO() {
        return new MysqlProgrammeDAO();
    }

    @Override
    public SubjectDAO getSubjectDAO() {
        return new MysqlSubjectDAO();
    }

    @Override
    public ApplicationDAO getApplicationDAO() {
        return new MysqlApplicationDAO();
    }

    @Override
    public ReportDAO getReportsDAO() {
        return new MysqlReportDAO();
    }
}
