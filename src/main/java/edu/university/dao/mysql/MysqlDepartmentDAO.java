package edu.university.dao.mysql;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.DepartmentDAO;
import edu.university.entity.Department;
import edu.university.exception.DAOException;
import org.slf4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MysqlDepartmentDAO implements DepartmentDAO {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ConnectionBuilder connectionBuilder;

    private static final String FIELD_DEPARTMENT_ID = "department_id";
    private static final String FIELD_PARENT_ID = "parent_id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIND_DEPARTMENT_BY_ID = "SELECT * FROM departments WHERE department_id=?";
    private static final String FIND_ALL_DEPARTMENTS = "SELECT * FROM departments";
    private static final String FIND_ALL_PARENT_DEPARTMENTS = "SELECT * FROM departments WHERE parent_id is NULL";
    private static final String FIND_ALL_SUB_DEPARTMENTS = "SELECT * FROM departments WHERE parent_id=?";
    private static final String UPDATE_DEPARTMENT = "UPDATE departments\n" +
            "SET name = ?, description = ?\n" +
            "WHERE department_id = ?";
    private static final String SQL_DELETE_DEPARTMENT = "DELETE FROM departments WHERE department_id = ?";
    private static final String SQL_CREATE_DEPARTMENT = "INSERT INTO departments VALUE (DEFAULT, ?, ?, ?)";
    private static final String SQL_CREATE_ROOT_DEPARTMENT = "INSERT INTO departments VALUE (DEFAULT, NULL, ?, ?)";

    @Override
    public Optional<Department> getDepartment(int departmentId) throws DAOException {
        if (departmentId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(FIND_DEPARTMENT_BY_ID)) {

            stmt.setInt(1, departmentId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(getDepartment(rs));
            }
            return Optional.empty();

        } catch (SQLException e) {
            log.error("Exception in findDepartment method", e);
            throw new DAOException("Failed to find the department");
        }
    }


    @Override
    public List<Department> getAllDepartments() throws DAOException {
        List<Department> result = new ArrayList<>();
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(FIND_ALL_DEPARTMENTS)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(getDepartment(rs));
            }
            return result;
        } catch (SQLException e) {
            log.error("Exception in findAllDepartment method", e);
            throw new DAOException("Failed to find departments");
        }
    }

    @Override
    public List<Department> getAllSubDepartments(int parentId) throws DAOException {
        List<Department> result = new ArrayList<>();

        String query = FIND_ALL_SUB_DEPARTMENTS;
        if (parentId == 0) {
            query = FIND_ALL_PARENT_DEPARTMENTS;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)) {

            if (parentId != 0) {
                stmt.setInt(1, parentId);
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(getDepartment(rs));
            }
            return result;

        } catch (SQLException e) {
            log.error("Exception in findAllSubDepartment method", e);
            throw new DAOException("Failed to find departments");
        }
    }

    @Override
    public Optional<Department> createDepartment(Department department) throws DAOException {
        if (department == null) {
            return Optional.empty();
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = connectionBuilder.getConnection();

            stmt = conn.prepareStatement(department.getParentId() == 0 ?
                            SQL_CREATE_ROOT_DEPARTMENT : SQL_CREATE_DEPARTMENT,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 0;
            if (department.getParentId()>0) {
                stmt.setInt(++k, department.getParentId());
            }
            stmt.setString(++k, department.getName());
            stmt.setString(++k, department.getDescription());

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    Department createdDepartment = new Department.Builder()
                            .id(rs.getInt(1))
                            .name(department.getName())
                            .description(department.getDescription())
                            .parentId(department.getParentId())
                            .build();
                    log.debug("New department saved, id = {}", createdDepartment.getId());
                    return Optional.of(createdDepartment);
                }
            }
            return Optional.empty();

        } catch (SQLException e) {
            String msg = "Failed to save department";
            log.error(msg, e);
            throw new DAOException(msg);

        } finally {
            closeResources(rs, stmt, conn);
        }
    }

    @Override
    public boolean updateDepartment(Department department) throws DAOException {
        if (department == null) {
            return false;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(UPDATE_DEPARTMENT)) {

            int k = 0;
            stmt.setString(++k, department.getName());
            stmt.setString(++k, department.getDescription());
            stmt.setInt(++k, department.getId());

            if (stmt.executeUpdate() > 0) {
                return true;
            }

            return false;

        } catch (SQLException e) {
            String msg = "Failed to update department";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private Department getDepartment(ResultSet rs) throws SQLException {
        return new Department.Builder()
                .id(rs.getInt(FIELD_DEPARTMENT_ID))
                .parentId(rs.getInt(FIELD_PARENT_ID))
                .name(rs.getString(FIELD_NAME))
                .description(rs.getString(FIELD_DESCRIPTION))
                .build();
    }

    @Override
    public boolean deleteDepartment(int departmentId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_DELETE_DEPARTMENT)) {

            stmt.setInt(1, departmentId);
            stmt.executeUpdate();
            log.debug("Department with id={} was removed from database", departmentId);
            return true;
        } catch (SQLException e) {
            final String msg = "Failed to delete department";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn) {
        final String errMsg = "Failed to close resources";
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }
    }
}
