package edu.university.dao.mysql;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.ProgrammeDAO;
import edu.university.entity.*;
import edu.university.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class MysqlProgrammeDAO implements ProgrammeDAO {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ConnectionBuilder connectionBuilder;

    private static final String FIELD_ADMISSIONS_START_DATE = "admissions_start_date";
    private static final String FIELD_PROGRAMME_ID = "programme_id";
    private static final String FIELD_DEPARTMENT_ID = "department_id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_SHORT_DESCRIPTION = "short_description";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_ADMISSIONS_END_DATE = "admissions_end_date";
    private static final String FIELD_BUDGET_PLACES = "budget_places";
    private static final String FIELD_TOTAL_PLACES = "total_places";
    private static final String FIELD_SUBJECT_GROUP = "subject_group";
    private static final String FIELD_SUBJECT_ID = "subject_id";
    private static final String FIELD_SUBJECT_NAME = "subject_name";
    private static final String FIELD_MIN_SCORE = "min_score";
    private static final String FIELD_COEFFICIENT = "coefficient";
    private static final String SQL_FIND_PROGRAMME_DETAILS = "SELECT * FROM programme_details WHERE programme_id=?";
    private static final String SQL_FIND_PROGRAMME_BY_ID = "SELECT * FROM programmes WHERE programme_id=?";
    private static final String SQL_FIND_PROGRAMME_REQUIREMENTS = "SELECT ssr.subject_group, s.subject_id, s.name AS subject_name, ssr.min_score, ssr.coefficient FROM programme_subjects_requirements  pr " +
            "JOIN subjects s ON pr.subject_id = s.subject_id " +
            "JOIN programme_subjects_scores_requirements ssr ON  pr.programme_id = ssr.programme_id AND pr.subject_group=ssr.subject_group " +
            "WHERE pr.programme_id=?";
    private static final String SQL_FIND_ALL_PROGRAMMES_WITH_DETAILS_FILTERING = "SELECT p.programme_id, department_id, name, short_description, description, budget_places, total_places, admissions_start_date, admissions_end_date FROM programmes AS p " +
            "JOIN programme_details pd on p.programme_id = pd.programme_id " +
            "WHERE p.name LIKE ?";
    private static final String SQL_FIND_ALL_PROGRAMMES = "SELECT * FROM programmes";
    private static final String SQL_FIND_ALL_PROGRAMMES_IN_DEPARTMENT = "SELECT * FROM programmes WHERE department_id=?";
    private static final String SQL_FIND_ALL_PROGRAMMES_WITH_DETAILS = "SELECT p.programme_id, department_id, name, short_description, description, budget_places, total_places, admissions_start_date, admissions_end_date FROM programmes AS p " +
            "JOIN programme_details pd on p.programme_id = pd.programme_id ";
    private static final String SQL_CREATE_PROGRAMME = "INSERT INTO programmes VALUE (DEFAULT, ?, ?, ?)";
    private static final String SQL_UPDATE_PROGRAMME = "UPDATE programmes SET name=?, short_description=? WHERE programme_id=?";
    private static final String SQL_CREATE_PROGRAMME_DETAILS = "INSERT INTO programme_details VALUE (?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_PROGRAMME_DETAILS = "UPDATE programme_details SET description=?, budget_places=?, total_places=?, admissions_start_date=?,admissions_end_date=? WHERE programme_id=?";
    private static final String SQL_CREATE_PROGRAMME_REQUIREMENTS = "INSERT INTO programme_subjects_scores_requirements VALUE (?,?,?,?)";
    private static final String SQL_DELETE_PROGRAMME_SUBJECTS_REQUIREMENTS = "DELETE FROM programme_subjects_requirements WHERE programme_id=?";
    private static final String SQL_DELETE_PROGRAMME_SCORES_REQUIREMENTS = "DELETE FROM programme_subjects_scores_requirements WHERE programme_id=?";
    private static final String SQL_CREATE_SUBJECTS_GROUP = "INSERT INTO programme_subjects_requirements VALUES ";
    private static final String SQL_GET_PROGRAMME_SUBJECTS_AMOUNT = "SELECT COUNT(*) FROM programme_subjects_requirements WHERE programme_id=?";
    private static final String SQL_GET_PROGRAMME_SUBJECTS_GROUPS_AMOUNT = "SELECT COUNT(*) FROM programme_subjects_scores_requirements WHERE programme_id=?";
    private static final String SQL_DELETE_PROGRAMME = "DELETE FROM programmes WHERE programme_id = ?";

    @Override
    public Optional<Programme> getProgrammeById(int programmeId) throws DAOException {
        if (programmeId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_PROGRAMME_BY_ID)) {

            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(extractProgramme(rs));
            }
            return Optional.empty();

        } catch (SQLException e) {
            log.error("Exception in findProgramme method", e);
            throw new DAOException("Failed to find the programme");
        }
    }

    @Override
    public List<Programme> getProgrammesInDepartment(int departmentId) throws DAOException {
        List<Programme> result = new ArrayList<>();
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_PROGRAMMES_IN_DEPARTMENT)) {

            stmt.setInt(1, departmentId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(extractProgramme(rs));
            }
            return result;
        } catch (SQLException e) {
            log.error("Exception in findProgrammesInDepartment method", e);
            throw new DAOException("Failed to find programmes");
        }
    }

    @Override
    public List<Programme> getAllProgrammes() throws DAOException {
        List<Programme> result = new ArrayList<>();
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_PROGRAMMES)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(extractProgramme(rs));
            }
            return result;
        } catch (SQLException e) {
            log.error("Exception in findAllProgramme method", e);
            throw new DAOException("Failed to find programmes");
        }
    }

    @Override
    public Optional<ProgrammeDetails> getProgrammeDetails(int programmeId) throws DAOException {
        if (programmeId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_PROGRAMME_DETAILS)) {

            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(extractProgrammeDetails(rs));
            }
            return Optional.empty();
        } catch (SQLException e) {
            String msg = "Failed to find the programme details";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }


    @Override
    public List<SubjectRequirements> getProgrammeRequirements(int programmeId) throws DAOException {
        if (programmeId <= 0) {
            return new ArrayList<>();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_PROGRAMME_REQUIREMENTS)) {

            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            Map<Integer, SubjectRequirements> map = new TreeMap<>();
            while (rs.next()) {
                int subjectGroup = rs.getInt(FIELD_SUBJECT_GROUP);
                if (map.containsKey(subjectGroup)) {
                    SubjectRequirements sr = map.get(subjectGroup);
                    sr.getSubjects()
                            .add(extractSubject(rs));
                } else {
                    SubjectRequirements sr = extractSubjectRequirements(rs);
                    map.put(subjectGroup, sr);
                }
            }
            //sort by groupId and return as list
            return map.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .map(Map.Entry::getValue)
                    .collect(Collectors.toList());

        } catch (SQLException e) {
            String msg = "Failed to find the programme requirements";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private SubjectRequirements extractSubjectRequirements(ResultSet rs) throws SQLException {
        Subject subject = extractSubject(rs);
        List<Subject> list = new ArrayList<>();
        list.add(subject);

        return new SubjectRequirements.Builder()
                .subjects(list)
                .minScore(rs.getInt(FIELD_MIN_SCORE))
                .coefficient(rs.getDouble(FIELD_COEFFICIENT))
                .build();
    }

    private Subject extractSubject(ResultSet rs) throws SQLException {
        return new Subject.Builder()
                .id(rs.getInt(FIELD_SUBJECT_ID))
                .name(rs.getString(FIELD_SUBJECT_NAME))
                .build();
    }


    private Programme extractProgramme(ResultSet rs) throws SQLException {
        return new Programme.Builder()
                .id(rs.getInt(FIELD_PROGRAMME_ID))
                .departmentId(rs.getInt(FIELD_DEPARTMENT_ID))
                .name(rs.getString(FIELD_NAME))
                .shortDescription(rs.getString(FIELD_SHORT_DESCRIPTION))
                .build();
    }


    private ProgrammeDetails extractProgrammeDetails(ResultSet rs) throws SQLException {
        return new ProgrammeDetails.Builder()
                .description(rs.getString(FIELD_DESCRIPTION))
                .budgetPlaces(rs.getInt(FIELD_BUDGET_PLACES))
                .totalPlaces(rs.getInt(FIELD_TOTAL_PLACES))
                .admissionsStartDate(rs.getDate(FIELD_ADMISSIONS_START_DATE))
                .admissionsEndDate(rs.getDate(FIELD_ADMISSIONS_END_DATE))
                .build();
    }

    @Override
    public Map<Programme, ProgrammeDetails> getAllProgrammesWithDetails() throws DAOException {
        Map<Programme, ProgrammeDetails> result = new HashMap<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_PROGRAMMES_WITH_DETAILS)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.put(extractProgramme(rs),
                        extractProgrammeDetails(rs));
            }
            return result;
        } catch (SQLException e) {
            log.error("Exception in findAllProgramme method", e);
            throw new DAOException("Failed to find programmes");
        }
    }

    @Override
    public Map<Programme, ProgrammeDetails> getAllProgrammesWithDetailsFiltering(String name) throws DAOException {
        Map<Programme, ProgrammeDetails> result = new HashMap<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_PROGRAMMES_WITH_DETAILS_FILTERING)) {

            stmt.setString(1, "%" + (name == null ? "" : name) + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.put(extractProgramme(rs),
                        extractProgrammeDetails(rs));
            }
            rs.close();

            return result;
        } catch (SQLException e) {
            log.error("Exception in findAllProgramme method", e);
            throw new DAOException("Failed to find programmes");
        }
    }

    @Override
    public boolean deleteProgramme(int programmeId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_DELETE_PROGRAMME)) {

            stmt.setInt(1, programmeId);
            stmt.executeUpdate();
            log.debug("Programme with id={} was removed from database", programmeId);
            return true;
        } catch (SQLException e) {
            final String msg = "Failed to delete programme";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Optional<Programme> createProgramme(Programme programme, ProgrammeDetails programmeDetails, List<SubjectRequirements> programmeRequirements) throws DAOException {
        if (programme == null || programmeDetails == null || programmeRequirements.isEmpty()) {
            return Optional.empty();
        }

        Connection conn = null;

        try {
            conn = connectionBuilder.getConnection();
            conn.setAutoCommit(false);

            int programmeId = createProgramme(programme, conn)
                    .orElseThrow(() -> new SQLException("Unable to save programme"));

            if (!createProgrammeDetails(programmeId, programmeDetails, conn)) {
                throw new SQLException("Unable to save programme details");
            }

            if (!createProgrammeRequirements(programmeId, programmeRequirements, conn)) {
                throw new SQLException("Failed to save programme requirements");
            }

            conn.commit();

            return Optional.of(new Programme.Builder()
                    .id(programmeId)
                    .departmentId(programme.getDepartmentId())
                    .name(programme.getName())
                    .shortDescription(programme.getShortDescription())
                    .build());

        } catch (SQLException e) {
            String msg = "Failed to create new programme in database";
            log.error(msg, e);

            rollBack(conn);

            throw new DAOException(msg);
        } finally {
            closeConnection(conn);
        }
    }

    @Override
    public boolean updateProgramme(Programme programme, ProgrammeDetails programmeDetails, List<SubjectRequirements> programmeRequirements) throws DAOException {
        if (programme == null || programmeDetails == null || programmeRequirements.isEmpty()) {
            return false;
        }

        Connection conn = null;

        try {
            conn = connectionBuilder.getConnection();
            conn.setAutoCommit(false);

            if (!updateProgramme(programme, conn)) {
                throw new SQLException("Unable to update programme");
            }

            if (!updateProgrammeDetails(programme.getId(), programmeDetails, conn)) {
                throw new SQLException("Unable to update programme details");
            }
            if (!deleteProgrammeRequirements(programme.getId(), conn)) {
                throw new SQLException("Failed to update programme requirements");
            }

            if (!createProgrammeRequirements(programme.getId(), programmeRequirements, conn)) {
                throw new SQLException("Failed to save programme requirements");
            }

            conn.commit();

            return true;

        } catch (SQLException e) {
            String msg = "Failed to update programme in database";
            log.error(msg, e);

            rollBack(conn);

            throw new DAOException(msg);
        } finally {
            closeConnection(conn);
        }
    }

    private boolean deleteProgrammeRequirements(int programmeId, Connection conn) throws DAOException {
        int countSubjects = getProgrammeSubjectsCount(programmeId, conn);
        int countGroups = getProgrammeSubjectsGroupsCount(programmeId, conn);

        return countSubjects == deleteSubjectsRequirements(programmeId, conn) &&
                countGroups == deleteScoresRequirements(programmeId, conn);
    }

    private int getProgrammeSubjectsCount(int programmeId, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_GET_PROGRAMME_SUBJECTS_AMOUNT)) {
            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            log.error("Exception in getProgrammeSubjectsCount method", e);
            throw new DAOException("Failed to count subjects for programme id=" + programmeId);
        }
    }

    private int getProgrammeSubjectsGroupsCount(int programmeId, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_GET_PROGRAMME_SUBJECTS_GROUPS_AMOUNT)) {
            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            log.error("Exception in getProgrammeSubjectsGroupsCount method", e);
            throw new DAOException("Failed to count subject groups for programme id=" + programmeId);
        }
    }

    private int deleteScoresRequirements(int programmeId, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_DELETE_PROGRAMME_SCORES_REQUIREMENTS)) {
            int k = 0;
            stmt.setInt(++k, programmeId);

            return stmt.executeUpdate();

        } catch (SQLException e) {
            String msg = "Failed to remove scores requirements";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private int deleteSubjectsRequirements(int programmeId, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_DELETE_PROGRAMME_SUBJECTS_REQUIREMENTS)) {
            int k = 0;
            stmt.setInt(++k, programmeId);

            return stmt.executeUpdate();

        } catch (SQLException e) {
            String msg = "Failed to remove subjects requirements";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private boolean updateProgramme(Programme programme, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_UPDATE_PROGRAMME)) {
            int k = 0;
            stmt.setString(++k, programme.getName());
            stmt.setString(++k, programme.getShortDescription());
            stmt.setInt(++k, programme.getId());

            return stmt.executeUpdate() > 0;

        } catch (SQLException e) {
            String msg = "Failed to update programme";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private boolean updateProgrammeDetails(int programmeId, ProgrammeDetails programmeDetails, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_UPDATE_PROGRAMME_DETAILS)) {
            int k = 0;
            stmt.setString(++k, programmeDetails.getDescription());
            stmt.setInt(++k, programmeDetails.getBudgetPlaces());
            stmt.setInt(++k, programmeDetails.getTotalPlaces());
            stmt.setDate(++k, new java.sql.Date(programmeDetails.getAdmissionsStartDate().getTime()));
            stmt.setDate(++k, new java.sql.Date(programmeDetails.getAdmissionsEndDate().getTime()));
            stmt.setInt(++k, programmeId);

            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            String msg = "Failed to update programme details";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private Optional<Integer> createProgramme(Programme programme, Connection conn) throws DAOException, SQLException {
        ResultSet rs = null;

        try (PreparedStatement stmt = conn.prepareStatement(SQL_CREATE_PROGRAMME,
                Statement.RETURN_GENERATED_KEYS)) {

            int k = 0;
            stmt.setInt(++k, programme.getDepartmentId());
            stmt.setString(++k, programme.getName());
            stmt.setString(++k, programme.getShortDescription());

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int programmeId = rs.getInt(1);
                    log.debug("New programme saved, id = {}", programmeId);

                    return Optional.of(programmeId);
                }
            }
            return Optional.empty();

        } catch (SQLException e) {
            String msg = "Failed to save programme";
            log.error(msg, e);
            throw new DAOException(msg);

        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    private boolean createProgrammeDetails(int programmeId, ProgrammeDetails programmeDetails, Connection conn) throws DAOException {
        try (PreparedStatement stmt = conn.prepareStatement(SQL_CREATE_PROGRAMME_DETAILS)) {

            int k = 0;
            stmt.setInt(++k, programmeId);
            stmt.setString(++k, programmeDetails.getDescription());
            stmt.setInt(++k, programmeDetails.getBudgetPlaces());
            stmt.setInt(++k, programmeDetails.getTotalPlaces());
            stmt.setDate(++k, new java.sql.Date(programmeDetails.getAdmissionsStartDate().getTime()));
            stmt.setDate(++k, new java.sql.Date(programmeDetails.getAdmissionsEndDate().getTime()));

            return stmt.executeUpdate() > 0;

        } catch (SQLException e) {
            String msg = "Failed to save programme details";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private boolean createProgrammeRequirements(int programmeId, List<SubjectRequirements> programmeRequirements, Connection conn) throws DAOException {
        int groupIndex = 1;
        for (SubjectRequirements requirements : programmeRequirements) {
            if (!createSubjectGroup(programmeId, groupIndex, requirements.getSubjects(), conn)) {
                throw new DAOException("Can't create subjects group");
            }
            if (!createRequirements(programmeId, groupIndex, requirements.getMinScore(), requirements.getCoefficient(), conn)) {
                throw new DAOException("Can't create subject scores requirements");
            }
            groupIndex++;
        }
        return true;
    }

    private boolean createSubjectGroup(int programmeId, int groupIndex, List<Subject> subjects, Connection conn) throws DAOException {
        String query = subjects.stream()
                .map(s -> "(?,?,?)")
                .collect(Collectors.joining(", ", SQL_CREATE_SUBJECTS_GROUP, ""));

        try (PreparedStatement stmt = conn.prepareStatement(query)) {

            int k = 0;
            for (Subject subject : subjects) {
                stmt.setInt(++k, programmeId);
                stmt.setInt(++k, groupIndex);
                stmt.setInt(++k, subject.getId());
            }

            return stmt.executeUpdate() > 0;

        } catch (SQLException e) {
            String msg = "Failed to save requirements for programme";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private boolean createRequirements(int programmeId, int groupIndex, int minScore, double coefficient, Connection conn) throws DAOException {
        log.debug("Saving requirements for programme id={}, groupIndex={}, minScore={}, coef={}", programmeId, groupIndex, minScore, coefficient);
        try (PreparedStatement stmt = conn.prepareStatement(SQL_CREATE_PROGRAMME_REQUIREMENTS)) {

            int k = 0;
            stmt.setInt(++k, programmeId);
            stmt.setInt(++k, groupIndex);
            stmt.setInt(++k, minScore);
            stmt.setDouble(++k, coefficient);

            return stmt.executeUpdate() > 0;

        } catch (SQLException e) {
            String msg = "Failed to save requirements for programme";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }


    private void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.error("Failed to close connection", e);
            }
        }
    }

    private void rollBack(Connection conn) {
        try {
            log.warn("Rolling back changes in database...");
            conn.rollback();
            log.warn("Rolled back successfully");
        } catch (SQLException e) {
            log.error("Unable to roll back changes");
        }
    }

}
