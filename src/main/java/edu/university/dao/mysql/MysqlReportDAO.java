package edu.university.dao.mysql;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.ReportDAO;
import edu.university.entity.Document;
import edu.university.entity.Programme;
import edu.university.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

public class MysqlReportDAO implements ReportDAO {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ConnectionBuilder connectionBuilder;

    private static final String FIELD_REPORT_NAME = "report_name";
    private static final String FIELD_REPORT_FILE = "report_file";
    private static final String FIELD_REPORT_ID = "report_id";
    private static final String FIELD_PROGRAMME_ID = "programme_id";
    private static final String FIELD_PROGRAMME_NAME = "programme_name";
    private static final String FIELD_DEPARTMENT_ID = "department_id";
    private static final String FIELD_PROGRAMME_SHORT_DESCRIPTION = "short_description";
    private static final String SQL_SAVE_REPORT = "INSERT INTO reports VALUE (DEFAULT,?,?,?)";
    private static final String SQL_GET_REPORT_BY_ID = "SELECT report_name, report_file FROM reports WHERE report_id=?";
    private static final String SQL_GET_REPORTS_BY_PROGRAMME_ID = "SELECT report_id, report_name, report_file FROM reports WHERE programme_id=?";
    private static final String SQL_GET_ALL_REPORTS = "SELECT report_id, report_name, report_file, reports.programme_id, p.name as programme_name, department_id, short_description " +
            "FROM reports JOIN programmes p on reports.programme_id = p.programme_id";

    @Override
    public Optional<Document> getReportById(int reportId) throws DAOException {
        if (reportId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_GET_REPORT_BY_ID)) {

            stmt.setInt(1, reportId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(new Document.Builder()
                        .id(reportId)
                        .name(rs.getString(FIELD_REPORT_NAME))
                        .file(rs.getString(FIELD_REPORT_FILE))
                        .build());
            }
            return Optional.empty();

        } catch (SQLException e) {
            final String msg = "Failed to find the user document";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public List<Document> getReportsByProgrammeId(int programmeId) throws DAOException {
        List<Document> result = new ArrayList<>();
        if (programmeId <= 0) {
            return result;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_GET_REPORTS_BY_PROGRAMME_ID)) {

            stmt.setInt(1, programmeId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(new Document.Builder()
                        .id(rs.getInt(FIELD_REPORT_ID))
                        .name(rs.getString(FIELD_REPORT_NAME))
                        .file(rs.getString(FIELD_REPORT_FILE))
                        .build());
            }
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find the reports for programme id=" + programmeId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Map<Programme, Document> getAllReports() throws DAOException {
        Map<Programme, Document> result = new HashMap<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_GET_ALL_REPORTS)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.put(extractProgramme(rs),
                        extractDocument(rs));
            }
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find all reports";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private Document extractDocument(ResultSet rs) throws SQLException {
        return new Document.Builder()
                .id(rs.getInt(FIELD_REPORT_ID))
                .name(rs.getString(FIELD_REPORT_NAME))
                .file(rs.getString(FIELD_REPORT_FILE))
                .build();
    }

    @Override
    public Optional<Document> saveReport(int programmeId, String reportName, String fileName) throws DAOException {
        if (programmeId <= 0 || reportName == null || fileName == null || reportName.isEmpty() || fileName.isEmpty()) {
            return Optional.empty();
        }

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = connectionBuilder.getConnection();
            stmt = conn.prepareStatement(SQL_SAVE_REPORT,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 0;
            stmt.setInt(++k, programmeId);
            stmt.setString(++k, reportName);
            stmt.setString(++k, fileName);
            log.debug("report name: {}",reportName);
            log.debug("file name: {}",fileName);

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    Document document = new Document.Builder()
                            .id(rs.getInt(1))
                            .name(reportName)
                            .build();
                    log.debug("New report saved: {}", document);
                    return Optional.of(document);
                }
            }
            return Optional.empty();

        } catch (SQLException e) {
            String msg = "Failed to save report to database";
            log.error(msg, e);
            throw new DAOException(msg);
        } finally {
            closeResources(rs, stmt, conn);
        }
    }

    private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn) {
        final String errMsg = "Failed to close resources";
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }
    }

    private Programme extractProgramme(ResultSet rs) throws SQLException {
        return new Programme.Builder()
                .id(rs.getInt(FIELD_PROGRAMME_ID))
                .departmentId(rs.getInt(FIELD_DEPARTMENT_ID))
                .name(rs.getString(FIELD_PROGRAMME_NAME))
                .shortDescription(rs.getString(FIELD_PROGRAMME_SHORT_DESCRIPTION))
                .build();
    }
}
