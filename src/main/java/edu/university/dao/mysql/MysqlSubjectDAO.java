package edu.university.dao.mysql;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.SubjectDAO;
import edu.university.entity.Department;
import edu.university.entity.Subject;
import edu.university.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MysqlSubjectDAO implements SubjectDAO {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ConnectionBuilder connectionBuilder;

    private static final String ERROR_MESSAGE = "Error occurred while working with database";
    private static final String FIELD_SUBJECT_ID = "subject_id";
    private static final String FIELD_SUBJECT_NAME = "name";
    private static final String FIND_ALL_SUBJECTS = "SELECT * FROM subjects";
    private static final String FIND_SUBJECT_BY_ID = "SELECT * FROM subjects WHERE subject_id=?";
    private static final String CREATE_SUBJECT = "INSERT INTO subjects VALUE (DEFAULT,?)";
    private static final String UPDATE_SUBJECT = "UPDATE subjects SET name = ? WHERE subject_id = ?";
    private static final String SQL_DELETE_SUBJECT = "DELETE FROM subjects WHERE subject_id = ?";

    @Override
    public List<Subject> getAllSubjects() throws DAOException {
        List<Subject> subjects = new ArrayList<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(FIND_ALL_SUBJECTS)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                subjects.add(new Subject.Builder()
                        .id(rs.getInt(FIELD_SUBJECT_ID))
                        .name(rs.getString(FIELD_SUBJECT_NAME))
                        .build());
            }
            return subjects;
        } catch (SQLException e) {
            log.error("Exception in find subjects method", e);
            throw new DAOException(ERROR_MESSAGE);
        }
    }

    @Override
    public Optional<Subject> getSubjectById(int subjectId) throws DAOException {
        if (subjectId<=0){
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(FIND_SUBJECT_BY_ID)) {

            stmt.setInt(1,subjectId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(new Subject.Builder()
                        .id(rs.getInt(FIELD_SUBJECT_ID))
                        .name(rs.getString(FIELD_SUBJECT_NAME))
                        .build());
            }
            return Optional.empty();
        } catch (SQLException e) {
            log.error("Exception in find subject method", e);
            throw new DAOException(ERROR_MESSAGE);
        }
    }

    @Override
    public Optional<Subject> createSubject(String subjectName) throws DAOException {
        if (subjectName == null || subjectName.isEmpty()) {
            return Optional.empty();
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = connectionBuilder.getConnection();

            stmt = conn.prepareStatement(CREATE_SUBJECT,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 0;
            stmt.setString(++k, subjectName);

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    Subject subject = new Subject.Builder()
                            .id(rs.getInt(1))
                            .name(subjectName)
                            .build();

                    log.debug("New subject saved, id = {}", subject);
                    return Optional.of(subject);
                }
            }
            return Optional.empty();

        } catch (SQLException e) {
            String msg = "Failed to save subject";
            log.error(msg, e);
            throw new DAOException(msg);

        } finally {
            closeResources(rs, stmt, conn);
        }
    }

    @Override
    public boolean updateSubject(Subject subject) throws DAOException {
        if (subject == null) {
            return false;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(UPDATE_SUBJECT)) {

            int k = 0;
            stmt.setString(++k, subject.getName());
            stmt.setInt(++k, subject.getId());

            if (stmt.executeUpdate() > 0) {
                return true;
            }
            return false;

        } catch (SQLException e) {
            String msg = "Failed to update subject";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public boolean deleteSubject(int subjectId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_DELETE_SUBJECT)) {

            stmt.setInt(1, subjectId);
            stmt.executeUpdate();
            log.debug("Subject with id={} was removed from database", subjectId);
            return true;

        } catch (SQLException e) {
            final String msg = "Failed to delete subject";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn) {
        final String errMsg = "Failed to close resources";
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }
    }
}
