package edu.university.dao.mysql;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.UserDAO;
import edu.university.entity.*;
import edu.university.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.sql.*;
import java.util.*;


public class MysqlUserDAO implements UserDAO {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ConnectionBuilder connectionBuilder;

    private static final String FIELD_COMMENT = "comment";
    private static final String FIELD_CITY = "city";
    private static final String FIELD_USER_ID = "user_id";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_FIRST_NAME = "first_name";
    private static final String FIELD_LAST_NAME = "last_name";
    private static final String FIELD_MIDDLE_NAME = "middle_name";
    private static final String FIELD_PASSWORD = "password";
    private static final String FIELD_ROLE = "role_name";
    private static final String FIELD_REGION = "region";
    private static final String FIELD_SCHOOL = "school";
    private static final String FIELD_DOCUMENT_ID = "document_id";
    private static final String FIELD_DOCUMENT_NAME = "document_name";
    private static final String FIELD_DOCUMENT_FILE = "document_file";
    private static final String FIELD_IS_BLOCKED = "is_blocked";
    private static final String SQL_FIND_USER_BY_ID = "SELECT user_id, email, first_name, last_name, middle_name, password, role_name " +
            "FROM users JOIN roles ON users.role_id=roles.role_id WHERE user_id=?";
    private static final String SQL_FIND_USER_BY_EMAIL = "SELECT user_id, email, first_name, last_name, middle_name, password, role_name " +
            "FROM users JOIN roles ON users.role_id=roles.role_id WHERE email=?";
    private static final String SQL_FIND_USER_DETAILS_BY_ID = "SELECT ud.user_id, city, region, school FROM user_details as ud " +
            "WHERE ud.user_id=?";
    private static final String SQL_FIND_ALL_USERS_FILTERING = "SELECT user_id, email, first_name, last_name, middle_name, role_name " +
            "FROM users JOIN roles ON users.role_id=roles.role_id WHERE email LIKE ?";
    private static final String SQL_FIND_USER_DOCUMENTS = "SELECT document_id, document_name, document_file FROM user_documents " +
            "WHERE user_id=?";
    private static final String SQL_SAVE_USER_DETAILS = "INSERT INTO user_details VALUE (?,?,?,?) ON DUPLICATE KEY UPDATE region =?, city=?, school=?";
    private static final String SQL_GET_USER_DOCUMENT = "SELECT document_name, document_file FROM user_documents WHERE document_id=?";
    private static final String SQL_CREATE_DOCUMENT = "INSERT INTO user_documents VALUE (DEFAULT,?,?,?)";
    private static final String SQL_DELETE_DOCUMENT = "DELETE FROM user_documents WHERE document_id = ?";
    private static final String SQL_FIND_ALL_USERS_IN_BLACKLIST = "SELECT ubl.user_id, email, first_name, last_name, middle_name, role_name\n" +
            "FROM users_blacklist as ubl\n" +
            "         JOIN users ON ubl.user_id = users.user_id\n" +
            "         JOIN roles r on users.role_id = r.role_id";
    private static final String SQL_FIND_BLACKLIST_COMMENT = "SELECT comment FROM users_blacklist WHERE user_id = ?";
    private static final String SQL_BLOCK_USER = "INSERT INTO users_blacklist VALUE (?,?)";
    private static final String SQL_CHECK_IF_USER_IN_BLACKLIST = "SELECT COUNT(1) AS is_blocked\n" +
            "FROM users_blacklist WHERE user_id=?";
    private static final String SQL_UNBLOCK_USER = "DELETE FROM users_blacklist WHERE user_id = ?";
    private static final String SQL_CREATE_USER = "INSERT INTO users VALUE (DEFAULT,?,?,?,?,?, (SELECT role_id FROM roles WHERE role_name=?))";

    @Override
    public Optional<User> getUser(int userId) throws DAOException {
        if (userId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_USER_BY_ID)) {

            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(extractUser(rs, false));
            }
            return Optional.empty();

        } catch (SQLException e) {
            log.error("Exception in findUser method", e);
            throw new DAOException("Failed to find the user");
        }
    }

    @Override
    public Optional<User> getUserByEmail(String userEmail) throws DAOException {
        if (Objects.isNull(userEmail)) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_USER_BY_EMAIL)) {

            stmt.setString(1, userEmail.toLowerCase());
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(extractUser(rs, true));
            }
            return Optional.empty();
        } catch (SQLException e) {
            log.error("Exception in findUserByEmail method", e);
            throw new DAOException("Failed to find the user");
        }
    }

    @Override
    public Optional<UserDetails> getUserDetails(int userId) throws DAOException {
        if (userId <= 0) {
            return Optional.empty();
        }

        UserDetails userDetails;
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_USER_DETAILS_BY_ID)) {

            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                userDetails = new UserDetails.Builder()
                        .userId(userId)
                        .city(rs.getString(FIELD_CITY))
                        .region(rs.getString(FIELD_REGION))
                        .school(rs.getString(FIELD_SCHOOL))
                        .build();
                return Optional.of(userDetails);
            }
            return Optional.empty();
        } catch (SQLException e) {
            final String msg = "Failed to find the user details";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public List<User> getAllUsersFiltering(String email, String firstName, String lastName) throws DAOException {
        List<User> result = new ArrayList<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_USERS_FILTERING)) {

            stmt.setString(1, "%" + (email == null ? "" : email) + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(extractUser(rs, false));
            }
            return result;
        } catch (SQLException e) {
            final String msg = "Failed to find users";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public boolean saveUserDetails(UserDetails userDetails) throws DAOException {
        if (userDetails == null) {
            return false;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_SAVE_USER_DETAILS)) {

            int k = 0;
            stmt.setInt(++k, userDetails.getUserId());
            stmt.setString(++k, userDetails.getRegion());
            stmt.setString(++k, userDetails.getCity());
            stmt.setString(++k, userDetails.getSchool());
            stmt.setString(++k, userDetails.getRegion());
            stmt.setString(++k, userDetails.getCity());
            stmt.setString(++k, userDetails.getSchool());

            if (stmt.executeUpdate() > 0) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            String msg = "Failed to save user details";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public List<Document> getUserDocuments(int userId) throws DAOException {
        List<Document> result = new ArrayList<>();
        if (userId <= 0) {
            return result;
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_USER_DOCUMENTS)) {

            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(new Document.Builder()
                        .id(rs.getInt(FIELD_DOCUMENT_ID))
                        .name(rs.getString(FIELD_DOCUMENT_NAME))
                        .build());
            }
            return result;

        } catch (SQLException e) {
            final String msg = "Failed to find the user documents";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Optional<String> getUserDocumentFile(int documentId) throws DAOException {
        if (documentId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_GET_USER_DOCUMENT)) {

            stmt.setInt(1, documentId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(rs.getString(FIELD_DOCUMENT_FILE));
            }
            return Optional.empty();

        } catch (SQLException e) {
            final String msg = "Failed to find the user document";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Optional<Document> getUserDocument(int documentId) throws DAOException {
        if (documentId <= 0) {
            return Optional.empty();
        }

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_GET_USER_DOCUMENT)) {

            stmt.setInt(1, documentId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(new Document.Builder()
                        .id(documentId)
                        .name(rs.getString(FIELD_DOCUMENT_NAME))
                        .build());
            }
            return Optional.empty();

        } catch (SQLException e) {
            final String msg = "Failed to find the user document";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Optional<User> createUser(User user) throws DAOException {
        if (user == null) {
            return Optional.empty();
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = connectionBuilder.getConnection();
            stmt = conn.prepareStatement(SQL_CREATE_USER,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 0;
            stmt.setString(++k, user.getEmail());
            stmt.setString(++k, user.getFirstName());
            stmt.setString(++k, user.getLastName());
            stmt.setString(++k, user.getMiddleName());
            stmt.setString(++k, user.getPassword());
            stmt.setString(++k, user.getRole().toString());

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    User createdUser = new User.Builder()
                            .id(rs.getInt(1))
                            .email(user.getEmail())
                            .firstName(user.getFirstName())
                            .lastName(user.getLastName())
                            .middleName(user.getMiddleName())
                            .role(user.getRole())
                            .build();
                    log.debug("New user saved, userId = {}", createdUser.getId());
                    return Optional.of(createdUser);
                }
            }
            return Optional.empty();

        } catch (SQLException e) {
            log.error("Failed to save user", e);
            throw new DAOException("Failed to save user");
        } finally {
            closeResources(rs, stmt, conn);
        }
    }

    private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn) {
        final String errMsg = "Failed to close resources";
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.error(errMsg, e);
            }
        }
    }

    @Override
    public Optional<Document> saveUserDocument(int userId, String name, String path) throws DAOException {
        if (userId <= 0 || name == null || path == null || name.isEmpty() || path.isEmpty()) {
            return Optional.empty();
        }

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = connectionBuilder.getConnection();
            stmt = conn.prepareStatement(SQL_CREATE_DOCUMENT,
                    Statement.RETURN_GENERATED_KEYS);

            int k = 0;
            stmt.setInt(++k, userId);
            stmt.setString(++k, name);
            stmt.setString(++k, path);

            if (stmt.executeUpdate() > 0) {
                rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    Document document = new Document.Builder()
                            .id(rs.getInt(1))
                            .name(name)
                            .build();
                    log.debug("New document saved: {}", document);
                    return Optional.of(document);
                }
            }
            return Optional.empty();
        } catch (SQLException e) {
            String msg = "Failed to save document to database";
            log.error(msg, e);
            throw new DAOException(msg);
        } finally {
            closeResources(rs, stmt, conn);
        }
    }

    @Override
    public boolean deleteUserDocument(int documentId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_DELETE_DOCUMENT)) {

            stmt.setInt(1, documentId);
            stmt.executeUpdate();
            log.debug("Document with id={} was removed from database", documentId);
            return true;
        } catch (SQLException e) {
            final String msg = "Failed to delete user document";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public boolean blockUser(int userId, String comment) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_BLOCK_USER)) {
            int k = 0;
            stmt.setInt(++k, userId);
            stmt.setString(++k, comment);
            stmt.executeUpdate();
            log.info("User id={} was added to the blacklist", userId);
            return true;
        } catch (SQLException e) {
            final String msg = "Failed to add user to the blacklist. User id = " + userId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public boolean unblockUser(int userId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_UNBLOCK_USER)) {

            stmt.setInt(1, userId);
            stmt.executeUpdate();
            log.info("User id={} was removed from the blacklist", userId);
            return true;
        } catch (SQLException e) {
            final String msg = "Failed to remove user from the blacklist. User id = " + userId;
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public boolean isUserBlocked(int userId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_CHECK_IF_USER_IN_BLACKLIST)) {

            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return rs.getInt(FIELD_IS_BLOCKED) == 1;
            }
            throw new SQLException("Impossible result");

        } catch (SQLException e) {
            final String msg = "Failed to check if user is in the blacklist";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public List<User> getBlackListedUsers() throws DAOException {
        List<User> result = new ArrayList<>();

        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_ALL_USERS_IN_BLACKLIST)) {

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                result.add(extractUser(rs, false));
            }
            return result;
        } catch (SQLException e) {
            final String msg = "Failed to find users in blacklist";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    @Override
    public Optional<String> getBlacklistComment(int userId) throws DAOException {
        try (Connection conn = connectionBuilder.getConnection();
             PreparedStatement stmt = conn.prepareStatement(SQL_FIND_BLACKLIST_COMMENT)) {

            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return Optional.of(rs.getString(FIELD_COMMENT));
            }
            return Optional.empty();

        } catch (SQLException e) {
            final String msg = "Failed to get blacklist comment";
            log.error(msg, e);
            throw new DAOException(msg);
        }
    }

    private User extractUser(ResultSet rs, boolean extractWithPassword) throws SQLException {
        User.Builder ub = new User.Builder()
                .id(rs.getInt(FIELD_USER_ID))
                .email(rs.getString(FIELD_EMAIL))
                .firstName(rs.getString(FIELD_FIRST_NAME))
                .middleName(rs.getString(FIELD_MIDDLE_NAME))
                .lastName(rs.getString(FIELD_LAST_NAME))
                .role(Roles.valueOf(rs.getString(FIELD_ROLE)));

        if (extractWithPassword) {
            ub = ub.password(rs.getString(FIELD_PASSWORD));
        }

        return ub.build();
    }
}
