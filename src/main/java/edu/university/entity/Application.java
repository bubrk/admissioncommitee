package edu.university.entity;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Represents users application for educational programme
 */
public class Application implements Serializable {
    private int id;
    private int userId;
    @NotNull(message = "Scores must contain at least one subject")
    @Size(min = 1,message = "Scores must contain at least one subject")
    Map<Subject,Score> scores;
    private int programmeId;

    Application (Builder builder){
        this.id = builder.id;
        this.userId = builder.userId;
        this.programmeId = builder.programmeId;
        this.scores = builder.scores;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public int getProgrammeId() {
        return programmeId;
    }

    public Map<Subject, Score> getScores() {
        return scores;
    }

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", userId=" + userId +
                ", programmeId=" + programmeId +
                ", scores=" + scores +
                '}';
    }

    public static class Builder{
        Map<Subject,Score> scores;
        private int id;
        private int userId;
        private int programmeId;

        public Builder id(int id){
            this.id = id;
            return this;
        }

        public Builder userId(int userId){
            this.userId = userId;
            return this;
        }

        public Builder programmeId(int programmeId){
            this.programmeId = programmeId;
            return this;
        }

        public Builder scores(Map<Subject,Score> scores){
            this.scores = scores;
            return this;
        }

        public Application build(){
            return new Application(this);
        }
    }
}
