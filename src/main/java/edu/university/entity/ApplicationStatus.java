package edu.university.entity;

import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import java.util.Date;

/**
 * Contains information about status of the application
 */
public class ApplicationStatus {
    int id;
    Statuses status;
    Date dateOfCreation;
    @MaxLength(length=255,message = "Comment must be less then 255 characters long")
    String comment;

    ApplicationStatus(Builder builder) {
        this.id = builder.id;
        this.status = builder.status;
        this.dateOfCreation = builder.dateOfCreation;
        this.comment = builder.comment;
    }

    public int getId() {
        return id;
    }

    public Statuses getStatus() {
        return status;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return "ApplicationStatus{" +
                "id=" + id +
                ", status=" + status +
                ", dateOfCreation=" + dateOfCreation +
                ", comment='" + comment + '\'' +
                '}';
    }

    public enum Statuses {
        NEW,
        ACCEPTED,
        REJECTED,
        ENROLLED
    }

    public static class Builder {
        int id;
        Statuses status;
        Date dateOfCreation;
        String comment;

        public Builder id(int id){
            this.id = id;
            return this;
        }

        public Builder status(Statuses status){
            this.status = status;
            return this;
        }

        public Builder dateOfCreation(Date dateOfCreation){
            this.dateOfCreation = dateOfCreation;
            return this;
        }

        public Builder comment(String comment){
            this.comment = comment;
            return this;
        }

        public ApplicationStatus build(){
            return new ApplicationStatus(this);
        }
    }
}
