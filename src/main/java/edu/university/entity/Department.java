package edu.university.entity;

import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import java.io.Serializable;
import java.util.Objects;

import static edu.university.entity.validator.RegexMatch.PATTERN_NAME;
import static edu.university.entity.validator.RegexMatch.PATTERN_TEXT;

/**
 * Represents department, contains basic information like about name, description. Departments can be nested,
 * parentId is an id of container for current department
 */
public class Department implements Serializable {
    private final int id;
    private final int parentId;

    @NotNullNotEmpty(message = "Department name can't be empty")
    @RegexMatch(pattern = PATTERN_TEXT,message = "Forbidden symbol(s) is used for department name")
    @MaxLength(length=150,message = "Department name must be less then 150 characters long")
    private final String name;

    @NotNullNotEmpty(message = "Department name can't be empty")
    @RegexMatch(pattern = PATTERN_TEXT,message = "Forbidden symbol(s) is used for department description")
    @MaxLength(length=255,message = "Description must be less then 255 characters long")
    private final String description;

    public Department(Builder builder) {
        this.id = builder.id;
        this.parentId = builder.parentId;
        this.name = builder.name;
        this.description = builder.description;
    }

    public int getId() {
        return id;
    }

    public int getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public static class Builder{
        private int id;
        private int parentId;
        private String name;
        private String description;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder parentId(int parentId) {
            this.parentId = parentId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description){
            this.description = description;
            return this;
        }

        public Department build(){
            return new Department(this);
        }

    }
}
