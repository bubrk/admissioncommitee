package edu.university.entity;

import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import java.io.Serializable;
import java.util.Objects;

import static edu.university.entity.validator.RegexMatch.PATTERN_NAME;

/**
 * Represents uploaded file or document
 */
public class Document implements Serializable {
    private int id;
    @NotNullNotEmpty(message = "Document name can't be empty")
    @MaxLength(length=200,message = "Document name must be less then 200 characters long")
    private String name;
    @MaxLength(length=40,message = "Document file name must be less then 40 characters long")
    private String file;

    Document (Builder builder){
        this.id = builder.id;
        this.name = builder.name;
        this.file = builder.file;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return id == document.id && Objects.equals(name, document.name) && Objects.equals(file, document.file);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, file);
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", file='" + file + '\'' +
                '}';
    }

    public static class Builder{
        private int id;
        private String name;
        private String file;

        public Builder id(int id){
            this.id = id;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder file(String file){
            this.file = file;
            return this;
        }

        public Document build(){
            return new Document (this);
        }
    }
}
