package edu.university.entity;

import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import java.io.Serializable;
import java.util.Objects;

import static edu.university.entity.validator.RegexMatch.PATTERN_NAME;
import static edu.university.entity.validator.RegexMatch.PATTERN_TEXT;

/**
 * Represents educational programme
 */
public class Programme implements Serializable {
   private final int id;
   private final int departmentId;

   @NotNullNotEmpty(message = "Programme name can't be empty")
   @RegexMatch(pattern = PATTERN_TEXT,message = "Forbidden symbol(s) is used for programme name")
   @MaxLength(length=150,message = "Programme name be less then 150 characters long")
   private final String name;

   @NotNullNotEmpty(message = "Programme name can't be empty")
   @RegexMatch(pattern = PATTERN_TEXT,message = "Forbidden symbol(s) is used for programme short description")
   @MaxLength(length=255,message = "Short description must be less then 255 characters long")
   private final String shortDescription;

   public Programme(Builder builder) {
      this.id = builder.id;
      this.departmentId = builder.departmentId;
      this.name = builder.name;
      this.shortDescription = builder.shortDescription;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Programme programme = (Programme) o;
      return id == programme.id;
   }

   @Override
   public int hashCode() {
      return Objects.hash(id);
   }

   @Override
   public String toString() {
      return "Programme{" +
              "id=" + id +
              ", departmentId=" + departmentId +
              ", name='" + name + '\'' +
              ", shortDescription='" + shortDescription + '\'' +
              '}';
   }

   public int getId() {
      return id;
   }

   public int getDepartmentId() {
      return departmentId;
   }

   public String getName() {
      return name;
   }

   public String getShortDescription() {
      return shortDescription;
   }

   public static class Builder{
      private int id;
      private int departmentId;
      private String name;
      private String shortDescription;

      public Builder id(int id) {
         this.id = id;
         return this;
      }

      public Builder departmentId(int departmentId) {
         this.departmentId = departmentId;
         return this;
      }

      public Builder name(String name) {
         this.name = name;
         return this;
      }

      public Builder shortDescription(String shortDescription) {
         this.shortDescription = shortDescription;
         return this;
      }


      public Programme build(){
         return new Programme(this);
      }
   }

}
