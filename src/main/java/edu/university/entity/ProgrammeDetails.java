package edu.university.entity;

import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Date;

import static edu.university.entity.validator.RegexMatch.PATTERN_TEXT;

/**
 * Contains additional information about educational programme
 */
public class ProgrammeDetails implements Serializable {
    @NotNullNotEmpty(message = "Programme description can't be empty")
    @RegexMatch(pattern = PATTERN_TEXT,message = "Forbidden symbol(s) is used for programme description")
    @MaxLength(length=500,message = "Description must be less then 500 characters long")
    private final String description;

    private final Date admissionsStartDate;
    private final Date admissionsEndDate;

    @Min(value = 0,message = "Amount of budget places must be 0 or positive")
    private final int budgetPlaces;
    @Min(value = 1,message = "Amount of total places must be positive")
    private final int totalPlaces;

    public ProgrammeDetails(Builder builder){
        this.description = builder.description;
        this.admissionsStartDate = builder.admissionsStartDate;
        this.admissionsEndDate = builder.admissionsEndDate;
        this.budgetPlaces = builder.budgetPlaces;
        this.totalPlaces = builder.totalPlaces;
    }

    public String getDescription() {
        return description;
    }

    public Date getAdmissionsStartDate() {
        return admissionsStartDate;
    }

    public Date getAdmissionsEndDate() {
        return admissionsEndDate;
    }

    public int getBudgetPlaces() {
        return budgetPlaces;
    }

    public int getTotalPlaces() {
        return totalPlaces;
    }

    @Override
    public String toString() {
        return "ProgrammeDetails{" +
                "description='" + description + '\'' +
                ", admissionsStartDate=" + admissionsStartDate +
                ", admissionsEndDate=" + admissionsEndDate +
                ", budgetPlaces=" + budgetPlaces +
                ", totalPlaces=" + totalPlaces +
                '}';
    }

    public static class Builder{
        private String description;
        private Date admissionsStartDate;
        private Date admissionsEndDate;
        private int budgetPlaces;
        private int totalPlaces;

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder admissionsStartDate(Date admissionsStartDate) {
            this.admissionsStartDate = admissionsStartDate;
            return this;
        }

        public Builder admissionsEndDate(Date admissionsEndDate) {
            this.admissionsEndDate = admissionsEndDate;
            return this;
        }

        public Builder budgetPlaces(int budgetPlaces) {
            this.budgetPlaces = budgetPlaces;
            return this;
        }

        public Builder totalPlaces(int totalPlaces) {
            this.totalPlaces = totalPlaces;
            return this;
        }

        public ProgrammeDetails build(){
            return new ProgrammeDetails(this);
        }
    }
}
