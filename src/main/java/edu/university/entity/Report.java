package edu.university.entity;

import java.util.List;
import java.util.Map;

/**
 * Represents final report for educational programme
 */
public class Report {
    private Programme programme;
    private ProgrammeDetails programmeDetails;
    private List<Map<String,String>> reportRows;

    public Report(Builder builder) {
        this.programme = builder.programme;
        this.programmeDetails = builder.programmeDetails;
        this.reportRows = builder.reportRows;
    }

    public Programme getProgramme() {
        return programme;
    }

    public ProgrammeDetails getProgrammeDetails() {
        return programmeDetails;
    }

    public List<Map<String, String>> getReportRows() {
        return reportRows;
    }

    public static class Builder {
        private Programme programme;
        private ProgrammeDetails programmeDetails;
        private List<Map<String,String>> reportRows;

        public Builder programme(Programme programme) {
            this.programme = programme;
            return this;
        }

        public Builder programmeDetails(ProgrammeDetails programmeDetails) {
            this.programmeDetails = programmeDetails;
            return this;
        }

        public Builder reportRows(List<Map<String,String>> reportRows){
            this.reportRows = reportRows;
            return this;
        }

        public Report build(){
            return new Report(this);
        }
    }

}
