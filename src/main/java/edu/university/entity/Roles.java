package edu.university.entity;

import java.io.Serializable;

/**
 * Enum <code>Roles</code> describes role of the user. <br>
 * USER - users who went successfully through the verification process <br>
 * ADMIN - verified users who have rights to edit content<br>
 */
public enum Roles {
        USER,
        ADMIN
}