package edu.university.entity;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * Represents score for school subject
 */
public class Score implements Serializable {
    @Min(value = 100, message = "Score can't be less then 100")
    @Max(value = 200, message = "Score can't be more then 200")
    private final int theScore;

    public Score(int score) {
        this.theScore = score;
    }

    public int getScore() {
        return theScore;
    }

    @Override
    public String toString() {
        return "Score = " + theScore;
    }
}
