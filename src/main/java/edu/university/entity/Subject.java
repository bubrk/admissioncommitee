package edu.university.entity;

import edu.university.entity.validator.RegexMatch;
import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;

import java.io.Serializable;
import java.util.Objects;

import static edu.university.entity.validator.RegexMatch.PATTERN_NAME;

/**
 * Represents school subject
 */
public class Subject implements Serializable {
    private int id;

    @NotNullNotEmpty(message = "Subject name can't be empty")
    @RegexMatch(pattern = PATTERN_NAME, message = "Forbidden symbol(s) is used for subject name")
    @MaxLength(length = 40, message = "Subject name must be less then 40 characters long")
    private String name;

    Subject(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subject subject = (Subject) o;
        return id == subject.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public static class Builder {
        private int id;
        private String name;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Subject build() {
            return new Subject(this);
        }
    }
}
