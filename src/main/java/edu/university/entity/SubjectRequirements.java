package edu.university.entity;

import edu.university.entity.validator.MaxDouble;
import edu.university.entity.validator.MinDouble;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * The requirements for educational programme - list of subjects with requirement of minimal score and
 * coefficient of participation in total rate
 */
public class SubjectRequirements implements Serializable {
    @NotNull(message = "Requirements must contain at least one subject")
    @Size(min = 1,message = "Requirements must contain at least one subject")
    private List<Subject> subjects;
    @Min(value = 100, message = "Score can't be less then 100")
    @Max(value = 200, message = "Score can't be more then 200")
    private int minScore;

    @MinDouble(value = 0.01, message = "Coefficient must be at least 0.01")
    @MaxDouble(value = 1.0, message = "Coefficient can't be more than 1")
    private double coefficient;

    SubjectRequirements(Builder builder){
        this.subjects = builder.subjects;
        this.minScore = builder.minScore;
        this.coefficient = builder.coefficient;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public int getMinScore() {
        return minScore;
    }

    public double getCoefficient() {
        return coefficient;
    }

    @Override
    public String toString() {
        return "SubjectRequirements{" +
                "subjects=" + subjects +
                ", minScore=" + minScore +
                ", coefficient=" + coefficient +
                '}';
    }

    public static class Builder{
        private List<Subject> subjects;
        private int minScore;
        private double coefficient;

        public Builder subjects(List<Subject> subjects){
            this.subjects = subjects;
            return this;
        }

        public Builder minScore(int minScore){
            this.minScore = minScore;
            return this;
        }

        public Builder coefficient(double coefficient){
            this.coefficient = coefficient;
            return this;
        }

        public SubjectRequirements build(){
            return new SubjectRequirements(this);
        }
    }
}
