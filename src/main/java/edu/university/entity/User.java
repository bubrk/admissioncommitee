package edu.university.entity;

import edu.university.entity.validator.Email;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

import static edu.university.entity.validator.RegexMatch.PATTERN_NAME;
import static edu.university.entity.validator.RegexMatch.PATTERN_NAME_STRICT;

/**
 * Class {@code User} represents a person, contains some basic information about this person
 */
public class User implements Serializable {
    private final int id;

    @Email
    @NotNullNotEmpty(message = "Email name can't be empty")
    @Size(max = 100, message = "Email must be less than 100 characters long")
    private final String email;

    @NotNullNotEmpty(message = "First name can't be empty")
    @RegexMatch(pattern = PATTERN_NAME_STRICT, message = "Forbidden symbol(s) is used for the first name")
    @Size(min = 3, max = 40, message = "First name must be between 3 and 40 characters long")
    private final String firstName;

    @NotNullNotEmpty(message = "Last name can't be empty")
    @RegexMatch(pattern = PATTERN_NAME_STRICT, message = "Forbidden symbol(s) is used for the last name")
    @Size(min = 3, max = 40, message = "Last name must be between 3 and 40 characters long")
    private final String lastName;

    @RegexMatch(pattern = PATTERN_NAME_STRICT, message = "Forbidden symbol(s) is used for the middle name")
    @Size(max = 40, message = "Middle name must less than 40 characters long")
    private final String middleName;

    private final Roles role;

    @NotNull
    @Size(min = 6, max = 100, message = "Password must be between 6 and 100 characters long")
    private transient String password;

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPassword() {
        return password;
    }

    public Roles getRole() {
        return role;
    }

    public User(Builder builder) {
        this.id = builder.id;
        this.email = builder.email;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.middleName = builder.middleName == null ? "" : builder.middleName;
        this.password = builder.password == null ? "" : builder.password;
        this.role = builder.role == null ? Roles.USER : builder.role;
    }

    public void emptyPassword() {
        this.password = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }

    public static class Builder {
        private int id;
        private String email;
        private String firstName;
        private String lastName;
        private String middleName;
        private String password;
        private Roles role;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder role(Roles role) {
            this.role = role;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                '}';
    }


}


