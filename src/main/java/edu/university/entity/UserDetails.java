package edu.university.entity;

import edu.university.entity.validator.MaxLength;
import edu.university.entity.validator.NotNullNotEmpty;
import edu.university.entity.validator.RegexMatch;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static edu.university.entity.validator.RegexMatch.PATTERN_NAME;

/**
 * Contains detailed information about the person
 */
public class UserDetails implements Serializable {
    private int userId;

    @NotNullNotEmpty(message = "City can't be empty")
    @RegexMatch(pattern = PATTERN_NAME, message = "Forbidden symbol(s) is used for city name")
    @MaxLength(length=100,message = "City name must be less then 100 characters long")
    private String city;

    @NotNullNotEmpty(message = "Region can't be empty")
    @RegexMatch(pattern = PATTERN_NAME, message = "Forbidden symbol(s) is used for region name")
    @MaxLength(length=100,message = "Region name must be less then 100 characters long")
    private String region;

    @NotNullNotEmpty(message = "School can't be empty")
    @RegexMatch(pattern = PATTERN_NAME, message = "Forbidden symbol(s) is used for school name")
    @MaxLength(length=100,message = "School name must be less then 100 characters long")
    private String school;

    UserDetails(Builder builder) {
        this.userId = builder.userId;
        this.city = builder.city;
        this.region = builder.region;
        this.school = builder.school;
    }

    public int getUserId() {
        return userId;
    }

    public String getCity() {
        return city;
    }

    public String getRegion() {
        return region;
    }

    public String getSchool() {
        return school;
    }


    @Override
    public String toString() {
        return "UserDetails{" +
                "userId=" + userId +
                ", city='" + city + '\'' +
                ", region='" + region + '\'' +
                ", school='" + school + '\'' +
                '}';
    }


    public static class Builder {
        private int userId;
        private String city;
        private String region;
        private String school;

        public Builder userId(int userId){
            this.userId = userId;
            return this;
        }
        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder region(String region) {
            this.region = region;
            return this;
        }

        public Builder school(String school) {
            this.school = school;
            return this;
        }

        public UserDetails build() {
            return new UserDetails(this);
        }
    }
}
