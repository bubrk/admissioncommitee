package edu.university.entity.dto;

import edu.university.entity.*;

import java.util.List;
import java.util.Map;

/**
 * Used for collecting results of searches for application
 */
public class ApplicationSearchResult {
    private Application application;
    private ApplicationStatus currentStatus;
    private List<ApplicationStatus> statusFlow;
    private Programme programme;
    private User user;
    private Map<Subject, Score> scores;

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public ApplicationStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(ApplicationStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    public List<ApplicationStatus> getStatusFlow() {
        return statusFlow;
    }

    public void setStatusFlow(List<ApplicationStatus> statusFlow) {
        this.statusFlow = statusFlow;
    }

    public Programme getProgramme() {
        return programme;
    }

    public void setProgramme(Programme programme) {
        this.programme = programme;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Map<Subject, Score> getScores() {
        return scores;
    }

    public void setScores(Map<Subject, Score> scores) {
        this.scores = scores;
    }

}
