package edu.university.entity.dto;

/**
 * Used for separating results into discrete pages
 */
public class Paginator {
    private int totalEntries;
    private int currentPage = 1;
    private int pageSize = 10;

    public int getTotalEntries() {
        return totalEntries;
    }

    public void setTotalEntries(int totalEntries) {
        this.totalEntries = totalEntries;
    }

    public int getTotalPages() {
        if (pageSize != 0) {
            return (int) Math.ceil((double) totalEntries / pageSize);
        }
        return 0;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
