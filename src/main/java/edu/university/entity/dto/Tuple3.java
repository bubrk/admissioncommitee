package edu.university.entity.dto;

/**
 *  Collection of three elements that may or may not be related to each other
 */
public class Tuple3 <K,V,T>{
    private K first;
    private V second;
    private T third;

    public Tuple3(K first, V second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public K getFirst() {
        return first;
    }

    public void setFirst(K first) {
        this.first = first;
    }

    public V getSecond() {
        return second;
    }

    public void setSecond(V second) {
        this.second = second;
    }

    public T getThird() {
        return third;
    }

    public void setThird(T third) {
        this.third = third;
    }
}
