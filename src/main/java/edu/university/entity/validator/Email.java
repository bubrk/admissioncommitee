package edu.university.entity.validator;

import org.apache.commons.validator.routines.EmailValidator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Used for validating fields with email address
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = Email.EmailValidation.class)
public @interface Email {
    String message() default "Email is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default{};

    class EmailValidation implements ConstraintValidator<Email,String> {


        @Override
        public void initialize(Email constraintAnnotation) {

        }

        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return EmailValidator.getInstance()
                    .isValid(value);
        }
    }
}
