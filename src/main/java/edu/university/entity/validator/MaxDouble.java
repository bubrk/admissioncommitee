package edu.university.entity.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used for validating double values
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MaxDouble.MaxDoubleValidation.class)
public @interface MaxDouble {
    double value();

    String message() default "The value is too big";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class MaxDoubleValidation implements ConstraintValidator<MaxDouble, Double> {
        double maxDouble;

        @Override
        public void initialize(MaxDouble constraintAnnotation) {

            this.maxDouble = constraintAnnotation.value();
        }

        @Override
        public boolean isValid(Double value, ConstraintValidatorContext context) {
            return (value <= maxDouble);
        }
    }
}
