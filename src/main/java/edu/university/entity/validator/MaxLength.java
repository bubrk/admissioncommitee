package edu.university.entity.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Used for validating double values
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MaxLength.MaxLengthValidation.class)
public @interface MaxLength {
    int length();

    String message() default "The string is too long";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class MaxLengthValidation implements ConstraintValidator<MaxLength, String> {
        int length;

        @Override
        public void initialize(MaxLength constraintAnnotation) {

            this.length = constraintAnnotation.length();
        }

        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {

            return (value == null || value.length() <= length);
        }
    }
}
