package edu.university.entity.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used for validating max length of the fields containing strings
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MinDouble.MaxDoubleValidation.class)
public @interface MinDouble {
    double value();

    String message() default "The value is too big";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class MaxDoubleValidation implements ConstraintValidator<MinDouble, Double> {
        double minDouble;

        @Override
        public void initialize(MinDouble constraintAnnotation) {

            this.minDouble = constraintAnnotation.value();
        }

        @Override
        public boolean isValid(Double value, ConstraintValidatorContext context) {

            return (value >= minDouble);
        }
    }
}
