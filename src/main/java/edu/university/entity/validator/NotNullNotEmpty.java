package edu.university.entity.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Used for validating fields for null and empty strings
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotNullNotEmpty.NotNullNotEmptyValidator.class)
public @interface NotNullNotEmpty {
    String message() default "Value can't be null or empty";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class NotNullNotEmptyValidator implements ConstraintValidator<NotNullNotEmpty, String> {

        @Override
        public void initialize(NotNullNotEmpty constraintAnnotation) {

        }

        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return !(value == null || value.isEmpty());
        }
    }
}
