package edu.university.entity.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used for validating strings by checking for allowed characters
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RegexMatch.CharactersValidation.class)
public @interface RegexMatch {
    String pattern();

    String message() default "Not allowed character";

    String PATTERN_NAME = "^[A-zА-яіїє'\\s\\-]*$";
    String PATTERN_NAME_STRICT = "^[A-zА-яіїє'\\-]*$";
    String PATTERN_TEXT = "^[А-яєії\\w\\d\\s\\-.,:;'\"\\(\\)]*$";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class CharactersValidation implements ConstraintValidator<RegexMatch, String> {
        String pattern;

        @Override
        public void initialize(RegexMatch constraintAnnotation) {

            this.pattern = constraintAnnotation.pattern();
        }

        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {
            if (value==null || value.isEmpty()){
                return true;
            }
            Pattern regexPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher matcher = regexPattern.matcher(value);

            return (matcher.find());
        }
    }
}
