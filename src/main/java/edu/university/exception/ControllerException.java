package edu.university.exception;

/**
 * Thrown when an exceptional condition has occurred inside the Controller
 */
public class ControllerException extends Exception{

    public ControllerException(String message) {
        super(message);
    }

    public ControllerException(String message, Throwable cause) {
        super(message, cause);
    }
}
