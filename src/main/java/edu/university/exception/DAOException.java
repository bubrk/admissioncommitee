package edu.university.exception;

/**
 * Thrown when an exceptional condition has occurred while working with database
 */
public class DAOException extends Exception{

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
