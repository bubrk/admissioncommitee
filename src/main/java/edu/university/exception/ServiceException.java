package edu.university.exception;

/**
 * Thrown when an exceptional condition has occurred in the service layer
 */
public class ServiceException extends Exception{
    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
