package edu.university.exception;

/**
 * Thrown when failed to validate given objects, fields or passed parameters
 */
public class ValidationException extends Exception{
    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
