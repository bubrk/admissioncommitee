package edu.university.service;

import edu.university.entity.Application;
import edu.university.entity.Score;
import edu.university.entity.Subject;
import edu.university.entity.User;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.ServiceException;

import edu.university.exception.ValidationException;
import java.util.List;
import java.util.Map;

public interface ApplicationService {
    /**
     * Returns user's application by given application id
     * @param applicationId id of the application
     * @param user user requesting an application
     * @return {@link ApplicationSearchResult} result set of the application and related data
     * @throws ValidationException if application with given id does not belong to given user and user is not admin
     * @throws ServiceException if an error occurred while processing data
     */
    public ApplicationSearchResult getApplicationById(int applicationId, User user) throws ValidationException, ServiceException;

    /**
     * Returns all user's application by given user id
     * @param userId id of the user
     * @return List of {@link ApplicationSearchResult} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<ApplicationSearchResult> getApplicationsByUserId(int userId) throws ServiceException;

    /**
     * Searches for all applications
     * @return List of {@link ApplicationSearchResult} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<ApplicationSearchResult> getAllApplications() throws ServiceException;

    /**
     * Saves application
     * @param application given application
     * @param scores Map of subjects with scores
     * @return true if saved successfully
     * @throws ValidationException thrown invalid date given in the application or scores
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean saveApplication(Application application, Map<Subject, Score> scores) throws ValidationException, ServiceException;

    /**
     * Returns true if user has any active applications (new or accepted) for given programme
     * @param userId id of the user
     * @param programmeId id of programme
     * @return true if any active applications found
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean hasActiveApplication(int userId, int programmeId) throws ServiceException;

    /**
     * Changes status for application with given id to ACCEPTED
     * @param applicationId id of the application
     * @param comment optional comment for status
     * @return true if saved successfully
     * @throws ValidationException thrown if invalid date was passed
     * @throws ServiceException if an error occurred while processing data
     * @see Application
     */
    public boolean acceptApplication(int applicationId, String comment) throws ValidationException, ServiceException;

    /**
     * Changes status for application with given id to REJECTED
     * @param applicationId id of the application
     * @param comment optional comment for status
     * @return true if saved successfully
     * @throws ValidationException thrown if invalid date was passed
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean rejectApplication(int applicationId, String comment) throws ValidationException, ServiceException;
}
