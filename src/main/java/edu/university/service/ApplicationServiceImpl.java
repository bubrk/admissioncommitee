package edu.university.service;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.ApplicationDAO;
import edu.university.dao.DAOFactory;
import edu.university.entity.*;
import edu.university.entity.dto.ApplicationSearchResult;
import edu.university.exception.DAOException;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.university.exception.ValidationException;
import java.util.List;
import java.util.Map;


public class ApplicationServiceImpl implements ApplicationService {
    @InjectLogger
    private Logger log;
    @InjectByType
    private ApplicationDAO applicationDAO;

    private static final String ERROR_MESSAGE = "Error occurred while working with database, please try again later";


    @Override
    public ApplicationSearchResult getApplicationById(int applicationId, User user) throws ServiceException, ValidationException {
        try {
            ApplicationSearchResult result = applicationDAO.getApplication(applicationId)
                    .orElseThrow(() -> new SecurityException("Failed to find application with given id"));
            if (user.getId() != result.getUser().getId() &&
                    !user.getRole().equals(Roles.ADMIN)) {
                throw new ValidationException("Access not granted");
            }
            result.setScores(applicationDAO.getApplicationScores(applicationId));

            return result;

        } catch (DAOException e) {
            log.error("Failed to find applications", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<ApplicationSearchResult> getApplicationsByUserId(int userId) throws ServiceException {
        try {
            return applicationDAO.getUsersApplications(userId);
        } catch (DAOException e) {
            log.error("Failed to find applications", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<ApplicationSearchResult> getAllApplications() throws ServiceException {
        try {
            return applicationDAO.getAllApplications();

        } catch (DAOException e) {
            log.error("Failed to find applications", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean saveApplication(Application application, Map<Subject, Score> scores) throws ValidationException, ServiceException {
        //TODO Validation
        //TODO check dates
        try {
            return applicationDAO.createApplication(application, scores)
                    .isPresent();
        } catch (DAOException e) {
            log.error("Failed to create application", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

    }

    @Override
    public boolean hasActiveApplication(int userId, int programmeId) throws ServiceException {
        try {
            List<ApplicationSearchResult> applications = applicationDAO.getUsersApplications(userId);
            for (ApplicationSearchResult application : applications) {
                if (application.getProgramme().getId() == programmeId
                        && isActiveStatus(application.getCurrentStatus())) {
                    return true;
                }
            }
            return false;

        } catch (DAOException e) {
            log.error("Failed to create application with applicationDAO", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean acceptApplication(int applicationId, String comment) throws ValidationException, ServiceException {
        return changeApplicationStatus(applicationId,
                ApplicationStatus.Statuses.ACCEPTED,
                comment);
    }

    @Override
    public boolean rejectApplication(int applicationId, String comment) throws ValidationException, ServiceException {
        return changeApplicationStatus(applicationId,
                ApplicationStatus.Statuses.REJECTED,
                comment);
    }

    private boolean changeApplicationStatus(int applicationId, ApplicationStatus.Statuses status, String comment) throws ValidationException, ServiceException{
        try {
            //TODO Validation
            if (comment.length()>255){
                throw new ValidationException("Comment is to long");
            }
            return applicationDAO.changeApplicationStatus(applicationId,
                    status,
                    comment);

        } catch (DAOException e) {
            log.error("Failed to change application status", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    private boolean isActiveStatus(ApplicationStatus currentStatus) {
        return (currentStatus.getStatus().equals(ApplicationStatus.Statuses.NEW) ||
                currentStatus.getStatus().equals(ApplicationStatus.Statuses.ACCEPTED));
    }
}
