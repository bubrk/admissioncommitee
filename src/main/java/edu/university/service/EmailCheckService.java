package edu.university.service;

/**
 * Used to check if given string is actually a valid email address
 */
public class EmailCheckService {

    /**
     * Method checks if string <code>email</code> is a valid email address<br>
     * Method doesn't check if given email address actually exists<br>
     * @param email string which contains an email address
     * @return <code>true</code> if <code>email</code> is a valid email address
     */
    public static boolean isEmailValid(String email){
        return org.apache.commons.validator.routines.EmailValidator.getInstance()
                .isValid(email);

    }

    /**
     * Method checks if string <code>email</code> is a valid email address<br>
     * Method doesn't check if given email address actually exists<br>
     * @param email string which contains an email address
     * @return <code>true</code> if <code>email</code> is NOT valid email address
     */
    public static boolean isEmailNotValid(String email){
        return !isEmailValid(email);
    }
}
