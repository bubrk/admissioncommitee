package edu.university.service;

import edu.university.entity.User;

/**
 * Interface Notifier, used to notify users with a given messages
 */
public interface NotifyService {

    /**
     * Method notify is used to send a message to the user
     * @param user specific user who will receive a message
     * @param message formatted message text to the user
     * @return returns <code>True</code> if message was successfully sent
     */
    public boolean notify(User user, String message);
}
