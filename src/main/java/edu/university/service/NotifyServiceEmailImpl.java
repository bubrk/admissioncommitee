package edu.university.service;

import edu.university.config.InjectLogger;
import edu.university.entity.User;
import org.slf4j.Logger;

import java.util.Objects;

public class NotifyServiceEmailImpl implements NotifyService {
    @InjectLogger
    private Logger log;

    @Override
    public boolean notify(User user, String message) {
        if (Objects.isNull(user) || Objects.isNull(message) || message.length()==0){
            return false;
        }
        //todo email notifier
        log.debug("Notifying user [{}] with message [{}]",user,message);
        return true;
    }
}
