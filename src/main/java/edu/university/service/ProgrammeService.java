package edu.university.service;

import edu.university.entity.*;
import edu.university.entity.dto.Paginator;
import edu.university.entity.dto.Tuple2;
import edu.university.exception.ServiceException;

import edu.university.exception.ValidationException;
import java.util.List;
import java.util.Map;

public interface ProgrammeService {

    /**
     * Returns all programmes from department with given id
     *
     * @param departmentId id of department
     * @return List of {@link Programme} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<Programme> getAllProgrammesInDepartment(int departmentId) throws ServiceException;

    /**
     * Returns educational programme by giving id
     *
     * @param programmeId id of the programme
     * @return {@link Programme} or throws exception if nothing found
     * @throws ServiceException if an error occurred while processing data or programme not found
     */
    public Programme getProgrammeById(int programmeId) throws ServiceException;

    /**
     * Returns detailed information about programme
     *
     * @param programmeId id of the programme
     * @return {@link ProgrammeDetails} or throws exception if nothing found
     * @throws ServiceException if an error occurred while processing data or programme not found
     */
    public ProgrammeDetails getProgrammeDetails(int programmeId) throws ServiceException;

    /**
     * Returns requirements for programme with giving id
     *
     * @param programmeId id of the programme
     * @return List of {@link SubjectRequirements} or throws exception if nothing found
     * @throws ServiceException if an error occurred while processing data or requirements not found
     */
    public List<SubjectRequirements> getProgrammeRequirements(int programmeId) throws ServiceException;

    /**
     * Returns all educational programmes
     *
     * @return List of {@link Programme} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<Programme> getAllProgrammes() throws ServiceException;

    /**
     * Returns all educational programmes with detailed data
     * @return Map {@link Programme} with {@link ProgrammeDetails} or empty map if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public Map<Programme, ProgrammeDetails> searchProgrammes() throws ServiceException;

    /**
     * Searches educational programmes with filters
     * @param props properties for filtering and sorting
     * @param paginator instance of {@link Paginator} for pagination
     * @return List of {@link Programme} with {@link ProgrammeDetails} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<Tuple2<Programme, ProgrammeDetails>> searchProgrammesWithFilter(Map<String, String> props, Paginator paginator) throws ServiceException;

    /**
     * Delete programme with all linked data
     * @param programmeId id of the programme
     * @return true if deleted successfully
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean deleteProgramme(int programmeId) throws ServiceException;

    /**
     * Returns department by given id
     * @param departmentId id of the department
     * @return {@link Department} or throws an exception if nothing found
     * @throws ServiceException if an error occurred while processing data or department not found
     */
    public Department getDepartment(int departmentId) throws ServiceException;

    /**
     * Returns all departments
     * @return List of {@link Department}
     * @throws ServiceException if an error occurred while processing data
     */
    public List<Department> getAllDepartments() throws ServiceException;

    /**
     * Returns all departments nested inside department with given id
     * @param parentDepartmentId id of department container
     * @return List of {@link Department} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<Department> getAllDepartments(int parentDepartmentId) throws ServiceException;

    /**
     * Saves department to the system
     * @param department given department
     * @return true if saved successfully
     * @throws ValidationException if invalid data was passed
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean saveDepartment(Department department) throws ValidationException, ServiceException;

    /**
     * Removes empty department from the system
     * @param departmentId id of the department
     * @return true if deleted successfully
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean deleteDepartment(int departmentId) throws ServiceException;

    /**
     * Saves or updates programme into the system
     * @param programme given programme
     * @param programmeDetails detailed information about given programme
     * @param programmeRequirements requirements for programme
     * @return {@link Programme} with generated id
     * @throws ValidationException if invalid data was passed with parameters
     * @throws ServiceException if an error occurred while processing data
     */
    public Programme saveProgramme(Programme programme, ProgrammeDetails programmeDetails, List<SubjectRequirements> programmeRequirements) throws ValidationException, ServiceException;

}
