package edu.university.service;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.DepartmentDAO;
import edu.university.dao.ProgrammeDAO;
import edu.university.entity.*;
import edu.university.entity.dto.Paginator;
import edu.university.entity.dto.Tuple2;
import edu.university.exception.DAOException;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import edu.university.exception.ValidationException;
import java.util.*;
import java.util.stream.Collectors;

public class ProgrammeServiceImpl implements ProgrammeService {
    @InjectLogger
    private Logger log;
    @InjectByType
    private DepartmentDAO departmentDAO;
    @InjectByType
    private ProgrammeDAO programmeDAO;

    private static final String ERROR_MESSAGE = "An error occurred while working with database";

    @Override
    public List<Programme> getAllProgrammesInDepartment(int departmentId) throws ServiceException {
        List<Programme> programmes;
        try {
            programmes = programmeDAO.getProgrammesInDepartment(departmentId);
        } catch (DAOException e) {
            log.error("Failed to find departments", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

        if (programmes == null) {
            return new ArrayList<>();
        }
        return programmes;
    }

    @Override
    public Programme getProgrammeById(int programmeId) throws ServiceException {
        Programme programme;
        try {
            return programmeDAO.getProgrammeById(programmeId)
                    .orElseThrow(() -> new ServiceException("Can't find programme with given id"));
        } catch (DAOException e) {
            log.error("Failed to find programme", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public ProgrammeDetails getProgrammeDetails(int programmeId) throws ServiceException {
        try {
            return programmeDAO.getProgrammeDetails(programmeId)
                    .orElseThrow(() -> new ServiceException("Can't find programme details for given programme"));

        } catch (DAOException e) {
            log.error("Failed to find programme details", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<SubjectRequirements> getProgrammeRequirements(int programmeId) throws ServiceException {
        try {
            List<SubjectRequirements> programmeRequirements = programmeDAO.getProgrammeRequirements(programmeId);

            if (programmeRequirements == null || programmeRequirements.isEmpty()) {
                throw new ServiceException("Can't find programme requirements for given programme");
            } else {
                return programmeRequirements;
            }

        } catch (DAOException e) {
            log.error("Failed to find programme requirements", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }


    @Override
    public List<Department> getAllDepartments() throws ServiceException {
        List<Department> departments;
        try {
            departments = departmentDAO.getAllDepartments();
        } catch (DAOException e) {
            log.error("Failed to find departments", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

        if (departments == null) {
            return new ArrayList<>();
        }
        return departments;
    }

    @Override
    public List<Department> getAllDepartments(int parentDepartmentId) throws ServiceException {
        List<Department> departments;
        try {
            departments = departmentDAO.getAllSubDepartments(parentDepartmentId);
        } catch (DAOException e) {
            log.error("Failed to find sub-departments", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

        if (departments == null) {
            return new ArrayList<>();
        }
        return departments;
    }

    @Override
    public Department getDepartment(int departmentId) throws ServiceException {

        try {
            return departmentDAO.getDepartment(departmentId).orElseThrow(() ->
                    new ServiceException("No department found with this id"));

        } catch (DAOException e) {
            log.error("Failed to get the department by id", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<Programme> getAllProgrammes() throws ServiceException {
        List<Programme> programmes;
        try {
            programmes = programmeDAO.getAllProgrammes();
        } catch (DAOException e) {
            log.error("Failed to find programmes", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

        if (programmes == null) {
            return new ArrayList<>();
        }
        return programmes;
    }

    @Override
    public Map<Programme, ProgrammeDetails> searchProgrammes() throws ServiceException {
        try {
            return programmeDAO.getAllProgrammesWithDetails();

        } catch (DAOException e) {
            log.error("Failed to get the programmes", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<Tuple2<Programme, ProgrammeDetails>> searchProgrammesWithFilter(Map<String, String> props, Paginator paginator) throws ServiceException {
        try {
            final String name = props.get("programmeName");

            Map<Programme, ProgrammeDetails> map = programmeDAO.getAllProgrammesWithDetailsFiltering(name);

            if (map == null || map.isEmpty()) {
                return new ArrayList<>();
            }

            final String dateQuery = props.get("dateQuery") != null ? props.get("dateQuery") : "all";
            final String sortBy = props.get("sortingType") != null ? props.get("sortingType") : "nameAz";
//
            List<Tuple2<Programme, ProgrammeDetails>> programmes =
                    map.entrySet()
                            .stream()
                            .filter(entry -> filterByDates(entry.getValue(), dateQuery))
                            .sorted(getComparatorForFiltering(sortBy))
                            .map(entry -> new Tuple2<Programme, ProgrammeDetails>(entry.getKey(), entry.getValue()))
                            .collect(Collectors.toList());

            paginator.setTotalEntries(programmes.size());

            return programmes.subList((paginator.getCurrentPage() - 1) * paginator.getPageSize(),
                    Math.min(paginator.getCurrentPage() * paginator.getPageSize(), programmes.size()));

        } catch (DAOException e) {
            log.error("Failed to get the programmes", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean deleteProgramme(int programmeId) throws ServiceException {
        try {
            return programmeDAO.deleteProgramme(programmeId);
        } catch (DAOException e) {
            log.error("Failed to delete programme", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean deleteDepartment(int departmentId) throws ServiceException {
        try {
            return departmentDAO.deleteDepartment(departmentId);
        } catch (DAOException e) {
            log.error("Failed to delete department", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean saveDepartment(Department department) throws ValidationException, ServiceException {
        if (department == null) {
            return false;
        }
        //TODO validation
        try {
            if (department.getId() == 0) {
                return departmentDAO.createDepartment(department).isPresent();
            } else {
                return departmentDAO.updateDepartment(department);
            }

        } catch (DAOException e) {
            log.error("Failed to get the programmes", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Programme saveProgramme(Programme programme, ProgrammeDetails programmeDetails, List<SubjectRequirements> programmeRequirements) throws ValidationException, ServiceException {
        validateProgramme(programme);
        validateProgrammeDetails(programmeDetails);
        validateProgrammeRequirements(programmeRequirements);

        try {
            if (programme.getId() == 0) {
                return programmeDAO.createProgramme(programme, programmeDetails, programmeRequirements)
                        .orElseThrow(() -> new ServiceException("Unexpected error, unable to save programme"));
            }

            if (programmeDAO.updateProgramme(programme, programmeDetails, programmeRequirements)) {
                return programme;
            } else {
                throw new ServiceException("Unexpected error, unable to update programme");
            }
        } catch (DAOException e) {
            log.error("Failed to save programme", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    private void validateProgramme(Programme programme) throws ValidationException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Programme>> violations = validator.validate(programme);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.iterator()
                    .next().getMessage());
        }
    }

    private void validateProgrammeDetails(ProgrammeDetails programmeDetails) throws ValidationException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ProgrammeDetails>> violations = validator.validate(programmeDetails);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.iterator()
                    .next().getMessage());
        }

        validatePlaces(programmeDetails);
        validateDates(programmeDetails);
    }

    private void validatePlaces(ProgrammeDetails programmeDetails) throws ValidationException {
        if (programmeDetails.getTotalPlaces() <
               programmeDetails.getBudgetPlaces()) {
            throw new ValidationException("Total amount of places can't be less than budget amount of places");
        }
    }

    private void validateDates(ProgrammeDetails programmeDetails) throws ValidationException {
        if (programmeDetails.getAdmissionsStartDate()
                .compareTo(
                        programmeDetails.getAdmissionsEndDate()) > 0) {
            throw new ValidationException("End date can't be before start date!");
        }

        if (programmeDetails.getAdmissionsStartDate()
                .compareTo(
                        programmeDetails.getAdmissionsEndDate()) == 0) {
            throw new ValidationException("End date and start date can't be same date!");
        }
    }

    private void validateProgrammeRequirements(List<SubjectRequirements> requirements) throws ValidationException {
        for (SubjectRequirements requirement : requirements) {
            validateSubjectRequirements(requirement);
        }
    }

    private void validateSubjectRequirements(SubjectRequirements requirements) throws ValidationException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SubjectRequirements>> violations = validator.validate(requirements);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.iterator()
                    .next().getMessage());
        }
    }

    private boolean filterByDates(ProgrammeDetails programmeDetails, String dates) {
        Date now = new Date();
        switch (dates) {
            case "future":
                return now.compareTo(programmeDetails.getAdmissionsStartDate()) < 0;
            case "past":
                return now.compareTo(programmeDetails.getAdmissionsEndDate()) > 0;
            case "opened":
                return now.compareTo(programmeDetails.getAdmissionsStartDate()) >= 0 &&
                        now.compareTo(programmeDetails.getAdmissionsEndDate()) <= 0;
            default:
                return true;
        }
    }

    private Comparator<Map.Entry<Programme, ProgrammeDetails>> getComparatorForFiltering(String sortBy) {
        switch (sortBy) {

            case "byBudget19":
                return Comparator.comparingInt(o -> o.getValue().getBudgetPlaces());

            case "byBudget91":
                return (o1, o2) -> Integer.compare(o2.getValue().getBudgetPlaces(), o1.getValue().getBudgetPlaces());

            case "byTotal19":
                return Comparator.comparingInt(o -> o.getValue().getTotalPlaces());

            case "byTotal91":
                return (o1, o2) -> Integer.compare(o2.getValue().getTotalPlaces(), o1.getValue().getTotalPlaces());

            case "byNameZa":
                return (o1, o2) -> o2.getKey().getName().compareTo(o1.getKey().getName());

            //default "byNameAz"
            default:
                return Comparator.comparing(o -> o.getKey().getName());

        }


    }


}
