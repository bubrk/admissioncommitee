package edu.university.service;

/**
 * Class contains {@link RandomCodeGeneratorService#generateCode()} method which is used for generating
 * random codes for verification processes
 */
public class RandomCodeGeneratorService {

    private static final int RANGE = 9999;

    /**
     * Generates random code between 0 and {@link RandomCodeGeneratorService#RANGE}
     * @return <code>String</code> with random code
     */
    public static String generateCode(){
        int code = (int) (Math.random()*RANGE);
        return String.format("%04d", code);
    }
}
