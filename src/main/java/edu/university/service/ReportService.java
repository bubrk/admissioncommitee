package edu.university.service;

import edu.university.entity.Document;
import edu.university.entity.Programme;
import edu.university.entity.Report;
import edu.university.exception.ServiceException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ReportService {
    /**
     * Returns all reports
     * @return Map of {@link Programme} with {@link Document}
     * @throws ServiceException if an error occurred while processing data
     */
    Map<Programme,Document> getAllReports() throws ServiceException;

    /**
     * Returns reports by programme id
     * @param programmeId id of the programme
     * @return List of {@link Document} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    List<Document> getReportsByProgrammeId(int programmeId) throws ServiceException;

    /**
     * Returns report  by giving id
     * @param reportId id of the report
     * @return <code>Optional.of(Report)</code>
     * @throws ServiceException if an error occurred while processing data
     */
    Optional<Report> getReportByReportId(int reportId) throws ServiceException;

    /**
     * Returns report as xml string by report id
     * @param reportId id of the report
     * @return String with report in xml format
     * @throws ServiceException if an error occurred while processing data
     */
    String getReportAsXMLStringByReportId(int reportId) throws ServiceException;

    /**
     * Saves report to the system
     * @param report given report
     * @param reportName name of the report
     * @return <code>Optional.of(Document)</code> if saved successfully
     * @throws ServiceException if an error occurred while processing data
     */
    Optional<Document> saveReport(Report report, String reportName) throws ServiceException;

    /**
     * Generates report for educational programme
     * @param programmeId id of the programme
     * @return generated {@link Report}
     * @throws ServiceException if an error occurred while processing data
     */
    Report generateReport(int programmeId) throws ServiceException;
}
