package edu.university.service;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.ApplicationDAO;
import edu.university.dao.ProgrammeDAO;
import edu.university.dao.ReportDAO;
import edu.university.dao.UserDAO;
import edu.university.entity.*;
import edu.university.exception.DAOException;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class ReportServiceImpl implements ReportService {
    @InjectLogger
    private Logger log;

    private static final String ERROR_MESSAGE = "Error occurred while working with database";

    @InjectByType
    private static ProgrammeDAO programmeDAO;
    @InjectByType
    private static ApplicationDAO applicationDAO;
    @InjectByType
    private static UserDAO userDAO;
    @InjectByType
    private static ReportDAO reportDAO;
    @InjectByType
    private static ReportsStorageService reportsStorage;

    @Override
    public Map<Programme, Document> getAllReports() throws ServiceException {
        try {
            return reportDAO.getAllReports();
        } catch (DAOException e) {
            log.error("Failed to find all reports", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<Document> getReportsByProgrammeId(int programmeId) throws ServiceException {
        try {
            return reportDAO.getReportsByProgrammeId(programmeId);
        } catch (DAOException e) {
            log.error("Failed to find reports for programme id={}", programmeId, e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Optional<Report> getReportByReportId(int reportId) throws ServiceException {
        try {
            Document report = reportDAO.getReportById(reportId)
                    .orElseThrow(() -> new SecurityException("Failed to get report id=" + reportId));
            return reportsStorage.getReportFromStorage(report.getFile());
        } catch (DAOException e) {
            log.error("Failed to get report id={}", reportId, e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public String getReportAsXMLStringByReportId(int reportId) throws ServiceException {
        try {
            Document report = reportDAO.getReportById(reportId)
                    .orElseThrow(() -> new SecurityException("Failed to get report id=" + reportId));
            return reportsStorage.getReportAsXMLStringFromStorage(report.getFile());
        } catch (DAOException e) {
            log.error("Failed to get report id={}", reportId, e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Optional<Document> saveReport(Report report, String reportName) throws ServiceException {
        try {
            String fileName = reportsStorage.saveReportToStorage(report)
                    .orElseThrow(() -> new ServiceException("Failed to save report into storage"));
            return reportDAO.saveReport(report.getProgramme().getId(), reportName, fileName);
        } catch (DAOException e) {
            log.error("Failed to save report for programme", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Report generateReport(int programmeId) throws ServiceException {
        try {
            //todo check if exists already
            Programme programme = programmeDAO.getProgrammeById(programmeId)
                    .orElseThrow(() -> new ServiceException("Failed to find programme id=" + programmeId));

            ProgrammeDetails details = programmeDAO.getProgrammeDetails(programmeId)
                    .orElseThrow(() -> new ServiceException("Failed to find details for programme id=" + programmeId));

            Date now = new Date();
            if (now.before(details.getAdmissionsEndDate())) {
                throw new ServiceException("Can't generate report before admissions end date");
            }

            List<SubjectRequirements> requirements = programmeDAO.getProgrammeRequirements(programmeId);
            if (requirements.isEmpty()) {
                throw new ServiceException("Failed to find requirements for programme id=" + programmeId);
            }

            List<Application> applications =
                    applicationDAO.getAcceptedApplicationsForProgramme(programmeId);

            if (applications.isEmpty()) {
                throw new ServiceException("There is no applications for this programme");
            }

            Map<Application, Double> totalScores = applications.stream()
                    .collect(Collectors.toMap(application -> application,
                            application -> getTotalScore(application.getScores(), requirements)));

            List<Application> sortedApplications = totalScores.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            List<Application> accepted = sortedApplications.subList(0, Math.min(details.getTotalPlaces(), sortedApplications.size()));
            List<Application> budget = sortedApplications.subList(0,Math.min(details.getBudgetPlaces(), sortedApplications.size()));

            return new Report.Builder()
                    .programme(programme)
                    .programmeDetails(details)
                    .reportRows(getReportRows(totalScores,accepted,budget))
                    .build();

        } catch (DAOException e) {
            log.error("Failed to get data from database", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    private List<Map<String, String>> getReportRows(Map<Application, Double> totalScores, List<Application> accepted, List<Application> budget) throws ServiceException {
        try {
            return totalScores.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .map(e -> getReportRow(e.getKey(), e.getValue(),accepted,budget))
                    .collect(Collectors.toList());

        } catch (IllegalArgumentException e) {
            final String msg = "Failed to generate report: can't fetch data properly";
            log.error(msg);
            throw new ServiceException(msg);
        }
    }

    private Map<String, String> getReportRow(Application application, Double totalScore, List<Application> accepted, List<Application> budget) {
        Map<String, String> row = new HashMap<>();
        try {
            User user = userDAO.getUser(application.getUserId())
                    .orElseThrow(() -> new ServiceException("Cant find user for application id = " + application.getId()));
            row.put("abiturient", String.format("%s %s %s", user.getFirstName(), user.getMiddleName(), user.getLastName()));
            row.put("email", user.getEmail());
            StringBuilder scores = new StringBuilder();
            application.getScores().forEach((subject, score) ->
                scores.append(subject.getName())
                        .append(": ")
                        .append(score.getScore())
                        .append("; ")
            );
            row.put("scores", scores.toString());
            row.put("totalScore", totalScore.toString());
            row.put("accepted", accepted.contains(application)?"YES":"NO");
            row.put("budget", budget.contains(application)?"YES":"NO");
            return row;

        } catch (DAOException | ServiceException e) {
            final String msg = "Failed to get data for report";
            log.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

    private Double getTotalScore(Map<Subject, Score> scores, List<SubjectRequirements> requirements) {
        return scores.entrySet().stream()
                .filter(entry -> isScoreMatchesRequirements(entry.getKey(), entry.getValue(), requirements))
                .map(entry -> {
                    double coefficient = getCoefficientForSubject(entry.getKey(), requirements);
                    return entry.getValue().getScore() * coefficient;
                }).mapToDouble(v -> v).sum();
    }

    private double getCoefficientForSubject(Subject subject, List<SubjectRequirements> requirements) {
        Optional<SubjectRequirements> requirement = requirements.stream()
                .filter(r -> r.getSubjects().contains(subject))
                .findAny();
        return requirement.map(SubjectRequirements::getCoefficient)
                .orElse(0.0);
    }

    private boolean isScoreMatchesRequirements(Subject subject, Score score, List<SubjectRequirements> requirements) {
        Optional<SubjectRequirements> opRequirements = requirements.stream()
                .filter(r -> r.getSubjects().contains(subject))
                .findAny();
        return (opRequirements.isPresent() && score.getScore() >= opRequirements.get().getMinScore());
    }

}
