package edu.university.service;

import edu.university.config.InjectLogger;
import edu.university.config.InjectProperty;
import edu.university.entity.Programme;
import edu.university.entity.ProgrammeDetails;
import edu.university.entity.Report;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class ReportStorageServiceImpl implements ReportsStorageService {
    @InjectProperty("reports.location")
    private String reportsLocation;
    @InjectLogger
    private static Logger log;

    public static String prettyPrintByTransformer(String xmlString, int indent, boolean ignoreDeclaration) throws ServiceException {
        try {
            InputSource src = new InputSource(new StringReader(xmlString));
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", indent);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, ignoreDeclaration ? "yes" : "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            Writer out = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(out));
            return out.toString();
        } catch (SAXException | TransformerException | ParserConfigurationException | IOException e) {
            log.error("Error occurs when pretty-printing xml:\n" + xmlString, e);
            throw new ServiceException("Error occurs when pretty-printing xml");
        }
    }

    @Override
    public Optional<String> saveReportToStorage(Report report) throws ServiceException {
        String nameOnServer = UUID.randomUUID().toString();
        String filePath = reportsLocation + File.separator + nameOnServer;

        if (saveReportToXMLFile(report, filePath)) {
            return Optional.of(nameOnServer);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Report> getReportFromStorage(String fileName) throws ServiceException {
        String filePath = reportsLocation + File.separator + fileName;
        return Optional.of(readReportFromXMLFile(filePath));
    }

    @Override
    public String getReportAsXMLStringFromStorage(String fileName) throws ServiceException {
        String filePath = reportsLocation + File.separator + fileName;
        return readXMLFromFile(filePath);
    }

    private boolean saveReportToXMLFile(Report report, String filePath) throws ServiceException {
        if (report == null || filePath == null || filePath.isEmpty()) {
            return false;
        }
        log.debug("Saving report to file [{}]", filePath);

        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8)) {
            String xml = reportToXML(report);
            writer.write(prettyPrintByTransformer(xml, 2, false));
            return true;

        } catch (IOException e) {
            log.error("Failed to save report to file [{}]", filePath, e);
            throw new ServiceException("Failed to save report");
        }
    }

    private String reportToXML(Report report) {
        StringBuilder reportXML = new StringBuilder();

        reportXML.append(programmeToXML(report.getProgramme()))
                .append(programmeDetailsToXML(report.getProgrammeDetails()))
                .append(reportRowsToXML(report.getReportRows()));

        // Todo xml schema

        return getXMLTag("report", reportXML.toString(), null);
    }

    private String reportRowsToXML(List<Map<String, String>> reportRows) {
        StringBuilder rowsXML = new StringBuilder();
        reportRows.forEach(row ->
                rowsXML.append(rowToXML(row)));
        return getXMLTag("reportRows", rowsXML.toString(), null);
    }

    private String rowToXML(Map<String, String> row) {
        StringBuilder rowXML = new StringBuilder();
        row.forEach((param, value) ->
                rowXML.append(getXMLTag(param, value, null)));
        return getXMLTag("row", rowXML.toString(), null);
    }

    private String programmeDetailsToXML(ProgrammeDetails programmeDetails) {
        StringBuilder str = new StringBuilder();
        str.append(getXMLTag("description", programmeDetails.getDescription(), null))
                .append(getXMLTag("admissionsStartDate", programmeDetails.getAdmissionsStartDate().toString(), null))
                .append(getXMLTag("admissionsEndDate", programmeDetails.getAdmissionsEndDate().toString(), null))
                .append(getXMLTag("budgetPlaces", String.valueOf(programmeDetails.getBudgetPlaces()), null))
                .append(getXMLTag("totalPlaces", String.valueOf(programmeDetails.getTotalPlaces()), null));

        return getXMLTag("programmeDetails", str.toString(), null);
    }

    private String programmeToXML(Programme programme) {
        StringBuilder str = new StringBuilder();
        str.append(getXMLTag("id", String.valueOf(programme.getId()), null))
                .append(getXMLTag("departmentId", String.valueOf(programme.getDepartmentId()), null))
                .append(getXMLTag("name", programme.getName(), null))
                .append(getXMLTag("shortDescription", programme.getShortDescription(), null));

        return getXMLTag("programme", str.toString(), null);
    }

    private String getXMLTag(String tagName, String value, Map<String, String> params) {
        StringBuilder xmlStr = new StringBuilder();
        xmlStr.append("<")
                .append(tagName);

        if (params != null && !params.isEmpty()) {
            params.forEach((param, val) ->
                    xmlStr.append(" ").append(param)
                            .append("=\"").append(val).append("\""));
        }

        xmlStr.append(">")
                .append(value)
                .append("</")
                .append(tagName)
                .append(">");

        return xmlStr.toString();
    }

    private Report readReportFromXMLFile(String filePath) throws ServiceException {
        log.debug("Reading report from file [{}]", filePath);

        try (Reader reader = new FileReader(filePath)) {
            String xml = readXMLFromFile(filePath);

            Report report = parseReportFromXML(xml);
            return report;
        } catch (IOException e) {
            log.error("Failed reading report from file [{}]", filePath, e);
            throw new ServiceException("Failed to read report from file");
        }
    }

    private Report parseReportFromXML(String xml) {
        //todo parse report?
        return new Report.Builder()
                .build();
    }


    private String readXMLFromFile(String filePath) throws ServiceException {
        try (FileInputStream is = new FileInputStream(filePath);
             InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(isr)) {

            // create StringBuilder object
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            // Append items from the file to string builder
            String ls = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            // delete the last new line separator
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            return stringBuilder.toString();
        } catch (IOException e) {
            log.error("Failed to read report from file [{}]", filePath, e);
            throw new ServiceException("Failed to read report");
        }
    }
}
