package edu.university.service;

import edu.university.entity.Report;
import edu.university.exception.ServiceException;

import java.util.Optional;

public interface ReportsStorageService {
    /**
     * Saves report to the file system
     * @param report instance of {@link Report}
     * @return name of the file in storage if saved successfully
     * @throws ServiceException if an error occurred while processing data
     */
    public Optional<String> saveReportToStorage(Report report) throws ServiceException;

    /**
     * Returns report from the file system
     * @param fileName name of the file
     * @return <code>Optional.of(Report)</code> if read successfully
     * @throws ServiceException if an error occurred while processing data
     */
    public Optional<Report> getReportFromStorage(String fileName) throws ServiceException;

    /**
     * Returns report as xml string from file system
     * @param fileName name of the file
     * @return String with report in xml format
     * @throws ServiceException if an error occurred while processing data
     */
    String getReportAsXMLStringFromStorage(String fileName) throws ServiceException;
}
