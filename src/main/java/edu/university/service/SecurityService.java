package edu.university.service;

import edu.university.config.InjectLogger;
import edu.university.config.InjectProperty;
import org.slf4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import static edu.university.config.Config.SECURITY_SALT;

/**
 * Security utils
 */
public class SecurityService {
    @InjectLogger
    private Logger log;
    @InjectProperty(SECURITY_SALT)
    private String SALT;

    /**
     * Method takes a password and returns a fixed-size string, the hash value of the given password
     * @param password a <code>String</code> with the password
     * @return <code>String</code> hash value of the password
     */
    public String getHashedPassword(String password){
        return md5WithSalt(password);
    }

    //TODO Salt must be generated and saved for each user?
    private String md5WithSalt(String input){
        return md5(input+SALT);
    }

    //generates md5 hash
    private String md5(String input) {
        String hash = null;

        if(Objects.isNull(input)) {
            return null;
        }

        try {
            //Create MessageDigest object for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            md.update(input.getBytes(), 0, input.length());

            //Converts message digest value in base 16 (hex)
            hash = new BigInteger(1, md.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {
            String errMessage = "Failed to get MessageDigest object for MD5";
            log.error(errMessage,e);
            throw new IllegalStateException(errMessage);
        }

        return hash;
    }
}
