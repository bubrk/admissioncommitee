package edu.university.service;

import edu.university.entity.Subject;
import edu.university.exception.ServiceException;

import java.util.List;

public interface SubjectService {
    /**
     * Returns all subjects
     * @return List of {@link Subject} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    public List<Subject> getAllSubjects() throws ServiceException;

    /**
     * Returns subject by giving id
     * @param subjectId id of the subject
     * @return {@link Subject} or throws an exception if nothing found
     * @throws ServiceException if an error occurred while processing data or nothing found
     */
    public Subject getSubjectById(int subjectId) throws ServiceException;

    /**
     * Saves subject to the system
     * @param subjectName name of the subject
     * @return {@link Subject} with generated id
     * @throws ServiceException if an error occurred while processing data
     */
    Subject saveSubject(String subjectName) throws  ServiceException;
}
