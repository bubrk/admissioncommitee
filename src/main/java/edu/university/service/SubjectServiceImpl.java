package edu.university.service;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.DAOFactory;
import edu.university.dao.SubjectDAO;
import edu.university.entity.Subject;
import edu.university.exception.DAOException;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SubjectServiceImpl implements SubjectService {
    @InjectLogger
    private static Logger log;
    @InjectByType
    private SubjectDAO subjectDAO;

    private static final String ERROR_MESSAGE = "An error occurred while working with database";

    @Override
    public List<Subject> getAllSubjects() throws ServiceException {
        try {
            return subjectDAO.getAllSubjects();
        } catch (DAOException e) {
            log.error("Failed to find subjects", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Subject getSubjectById(int subjectId) throws ServiceException {
        try {
            return subjectDAO.getSubjectById(subjectId)
                    .orElseThrow(() -> new ServiceException("Failed to find subject with given id"));
        } catch (DAOException e) {
            log.error("Failed to find subject by id", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Subject saveSubject(String subjectName) throws ServiceException {
        try {
            log.debug("Saving subject, name = {}",subjectName);
            return subjectDAO.createSubject(subjectName)
                    .orElseThrow(() -> new ServiceException("Failed to save subject"));
        } catch (DAOException e) {
            log.error("Failed to save subject [{}]", subjectName, e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }
}
