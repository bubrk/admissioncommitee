package edu.university.service;

import edu.university.entity.Document;
import edu.university.entity.User;
import edu.university.entity.UserDetails;
import edu.university.exception.ServiceException;

import edu.university.exception.ValidationException;
import java.util.List;
import java.util.Optional;

public interface UserBlacklistService {

    /**
     * Checks if user with giving id is in the blacklist
     * @param userId id of the user
     * @return true if user is blacklisted
     */
    boolean isUserInBlackList(int userId);

    /**
     * Returns comment for blacklisted user
     * @param userId id of the user
     * @return String with comment
     */
    String getBlacklistComment(int userId);

    /**
     * Adds user to blacklist
     * @param userId id of the user
     * @param comment String with comment
     * @return true is saved successfully
     * @throws ServiceException if an error occurred while processing data
     * @throws ValidationException if invalid data was passed to the method
     */
    boolean addUserToBlackList(int userId, String comment) throws ServiceException, ValidationException;

    /**
     * Removes user with giving id from blacklist
     * @param userId id of the user
     * @return true if removed successfully
     * @throws ServiceException if an error occurred while processing data
     */
    boolean removeUserFromBlackList(int userId) throws ServiceException;
}
