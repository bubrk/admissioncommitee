package edu.university.service;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.DAOFactory;
import edu.university.dao.UserDAO;
import edu.university.entity.User;
import edu.university.exception.DAOException;
import edu.university.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import edu.university.exception.ValidationException;
import java.util.List;

public class UserBlacklistServiceImpl implements UserBlacklistService {
    @InjectLogger
    private static Logger log;
    @InjectByType
    private UserDAO userDAO;

    private static final String ERROR_MESSAGE = "Error occurred while working with database";
    private static List<User> blackList;

    @Override
    public boolean isUserInBlackList(int userId) {
        return blackList.stream()
                .map(User::getId)
                .anyMatch(id -> id == userId);
    }

    @Override
    public String getBlacklistComment(int userId) {
        try {
            return userDAO.getBlacklistComment(userId).orElse("");
        } catch (DAOException e) {
            final String msg = "Failed to get blacklist comment, userId = " + userId;
            log.error(msg, e);
            throw new IllegalStateException(msg);
        } finally {
            updateBlackList();
        }
    }

    @Override
    public boolean addUserToBlackList(int userId, String comment) throws ServiceException, ValidationException {
        if (comment == null || comment.isEmpty()) {
            throw new ValidationException("Comment can't be empty");
        }
        if (comment.length() < 10 || comment.length() > 250) {
            throw new ValidationException("Comment must be between 10 and 250 characters long");
        }

        try {
            return userDAO.blockUser(userId, comment);
        } catch (DAOException e) {
            log.error("Failed to block user", e);
            throw new ServiceException(ERROR_MESSAGE);
        } finally {
            updateBlackList();
        }
    }

    @Override
    public boolean removeUserFromBlackList(int userId) throws ServiceException {
        try {
            return userDAO.unblockUser(userId);
        } catch (DAOException e) {
            log.error("Failed to unblock user", e);
            throw new ServiceException(ERROR_MESSAGE);
        } finally {
            updateBlackList();
        }
    }

    @PostConstruct
    public synchronized void updateBlackList() {
        try {
            blackList = userDAO.getBlackListedUsers();
        } catch (DAOException e) {
            final String msg = "Failed to get blacklisted users";
            log.error(msg, e);
            throw new IllegalStateException(msg);
        }
    }
}
