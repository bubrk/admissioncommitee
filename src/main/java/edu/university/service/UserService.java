package edu.university.service;

import edu.university.entity.Document;
import edu.university.entity.User;
import edu.university.entity.UserDetails;

import edu.university.exception.ServiceException;

import edu.university.exception.ValidationException;
import java.util.List;
import java.util.Optional;

/**
 * Facade that provides business logic and simplified interface to the UserDao
 */
public interface UserService {

    /**
     * Returns <code>true</code> if user already registered in the system
     *
     * @param email email address of the user
     * @return <code>true</code> user with such email exists or <code>false</code> if not found
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean userExists(String email) throws ServiceException;

    /**
     * Returns user id by email address
     *
     * @param email string with email address
     * @return id of the user
     * @throws ServiceException if an error occurred while processing data
     */
    public int getUserId(String email) throws ServiceException;


    /**
     * Performs a search for a user by address and if the passwords match, then returns the user<br>
     * Keep in mind that the <code>password</code> field must be cleared in the returned <code>User</code> instance.
     *
     * @param email    email address
     * @param password hash of the password
     * @return User if user exists and password hashes match
     * @throws ServiceException if an error occurred while processing data
     */
    public User getUser(String email, String password) throws ServiceException;

    /**
     * Return user by its id<br>
     *
     * @param userId unique identification of the user
     * @return User if user with given id exists
     * @throws ServiceException if user with given id doesn't exist or error occurred
     */
    public User getUserById(int userId) throws ServiceException;

    /**
     * Method saves If record in database with given email exists, then {@link ServiceException} will be thrown
     *
     * @return <code>Optional<User></code> if saved successfully
     * @throws ServiceException if email or password is not valid, or record with such an email already exists
     */
    public User saveUser(User user) throws ServiceException;

    /**
     * Returns detailed information about the user
     *
     * @param userId id of the user
     * @return <code>Optional.of(UserDetails)</code>
     * @throws ServiceException if an error occurred while processing data
     */
    public Optional<UserDetails> getUserDetails(int userId) throws ServiceException;

    /**
     * Searches all users
     *
     * @return List of {@link User} or empty list if nothing found
     * @throws ValidationException if invalid data was passed
     * @throws ServiceException    if an error occurred while processing data
     */
    List<User> searchUsers() throws ValidationException, ServiceException;

    /**
     * Saves detailed information about the user to the system
     *
     * @param userDetails instance of {@link UserDetails}
     * @return true if saved successfully
     * * @throws ValidationException if invalid data was passed
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean saveUserDetails(UserDetails userDetails) throws ValidationException, ServiceException;

    /**
     * Returns list of documents uploaded by user
     * @param userId id of the user
     * @return list of {@link Document} or empty list if nothing found
     * @throws ServiceException if an error occurred while processing data
     */
    List<Document> findUserDocuments(int userId) throws ServiceException;

    /**
     * Saves user document to the system
     * @param userId id of the user
     * @param documentName name of the document
     * @param fileName name of the file
     * @return {@link Document} with generated id if saved successfully
     * @throws ServiceException    if an error occurred while processing data
     * @throws ValidationException if invalid data was passed
     */
    public Document saveUserDocument(int userId, String documentName, String fileName) throws ServiceException, ValidationException;

    /**
     * Returns file name of the document by id
     * @param documentId id of the document
     * @return String with file name
     * @throws ServiceException if an error occurred while processing data
     */
    public String getUserDocumentFile(int documentId) throws ServiceException;

    /**
     * Returns document by id
     * @param documentId id of the document
     * @return {@link Document}
     * @throws ServiceException if an error occurred while processing data
     */
    public Document getUserDocument(int documentId) throws ServiceException;

    /**
     * Checks if user with giving id is owner of the document
     * @param userId id of the user
     * @param documentId id of the document
     * @return true if user with giving id is owner of the document with document id
     * @throws ServiceException if an error occurred while processing data
     */
    public boolean isUserOwnerOfTheDocument(int userId, int documentId) throws ServiceException;

    /**
     * Checks if user has all information needed for applying for programme
     * @param userId id of the user
     * @throws ValidationException if some information missing or invalid
     * @throws ServiceException    if an error occurred while processing data
     */
    public void checkIfUserFilledAllInfo(int userId) throws ValidationException, ServiceException;

    /**
     * Deletes user document
     * @param user user, owner of the document or admin
     * @param documentId id of the document
     * @return true id deleted successfully
     * @throws ServiceException    if an error occurred while processing data
     * @throws ValidationException if user is not owner of the document
     */
    public boolean deleteUserDocument(User user, int documentId) throws ServiceException, ValidationException;

}
