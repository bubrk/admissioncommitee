package edu.university.service;

import edu.university.config.InjectByType;
import edu.university.config.InjectLogger;
import edu.university.dao.UserDAO;
import edu.university.entity.Document;
import edu.university.entity.Roles;
import edu.university.entity.User;
import edu.university.entity.UserDetails;
import edu.university.exception.DAOException;
import edu.university.exception.ServiceException;

import org.slf4j.Logger;

import edu.university.exception.ValidationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    @InjectLogger
    private static Logger log;
    @InjectByType
    private UserDAO userDAO;

    private static final String ERROR_MESSAGE = "Error occurred while working with database, please try again later";


    @Override
    public boolean userExists(String email) throws ServiceException {
        return getUserId(email) > 0;
    }

    @Override
    public int getUserId(String email) throws ServiceException {
        checkEmail(email);

        try {
            Optional<User> ou = userDAO.getUserByEmail(email);

            if (ou.isPresent()) {
                return ou.get().getId();
            }

        } catch (DAOException e) {
            log.error("Failed to use userDAO", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

        return -1;
    }


    @Override
    public User getUser(String email, String password) throws ServiceException {
        checkEmail(email);
        checkPassword(password);

        Optional<User> optionalUser;

        try {
            optionalUser = userDAO.getUserByEmail(email);
        } catch (DAOException e) {
            log.error("Failed to get the user", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

        User user = optionalUser.orElseThrow(() ->
                new ServiceException("No record found with this email"));

        if (!password.equals(user.getPassword())) {
            throw new ServiceException("Password is incorrect");
        }

        user.emptyPassword(); //empty password field before return User

        return user;
    }

    @Override
    public User getUserById(int userId) throws ServiceException {
        try {
            return userDAO.getUser(userId)
                    .orElseThrow(() -> new ServiceException("Can't find record with given id"));
        } catch (DAOException e) {
            log.error("Failed to get the user", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Optional<UserDetails> getUserDetails(int userId) throws ServiceException {

        try {
            return userDAO.getUserDetails(userId);

        } catch (DAOException e) {
            log.error("Failed to get user details", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }


    @Override
    public User saveUser(User user) throws ServiceException {
        checkEmail(user.getEmail());
        checkPassword(user.getPassword());

        if (userExists(user.getEmail())) {
            throw new ServiceException("User with this email already exist");
        }

        try {
            return userDAO.createUser(user).orElseThrow(() ->
                    new ServiceException("Failed to save user to database. Unknown error occurred"));

        } catch (DAOException e) {
            log.error("Failed to save user", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<User> searchUsers() throws ValidationException, ServiceException{
        try{
            //todo filters
            return userDAO.getAllUsersFiltering("","","");
        } catch (DAOException e){
            log.error("Failed to find users",e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean saveUserDetails(UserDetails userDetails) throws ServiceException {
        //TODO validation

        try {
            return userDAO.saveUserDetails(userDetails);
        } catch (DAOException e) {
            log.error("Failed to save userDetails", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public List<Document> findUserDocuments(int userId) throws ServiceException {
        try {
            return userDAO.getUserDocuments(userId);
        } catch (DAOException e) {
            log.error("Failed to find user documents", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Document saveUserDocument(int userId, String documentName, String fileName) throws ServiceException, ValidationException {

        try {
            if (userDAO.getUserDocuments(userId).size() >= 5) {
                throw new ValidationException("You have reached the limit on the number of allowed uploads");
            }

            if (documentName.length() > 50) {
                documentName = shortenFileName(documentName, 50);
            }

            return userDAO.saveUserDocument(userId, documentName, fileName)
                    .orElseThrow(() -> new ServiceException("Failed to save document"));
        } catch (DAOException e) {
            log.error("Failed to save document", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public String getUserDocumentFile(int documentId) throws ServiceException {
        try {
            return userDAO.getUserDocumentFile(documentId)
                    .orElseThrow(() -> new ServiceException("Failed to find document file with given id"));

        } catch (DAOException e) {
            log.error("Failed to get user document", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public Document getUserDocument(int documentId) throws ServiceException {
        try {
            return userDAO.getUserDocument(documentId)
                    .orElseThrow(() -> new ServiceException("Failed to find document with given id"));
        } catch (DAOException e) {
            log.error("Failed to get user document", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean deleteUserDocument(User user, int documentId) throws ServiceException, ValidationException{
        if (user.getRole()!= Roles.ADMIN && !isUserOwnerOfTheDocument(user.getId(),documentId)){
            throw new ValidationException("Access forbidden");
        }

        try {
            String file = getUserDocumentFile(documentId);
            Path path = Paths.get("D:/uploads" +
                    File.separator + file);
//            Path path = Paths.get(ConfigImpl.getProperty(ConfigImpl.UPLOADS_LOCATION) +
//                    File.separator + file);

            if (Files.exists(path)){
                Files.delete(path);
                log.info("Document was deleted: {}",file);
            }

            userDAO.deleteUserDocument(documentId);

            return true;
        } catch (DAOException | IOException e) {
            log.error("Failed to delete user document", e);
            throw new ServiceException(ERROR_MESSAGE);
        }
    }

    @Override
    public boolean isUserOwnerOfTheDocument(int userId, int documentId) throws ServiceException {
        return findUserDocuments(userId)
                .stream()
                .anyMatch(doc -> doc.getId() == documentId);
    }


    @Override
    public void checkIfUserFilledAllInfo(int userId) throws ValidationException, ServiceException {
        try {
            if (!userDAO.getUserDetails(userId).isPresent()) {
                throw new ValidationException("Please fill details about your school");
            }
            if (userDAO.getUserDocuments(userId).isEmpty()) {
                throw new ValidationException("Please add your school certificate");
            }
        } catch (DAOException e) {
            log.error("Failed to check user details", e);
            throw new ServiceException(ERROR_MESSAGE);
        }

    }

    private boolean checkEmail(String email) throws ServiceException {
        if (email == null || email.isEmpty()) {
            throw new ServiceException("Email address can't be empty");
        }

        if (!EmailCheckService.isEmailValid(email)) {
            throw new ServiceException("Email address is incorrect");
        }

        return true;
    }


    private boolean checkPassword(String password) throws ServiceException {
        if (password == null || password.isEmpty()) {
            throw new ServiceException("Password can't be empty");
        }

        return true;
    }

    private String shortenFileName(String documentName, int maxLenght) {
        String fileExtension = documentName.substring(documentName.lastIndexOf("."));
        documentName = documentName.substring(0, maxLenght - fileExtension.length() - 1) +
                fileExtension;
        return documentName;
    }


}
