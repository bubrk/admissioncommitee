package edu.university.view.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * Tag that builds a custom card object
 */
public class Card extends BodyTagSupport {
    private String title = "Title";
    private String href = "";
    private String action;
    private String style = "";

    @Override
    public int doAfterBody() throws JspException {
        String body = getBodyContent().getString();
        JspWriter writer = getBodyContent().getEnclosingWriter();

        try {
            if (href != null && !href.isEmpty()) {
                writer.write(String.format(
                        "<div style='cursor: pointer;' onclick='window.location=\"%s\"'>", href));
            }
            writer.write(String.format(
                    "   <div class='card small hoverable %s'>" +
                            "       <div class='card-content'>" +
                            "           <span class='card-title'>%s</span>", style, title));
            writer.write(body);
            writer.write("       </div>");

            if (action != null && !action.isEmpty()) {
                writer.write(String.format(
                        "       <div class='card-action'>" +
                                "               %s" +
                                "       </div>", action));
            }
            writer.write("   </div>");

            if (href != null && !href.isEmpty()) {
                writer.write("</div>");
            }

        } catch (IOException e) {
            throw new JspException(e);
        }
        return EVAL_PAGE;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}