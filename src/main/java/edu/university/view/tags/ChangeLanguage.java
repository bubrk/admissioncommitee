package edu.university.view.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Drop down list of supported languages
 */
public class ChangeLanguage extends BodyTagSupport {
    private String selfUrl;

    @Override
    public int doAfterBody() throws JspException {
        String body = getBodyContent().getString();
        JspWriter out = getBodyContent().getEnclosingWriter();

        try {
            String id = UUID.randomUUID().toString();
            out.print(String.format("<ul id=\"%s\" class=\"dropdown-content\">", id));
            out.print(System.lineSeparator());
            for (Languages lang : Languages.values()) {
                out.print(String.format("<li><a href=\"%s\">%s</a></li>",
                        getLangUrl(lang.name()),
                        lang.language));
                out.print(System.lineSeparator());
            }
            out.print("</ul>\n");
            out.print(String.format("<a class=\"dropdown-trigger\" id=\"trigger-%s\" href=\"#!\" data-target=\"%s\">%s</a>",
                    id, id, body));
            out.print(String.format("\n<script type=\"text/javascript\">\n" +
                    "document.addEventListener('DOMContentLoaded', function() {\n" +
                    "    M.Dropdown.init(document.getElementById('trigger-%s'), {dropdown: true});\n" +
                    "})\n" +
                    "</script>\n", id));
        } catch (IOException e) {
            throw new JspException(e);
        }

        return EVAL_PAGE;
    }

    public String getSelfUrl() {
        return selfUrl;
    }

    public void setSelfUrl(String selfUrl) {
        this.selfUrl = selfUrl;
    }

    private String getLangUrl(String lang) {
        Pattern p = Pattern.compile("lang=\\w\\w_\\w\\w", Pattern.CASE_INSENSITIVE);
        if (p.matcher(selfUrl).find()) {
            return p.matcher(selfUrl).replaceAll("lang=" + lang);
        }
        return selfUrl + "&lang=" + lang;
    }

    private enum Languages {
        us_EN("English"),
        ua_UK("Українська");

        final String language;

        Languages(String language) {
            this.language = language;
        }
    }
}