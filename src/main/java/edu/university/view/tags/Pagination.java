package edu.university.view.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Used for separating results into discrete pages
 */
public class Pagination extends TagSupport {
    private int currentPage;
    private int lastPage;
    private String url;


    @Override
    public int doStartTag() throws JspException {

        JspWriter out = pageContext.getOut();

        try {
            String urlNoPage = url.replaceAll("(&?)page=\\d+", "");

            out.println("<ul class='pagination'>");
            out.println(String.format("<li class='%s'>", currentPage == 1 ? "disabled" : "waves-effect"));
            out.println(String.format("<a href='%s&page=%d'><i class='material-icons'>chevron_left</i></a></li>",
                    urlNoPage, currentPage == 1 ? 1 : currentPage - 1));

            for (int i = 1; i <= lastPage; i++) {
                out.print(String.format("<li class='%s'><a href='%s&page=%d'>%d</a></li>",
                        currentPage == i ? "active" : "waves-effect",
                        urlNoPage, i, i));
            }

            out.println(String.format("<li class='%s'><a href='%s&page=%d'>" +
                            "<i class='material-icons'>chevron_right</i></a></li>",
                    currentPage == lastPage ? "disabled" : "waves-effect",
                    urlNoPage, currentPage == lastPage ? lastPage : currentPage + 1));
            out.println("</ul>");

        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}