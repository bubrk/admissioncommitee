package edu.university.view.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Used for indexing subject groups
 */
public class SubjectIndex extends TagSupport {
    private static final String[] array = {"First", "Second", "Third", "Forth"};
    private int index;

    @Override
    public int doStartTag() throws JspException {

        JspWriter out = pageContext.getOut();

        try {
            if (index < array.length) {
                out.print(String.format("%s subject", array[index]));
            } else {
                out.print(String.format("Subject №%d", ++index));
            }
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}