<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="${appName}/static/css/materialize.min.css" rel="stylesheet" type="text/css" media="screen,projection"/>
    <%--    <link rel="stylesheet" type="text/css" href="css/materialize.min.css"  media="screen,projection"/>--%>

    <link href="${appName}/static/css/style.css" rel="stylesheet" type="text/css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Title</title>
</head>
<body>

<div class="container center">
    <br>
    <h1 class="header">404</h1>
    <h4>Oops! Looks like you got lost.</h4>
    <h4>Or maybe this page is hiding out in quarantine?</h4>
    <h4>Let's go <a href="./">home</a> and try from there</h4>
</div>

</body>
</html>
