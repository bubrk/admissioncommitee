<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Application info" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>

<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="row">
    <div class="col s3"></div>
    <div class="col s6">
        <div class="col s12 m12">
            <h3 class="header center-align">Application (#${application.id})</h3>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <div class="row">
                            <h5>${programme.name}</h5>
                            <p>${programme.shortDescription}</p>
                            <br>
                        </div>
                        <div class="row">
                            <h5>Personal information</h5>
                            <p><strong>Abiturient: </strong>${user.firstName} ${user.middleName!=null?user.middleName:""} ${user.lastName}</p>
                            <br>
                            <p><strong>Email: </strong>${user.email}</p>
                            <br>
                            <p><strong>Scores: </strong></p>
                            <c:forEach var="entry" items="${scores}">
                                <div class="chip">
                                    ${entry.key.name} - ${entry.value.score}
                                </div>
                            </c:forEach>
                            <br>
                        </div>
                        <div class="row">
                            <h5>Status history</h5>
                            <ul class="collection">
                                <c:forEach var="entry" items="${statusFlow}">
                                    <li class="collection-item avatar">
                                        <img src="https://img.icons8.com/ios/50/000000/time-machine.png"  alt="" class="circle"/>
                                        <span class="title"><strong>${entry.status}</strong></span>
                                        <p><c:if test="${not empty entry.comment}">
                                            Comment: ${entry.comment}
                                           </c:if>
                                        </p>
                                        <p class="secondary-content">${entry.dateOfCreation}</p>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s3"></div>
</div>

<%@ include file="./common/footer.jspf" %>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>