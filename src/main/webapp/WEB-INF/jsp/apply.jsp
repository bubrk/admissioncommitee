<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Apply for programme" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>

<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="row">
    <div class="col s3"></div>
    <div class="col s6">
        <div class="col s12 m12">
            <h3 class="header center-align">Apply for programme</h3>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <div class="row">
                            <c:if test="${not empty sessionScope.errorMessage}">
                                <div class="row red accent-1">
                                    <h6 class="center-align">${errorMessage}</h6>
                                </div>
                                <c:remove var="errorMessage" scope="session" />
                            </c:if>

                            <h5>${programme.name}</h5>
                            <p>${programme.shortDescription}</p>
                            <br>
                            <p>
                                Budget places: ${programmeDetails.budgetPlaces}; Total places: ${programmeDetails.totalPlaces}
                            </p>
                        </div>

                        <div class="row">
                            <h6 class="center-align red-text text-darken-1">${applyError}</h6>
                            <c:remove var="applyError" scope="session"/>

                            <form class="col s12" action="${appName}/app" method="post">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="firstName" value="${loggedUser.firstName}" type="text" disabled>
                                        <label for="firstName">First Name</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="lastName" value="${loggedUser.lastName}" type="text" disabled>
                                        <label for="firstName">Last Name</label>
                                    </div>
                                </div>

                                <div class="row">

                                    <h5>Requirements:</h5>

                                    <c:forEach var="group" items="${programmeRequirements}" varStatus="loop">
                                        <div class="input-field col s6">
                                            <select name="group_subject_${loop.index}" class="validate" required aria-required="true">
                                                <option value="">Select subject</option>
                                                <c:forEach var="subject" items="${group.subjects}">
                                                    <option value="${subject.id}">${subject.name}</option>
                                                </c:forEach>
                                            </select>
                                            <label><ex:subjectIndex index="${loop.index}"/></label>
                                            <span class="helper-text" data-error="Please, choose subject" data-success="ok"></span>
                                        </div>

                                        <div class="input-field col s4">
                                            <input id="score${loop.index}" name="group_score_${loop.index}" value="" type="number" class="validate" min="${group.minScore}" max="200" required aria-required="true">
                                            <label for="score${loop.index}">Score (min: ${group.minScore})</label>
                                            <span class="helper-text" data-error="Please, enter your score" data-success="ok"></span>
                                        </div>
                                    </c:forEach>
                                </div>

                                <div class="row">
                                    <div class="center-align">
                                        <input name="programmeId" value="${programme.id}" type="hidden">
                                        <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="apply">
                                            Submit
                                        </button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s3"></div>
</div>

<%@ include file="./common/footer.jspf" %>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>