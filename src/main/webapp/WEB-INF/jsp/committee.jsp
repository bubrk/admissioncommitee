<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Commitee page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">

        <h3 class="header center-align">Our respectable members of admission committee</h3>

    <div class="row">

        <div class="col s12 m8 offset-m2 l8 offset-l2">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s2">
                        <img src="./static/img/Dumbledore.jpg" alt="" class="responsive-img">
                    </div>
                    <div class="col s10">
              <span class="black-text">
                  <p><b>Albus Dambledore</b></p> Headmaster of the Philosophical and Wizarding Departments
              </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m8 offset-m2 l8 offset-l2">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s2">
                        <img src="./static/img/Schrodinger.jpg" alt="" class="responsive-img">
                    </div>
                    <div class="col s10">
              <span class="black-text">
                  <p><b>Erwin Schrödinger</b></p> Head of the Department of Physics and Mathematics. Or not
              </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m8 offset-m2 l8 offset-l2">
            <div class="card-panel grey lighten-5 z-depth-1">
                <div class="row valign-wrapper">
                    <div class="col s2">
                        <img src="./static/img/Kolesnikov.jpg" alt="" class="responsive-img">
                    </div>
                    <div class="col s10">
              <span class="black-text">
                  <p><b>Dmytro Kolesnikov</b></p> Head of Software Engineering and Entertainment department
              </span>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


<jsp:include page="./common/footer.jspf"/>

</body>
</html>