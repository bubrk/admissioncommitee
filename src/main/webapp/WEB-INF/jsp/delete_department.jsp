<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Delete department" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <h3 class="header center-align">
        Delete department
    </h3>

    <div class="row">

        <div class="col s2">

        </div>

        <div class="col s8">
            <div class="row">
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content">
                            <form class="col s12" action="${appName}/app" method="post">
                                <div class="row">
                                    <div class="row center-align">
                                        <c:if test="${not empty errorMessage}">
                                            <h6 class="center-align row red accent-1">${errorMessage}</h6>
                                            <c:remove var="errorMessage" scope="session" />
                                        </c:if>
                                        <p>Warning! If you press delete button you will delete department forever. Recovery is not possible.</p>
                                        <p>Make sure department does not contain any programme or other departments. You can delete only an empty department.</p>
                                    </div>

                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" id="confirmCheckBox" onchange="confirm()">
                                            <span class="lever"></span>
                                            I understand, continue anyway
                                        </label>
                                    </div>
                                    <input type="hidden" name="confirmationKey" value="${confirmationKey}">
                                    <input type="hidden" name="departmentId" value="${departmentId}">
                                    <c:remove var="departmentId" scope="session"/>
                                    <br>
                                    <div class="row center-align">
                                        <button class="btn waves-effect waves-light red" disabled type="submit" id="confirmButton"  name="cmd" value="deleteDepartment">
                                            DELETE
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col s2">

        </div>

    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>
<script type="text/javascript" src="${appName}/static/js/confirm.js"></script>

</body>
</html>