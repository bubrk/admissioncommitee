<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Delete programme" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <h3 class="header center-align">
        Delete programme
    </h3>

    <div class="row">

        <div class="col s2">

        </div>

        <div class="col s8">
            <div class="row">
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content">

                            <form class="col s12" action="${appName}/app" method="post">

                                <div class="row">
                                    <div class="row center-align">
                                        <p>Warning! If you press delete button you will delete programme and all applications forever. Recovery is not possible</p>
                                    </div>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" id="confirmCheckBox" onchange="confirm()">
                                            <span class="lever"></span>
                                            I understand, continue anyway
                                        </label>
                                    </div>
                                    <input type="hidden" name="confirmationKey" value="${confirmationKey}">
                                    <input type="hidden" name="programmeId" value="${programmeId}">
                                    <c:remove var="programmeId" scope="session"/>
                                    <br>
                                    <div class="row center-align">
                                        <button class="btn waves-effect waves-light red" disabled type="submit" id="confirmButton"  name="cmd" value="deleteProgramme">
                                            DELETE
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col s2">

        </div>

    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>
<script type="text/javascript" src="${appName}/static/js/confirm.js"></script>

</body>
</html>