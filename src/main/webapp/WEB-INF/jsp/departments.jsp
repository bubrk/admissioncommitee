<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Welcome page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <c:if test="${empty parentDepartment}">
        <h3 class="header center-align">Specialities and educational programs</h3>
    </c:if>
    <c:if test="${not empty parentDepartment}">
        <a href="${appName}/app?cmd=departments&parentId=${parentDepartment.parentId}">
            <h3 class="header">
                <i class="material-icons">navigate_before</i>
                    ${parentDepartment.name}
            </h3>
        </a>
    </c:if>


    <div class="row">

        <c:forEach var="department" items="${departments}">
            <div class='col s12 m4'>
                <ex:card title="${department.name}"
                         href="${appName}/app?cmd=departments&parentId=${department.getId()}"
                         style="teal lighten-5">
                    <p>${department.description}</p>
                </ex:card>
            </div>
        </c:forEach>

    </div>

    <div class="row">
        <c:forEach var="programme" items="${programmes}">
            <div class='col s12 m4'>
                <ex:card title="${programme.name}"
                         href="${appName}/app?cmd=programme&programmeId=${programme.id}"
                         style="green lighten-4">
                    <p>${programme.shortDescription}</p>
                </ex:card>
            </div>
        </c:forEach>
    </div>


</div>


<jsp:include page="./common/footer.jspf"/>

</body>
</html>