<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Edit department" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <h3 class="header center-align">
        Edit department
    </h3>

    <div class="row">

        <div class="col s2">

        </div>

        <div class="col s8">
            <div class="row">
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content">

                            <form class="col s12" action="${appName}/app" method="post">
                                <c:if test="${not empty sessionScope.errorMessage}">
                                    <div class="row red accent-1">
                                        <h6 class="center-align">${errorMessage}</h6>
                                    </div>
                                    <c:remove var="errorMessage" scope="session" />
                                </c:if>
                                <div class="row">
                                    <p class="center-align">Please, enter information about department</p>
                                    <div class="input-field col s12">
                                        <input id="name" name="name" value="${empty name ? department.name : name}" type="text" class="validate" minlength="3" maxlength="150" required="" aria-required="true">
                                        <c:remove var="name" scope="session"/>
                                        <label for="name">Enter name of the department</label>
                                        <span class="helper-text" data-error="Please, enter department name" data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <textarea id="description" name="description" class="materialize-textarea validate"
                                                  minlength="10" maxlength="255" required="" aria-required="true">${empty description ? department.description : description}</textarea>
                                        <c:remove var="description" scope="session"/>
                                        <label for="description">Please, provide some description</label>
                                        <span class="helper-text" data-error="Please, enter department description" data-success="ok"></span>
                                    </div>
                                    <input name="parentId" value="${empty parentId ? department.parentId : parentId}" type="hidden">
                                    <c:remove var="parentId" scope="session"/>
                                    <input name="departmentId" value="${empty departmentId ? department.id : department.id}" type="hidden">
                                    <div class="col s12">
                                        <p>You can also <u><a class="red-text text-accent-1" href="${appName}/app?cmd=deleteDepartment&departmentId=${empty departmentId ? department.id : department.id}">delete</a></u> this department completely</p><br>
                                    </div>
                                    <c:remove var="departmentId" scope="session"/>
                                    <div class="row center-align">
                                        <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="editDepartment">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col s2">

        </div>

    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>