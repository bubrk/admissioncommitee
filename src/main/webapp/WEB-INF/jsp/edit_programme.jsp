<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Edit programme" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <h3 class="header center-align">
        Edit Programme
    </h3>

    <div class="row">
        <div class="col s2">
        </div>

        <div class="col s8">
            <div class="row">
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content">

                            <form id="programmeForm" class="col s12" action="${appName}/app" method="post"
                                  onsubmit="event.preventDefault(); submitForm('programmeForm')">
                                <c:if test="${not empty sessionScope.errorMessage}">
                                    <div class="row red accent-1">
                                        <h6 class="center-align">${errorMessage}</h6>
                                    </div>
                                    <c:remove var="errorMessage" scope="session"/>
                                </c:if>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="name" name="programmeName"
                                               value="${empty editProgrammeParams ? editProgramme.name : editProgrammeParams['programmeName']}"
                                               type="text" class="validate" minlength="3" maxlength="150" required=""
                                               aria-required="true">
                                        <label for="name">Enter name of the programme</label>
                                        <span class="helper-text" data-error="Please, enter programme name"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="shortDescription" name="shortDescription"
                                               value="${empty editProgrammeParams ? editProgramme.shortDescription : editProgrammeParams['shortDescription']}"
                                               type="text" class="validate" minlength="10" maxlength="255" required=""
                                               aria-required="true">
                                        <label for="shortDescription">Enter short description of the programme</label>
                                        <span class="helper-text"
                                              data-error="Please, enter short description of the programme"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <textarea id="description" name="programmeDescription"
                                                  type="text" class="validate materialize-textarea" minlength="10"
                                                  maxlength="500" required=""
                                                  aria-required="true">${empty editProgrammeParams ? editProgrammeDetails.description : editProgrammeParams['programmeDescription']}</textarea>
                                        <label for="description">Enter full description of the programme</label>
                                        <span class="helper-text"
                                              data-error="Please, enter full description of the programme"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="budgetPlaces" name="budgetPlaces" value="${empty editProgrammeParams ? editProgrammeDetails.budgetPlaces : editProgrammeParams['budgetPlaces']}" type="number"
                                               class="validate"
                                               min="1" required aria-required="true">
                                        <label for="budgetPlaces">Budget places</label>
                                        <span class="helper-text" data-error="Please, amount of budget places"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="totalPlaces" name="totalPlaces" value="${empty editProgrammeParams ? editProgrammeDetails.totalPlaces : editProgrammeParams['totalPlaces']}" type="number"
                                               class="validate"
                                               min="1" required aria-required="true">
                                        <label for="totalPlaces">Total places</label>
                                        <span class="helper-text" data-error="Please, total amount of places"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="admissionsStartDate" name="admissionsStartDate"
                                               value="${empty editProgrammeParams ? editProgrammeDetails.admissionsStartDate : editProgrammeParams['admissionsStartDate']}"
                                               type="text" class="datepicker"
                                               required="" aria-required="true">
                                        <label for="admissionsStartDate">Start date for applications</label>
                                        <span class="helper-text" data-error="Please, enter correct date"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="admissionsEndDate" name="admissionsEndDate"
                                               value="${empty editProgrammeParams ? editProgrammeDetails.admissionsEndDate : editProgrammeParams['admissionsEndDate']}"
                                               type="text" class="datepicker"
                                               required="" aria-required="true">
                                        <label for="admissionsEndDate">Last date for applications</label>
                                        <span class="helper-text" data-error="Please, enter correct date"
                                              data-success="ok"></span>
                                    </div>
                                    <div class="col s12">
                                        <br>
                                        <p>Requirements for programme:</p>
                                    </div>
                                    <input id="subjectList" type="hidden" value='${subjectList}'>
                                    <div id="subjectRequirements">
                                        <c:if test="${not empty editProgrammeRequirements}">
                                            <input type="hidden" id="groups_amount" name="groups_amount" value="${editProgrammeRequirements.size()}">
                                            <c:forEach var="group" items="${editProgrammeRequirements}" varStatus="loop">
                                                <c:set var="group_index" value="${loop.index+1}" scope="page"/>
                                                <div id="group_${group_index}">
                                                    <div class="input-field col s12">
                                                        <input type="hidden" id="initial_subjects_group_${group_index}" value="<c:forEach var="subject" items="${group.subjects}">${subject.name},</c:forEach>">
                                                        <div id="chips_${group_index}" class="chips chips-autocomplete chips-initial">
                                                            <input class="custom-class" name="subjects_group_${group_index}"/>
                                                        </div>
                                                        <input type="hidden" id="subjects_group_${group_index}" name="subjects_group_${group_index}">
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <input id="score_${group_index}" name="score_group_${group_index}" value="${group.minScore}" type="number"
                                                               class="validate"
                                                               min="100" max="200" required aria-required="true">
                                                        <label for="score_${group_index}">Min score</label>
                                                        <span class="helper-text" data-error="Please, enter your score"
                                                              data-success="ok"></span>
                                                    </div>
                                                    <div class="input-field col s6">
                                                        <input id="coef_${group_index}" name="coef_group_${group_index}" value="${group.coefficient}" type="number" class="validate"
                                                               min="0.01" step ="0.01" max="1" required aria-required="true">
                                                        <label for="coef_${group_index}">Coeficient</label>
                                                        <span class="helper-text" data-error="Please, enter coeficient"
                                                              data-success="ok"></span>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                    <div class="col s12">
                                        Add another group of subjects
                                        <a class="btn-floating btn-small waves-effect waves-light"
                                           onclick="addSubjectGroup()"><i class="material-icons">add</i></a>
                                    </div>

                                    <c:if test="${not empty programmeId}">
                                        <div class="col s12">
                                            <br>
                                            <p>You can also <u><a class="red-text text-accent-1"
                                                                  href="${appName}/app?cmd=deleteProgramme&programmeId=${programmeId}">delete</a></u>
                                                this programme completely</p><br>
                                        </div>
                                    </c:if>

                                    <div class="row center-align">
                                        <div class="col s12">
                                            <c:if test="${not empty programmeId}">
                                                <input type="hidden" name="programmeId" value="${programmeId}">
                                            </c:if>
                                            <input type="hidden" name="departmentId"
                                                   value="${departmentId}">
                                            <input type="hidden" name="cmd" value="saveProgramme">
                                            <button class="btn waves-effect waves-light" type="submit">
                                                Save
                                            </button>
                                            <c:remove var="editProgramme" scope="session"/>
                                            <c:remove var="editProgrammeDetails" scope="session"/>
                                            <c:remove var="editProgrammeRequirements" scope="session"/>
                                            <c:remove var="editProgrammeParams" scope="session"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col s2">

        </div>


    </div>

</div>

<c:remove var="editProgrammeParams" scope="session"/>

<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>
<script type="text/javascript" src="${appName}/static/js/datepicker.js"></script>
<%--<script type="text/javascript" src="${appName}/static/js/chip_init.js"></script>--%>
<script type="text/javascript" src="${appName}/static/js/subject_groups.js"></script>

</body>
</html>