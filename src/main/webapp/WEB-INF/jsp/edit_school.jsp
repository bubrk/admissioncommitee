<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Programme information" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<c:if test="${not empty updatedUserDetails}">
    <c:set var="userDetailes" value="${updatedUserDetails}" scope="page"/>
</c:if>

<div class="container">
    <h3 class="header center-align">
        Manage your profile
    </h3>

    <div class="row">

        <div class="col s2">

        </div>

        <div class="col s8">
            <div class="row">
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content">

                            <form class="col s12" action="${appName}/app" method="post">
                                <c:if test="${not empty sessionScope.errorMessage}">
                                    <div class="row red accent-1">
                                        <h6 class="center-align">${errorMessage}</h6>
                                    </div>
                                    <c:remove var="errorMessage" scope="session" />
                                </c:if>
                                <div class="row">
                                    <p class="center-align">Your school</p>
                                    <div class="input-field col s12">
                                        <input id="region" name="region" value="${userDetails.region}" type="text" class="validate autocomplete" minlength="3" required="" aria-required="true" placeholder="Київська обл." >
                                        <label for="region">Please, enter your region</label>
                                        <span class="helper-text" data-error="Please, enter your region" data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="city" name="city" value="${userDetails.city}" type="text" class="validate" minlength="3" required="" aria-required="true" placeholder="Київ">
                                        <label for="city">Please, enter your city</label>
                                        <span class="helper-text" data-error="Please, enter your city" data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="school" name="school" value="${userDetails.school}" type="text" class="validate" minlength="3" required="" aria-required="true" placeholder="Школа №1">
                                        <label for="school">Please, enter your school</label>
                                        <span class="helper-text" data-error="Please, enter your school" data-success="ok"></span>
                                    </div>
                                    <div class="row center-align">
                                        <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="editSchool">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="col s2">

        </div>

    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>