<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Login page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<c:if test="${loggedUser!=null}">
    <%@ include file="./common/sidenav.jspf" %>
</c:if>


<div class="container">
<div class="row">
    <div class="col s3"></div>
    <div class="col s6">
        <div class="col s12 m12">
            <h3 class="header center-align">Login</h3>
            <div class="card horizontal">
                <c:if test="${loggedUser==null}">
                    <div class="card-stacked">
                        <div class="card-content">
                            <c:if test="${not empty sessionScope.errorMessage}">
                                <div class="row red accent-1">
                                    <h6 class="center-align">${errorMessage}</h6>
                                </div>
                                <c:remove var="errorMessage" scope="session" />
                            </c:if>


                            <div class="row">
                                <form class="col s12" action="${appName}/app" method="post">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="email" name="email" value="${loginEmail}" type="email" class="validate" required="" aria-required="true">
                                            <label for="email">Email</label>
                                            <span class="helper-text" data-error="Please, enter a valid email address" data-success=""></span>
                                        </div>
                                        <c:remove var="loginEmail" scope="session" />
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="password" name="password" type="password" class="validate" minlength="8" required="" aria-required="true">
                                            <label for="password">Password</label>
                                            <span class="helper-text" data-error="Please, enter your password" data-success=""></span>
                                        </div>
                                    </div>
                                    <div class="row center-align">
                                        <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="login">
                                            Login
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="card-action">
                            <span class="center-align"><p>Don't have an account? <a href="${appName}/app?cmd=register">Create one</a></p></span>
                        </div>
                    </div>
                </c:if>

                <c:if test="${loggedUser!=null}">
                    <div class="card-stacked">
                        <div class="card-content">
                            <div class="row center-align">
                                <p>You already logged in as <em>${loggedUser.firstName} ${loggedUser.lastName}</em></p>
                                <br>
                            </div>
                            <div class="row center-align">
                                <form action="${appName}/app" method="get">
                                    <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="logout">
                                        Logout
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
    <div class="col s3"></div>
</div>
</div>
<%@ include file="./common/footer.jspf" %>

</body>
</html>