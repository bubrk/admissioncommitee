<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Manage applications" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">

    <h3 class="header">
        Manage applications
    </h3>


    <div class="row">
        <div class="row">
            <form>
                <div class="input-field col s4">
                    <i class="prefix"><img src="https://img.icons8.com/ios/32/000000/filter-and-sort.png"/></i>
                    <input name="name" value="${name}" type="text" id="autocomplete-input" class="autocomplete">
                    <label for="autocomplete-input">Filter by name</label>
                </div>
                <div class="input-field col s4">
                    <i class="prefix"><img src="https://img.icons8.com/ios/32/000000/filter-and-sort.png"/></i>
                    <input name="programme" value="${programme}" type="text" id="autocomplete-programme" class="autocomplete">
                    <label for="autocomplete-programme">Filter by programme</label>
                </div>
                <div class="input-field col s2">
                    <select name="status">
                        <option value="NEW" selected>New</option>
                        <option value="ALL" >All</option>
                    </select>
                    <label>Status</label>
                </div>
                <div class="input-field col s1">
                    <select name="pageSize">
                        <option value="10" ${pageSize=="10"?"selected":""}>10</option>
                        <option value="20" ${pageSize=="20"?"selected":""}>20</option>
                        <option value="50" ${pageSize=="50"?"selected":""}>50</option>
                    </select>
                    <label>Qty on page</label>
                </div>
                <div class="input-field col s1">
                    <button class="btn waves-effect waves-light" type="submit" name="cmd" value="manageApplications">
                        Search
                    </button>
                </div>
            </form>

        </div>
        <table class="highlight">
            <thead>
            <tr>
                <th class="left-align">Abiturient</th>
                <th class="left-align">Programme</th>
                <th class="center-align">Status</th>
                <th class="center-align">Date</th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${empty searchResult}">
                <p class="center-align">Nothing found</p>
            </c:if>
            <c:if test="${not empty searchResult}">

                <c:forEach var="entry" items="${searchResult}">

                    <tr style="cursor: pointer;"
                        onclick="window.location='${appName}/app?cmd=manageOneApplication&applicationId=${entry.application.id}'">

                        <td class="left-align">${entry.user.firstName} ${entry.user.middleName} ${entry.user.lastName}</td>
                        <td class="left-align">${entry.programme.name}</td>
                        <td class="center-align">${entry.currentStatus.status}</td>
                        <td class="center-align">${entry.currentStatus.dateOfCreation}</td>


                    </tr>

                </c:forEach>
            </c:if>
            </tbody>
        </table>


<%--        <c:if test="${not empty searchResult}">--%>
<%--            <ex:pagination currentPage="${searchResult.currentPage}"--%>
<%--                           lastPage="${searchResult.totalPages}"--%>
<%--                           url="${SELF}"/>--%>
<%--        </c:if>--%>
    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.autocomplete');
        var instances = M.Autocomplete.init(elems, {
            minLength:1,
            data: {
                <c:forEach var="programme" items="${allProgrammes}">
                    "${programme.name}":null,
                </c:forEach>
            }
        });
    });
</script>

</body>
</html>