<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Welcome page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <c:if test="${empty parentDepartment}">
        <h4 class="header center-align">Specialities and educational programs</h4>
    </c:if>
    <c:if test="${not empty parentDepartment}">
        <a href="${appName}/app?cmd=manageDepartments&parentId=${parentDepartment.parentId}">
            <h4 class="header center-align">
                <i class="material-icons">navigate_before</i>
                    ${parentDepartment.name}
            </h4>
        </a>
    </c:if>


    <div class="row">
        <div class="col s2">
        </div>
        <div class="col s8">
            <ul class="collection">
                <c:if test="${not empty departments}">

                    <c:forEach var="department" items="${departments}">
                        <li class="collection-item avatar" style="cursor: pointer;"
                            onclick="window.location='${appName}/app?cmd=manageDepartments&parentId=${department.getId()}'">
                            <img src="https://img.icons8.com/external-kmg-design-outline-color-kmg-design/32/000000/external-folder-business-management-kmg-design-outline-color-kmg-design.png"
                                 alt="" class="circle"/>
                            <span class="title">
                            <h6>${department.name}</h6>
                        </span>
                            <a href="${appName}/app?cmd=editDepartment&departmentId=${department.getId()}"
                               class="secondary-content btn-floating btn waves-effect waves-light blue-grey"><i
                                    class="material-icons">edit</i></a>
                        </li>
                    </c:forEach>
                </c:if>
                <li class="collection-item avatar">

                    <a href="${appName}/app?cmd=addDepartment&parentId=${empty parentDepartment.getId() ? 0 : parentDepartment.getId()}"
                       class="circle btn-floating btn waves-effect waves-light blue-grey">
                        <i class="material-icons">add</i>
                    </a>

                    <span class="title">
                            <h6>Add new department</h6>
                        </span>

                </li>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="col s2">
        </div>
        <div class="col s8">
            <c:if test="${not empty parentDepartment}">
                <ul class="collection">
                    <c:if test="${not empty programmes}">
                        <c:forEach var="programme" items="${programmes}">
                            <li class="collection-item avatar">
                                <img src="https://img.icons8.com/external-kmg-design-outline-color-kmg-design/32/000000/external-id-card-business-management-kmg-design-outline-color-kmg-design.png"
                                     alt="" class="circle"/>
                                <span class="title">
                                <h6>${programme.name}</h6>
                            </span>
                                <a href="${appName}/app?cmd=editProgramme&programmeId=${programme.getId()}&departmentId=${parentDepartment.id}"
                                   class="secondary-content btn-floating btn waves-effect waves-light blue-grey">
                                    <i class="material-icons">edit</i></a>
                            </li>
                        </c:forEach>
                    </c:if>

                    <li class="collection-item avatar">
                        <a href="${appName}/app?cmd=editProgramme&departmentId=${parentDepartment.id}&newProgramme=true"
                           class="circle btn-floating btn waves-effect waves-light blue-grey">
                            <i class="material-icons">add</i>
                        </a>
                        <span class="title">
                            <h6>Add new educational programme</h6>
                        </span>
                    </li>
                </ul>
            </c:if>
        </div>
    </div>

</div>

<script type="text/javascript" src="${appName}/static/js/fab.js"></script>

<jsp:include page="./common/footer.jspf"/>


</body>
</html>