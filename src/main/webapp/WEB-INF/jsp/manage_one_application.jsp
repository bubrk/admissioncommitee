<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Manage application" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>

<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="row">
    <h3 class="header center-align">Manage application (#${application.id})</h3>
    <div class="col s1">
    </div>
    <div class="col s5">
        <div class="col s12 m12">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">

                        <div class="row">
                            <h5>
                                <strong>${user.firstName} ${user.middleName!=null?user.middleName:""} ${user.lastName}</strong>
                            </h5>
                            <br>
                            <h6><i class="material-icons tiny">location_on</i>${userDetails.region}</h6>
                            <h6><i class="material-icons tiny">location_on</i>${userDetails.city}</h6>
                            <h6><i class="material-icons tiny">school</i>${userDetails.school}</h6>
                            <h6><i class="material-icons tiny">mail</i> <a href="mailto:${user.email}">${user.email}</a>
                            </h6>
                            <br>
                        </div>
                        <div class="row">
                            <h5>Scores: </h5>
                            <br>
                            <c:forEach var="entry" items="${scores}">
                                <div class="chip">
                                        ${entry.key.name} - ${entry.value.score}
                                </div>
                            </c:forEach>
                        </div>
                        <div class="row">
                            <h5>Uploaded documents</h5>
                            <c:if test="${empty userDocuments}">
                                There are no uploaded documents
                            </c:if>
                            <c:forEach var="doc" items="${userDocuments}">
                                <span class="chip">
                                    <a href="${appName}/app?cmd=getDocument&documentId=${doc.id}">
                                            ${doc.name}
                                    </a>
                                </span>
                            </c:forEach>

                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s5">
        <div class="col s12 m12">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <div class="row">
                            <h5><strong>${programme.name}</strong></h5>
                            <br>
                            <h5>Requirements: </h5>
                            <c:forEach var="group" items="${programmeRequirements}" varStatus="loop">

                                <ex:subjectIndex index="${loop.index}"/>

                                <c:forEach var="subject" items="${group.subjects}">
                                    <div class="chip">
                                            ${subject.name}
                                    </div>
                                </c:forEach>
                                <p>
                                    Minimum score: <span class="">${group.minScore}, </span>
                                </p>
                                <br>
                            </c:forEach>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s3"></div>
    <div class="col s6">
        <c:if test="${currentStatus.status == 'NEW'}">
            <div class="row">
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content">

                            <form class="col s12" action="${appName}/app" method="post">
                                <div class="input-field col s12">
                                        <textarea id="comment" name="comment" class="materialize-textarea validate"
                                                  maxlength="255"></textarea>
                                    <label for="comment">Please, provide some comment</label>
                                    <span class="helper-text" data-error="Please, provide some comment"
                                          data-success="ok"></span>
                                </div>
                                <input type="hidden" name="applicationId" value="${application.id}">
                                <div class="row center-align">
                                    <button class="btn waves-effect waves-light" type="submit" name="cmd"
                                            value="acceptApplication">
                                        Accept application
                                    </button>
                                    <button class="btn waves-effect waves-light red lighten-2" type="submit"
                                            name="cmd"
                                            value="rejectApplication">
                                        Reject
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <br>
        <div class="row">
            <h5>Status history</h5>
            <ul class="collection">
                <c:forEach var="entry" items="${statusFlow}">
                    <li class="collection-item avatar">
                        <img src="https://img.icons8.com/ios/50/000000/time-machine.png" alt="" class="circle"/>
                        <span class="title"><strong>${entry.status}</strong></span>
                        <p><c:if test="${not empty entry.comment}">
                            Comment: ${entry.comment}
                        </c:if>
                        </p>
                        <p class="secondary-content">${entry.dateOfCreation}</p>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>

<%@ include file="./common/footer.jspf" %>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>