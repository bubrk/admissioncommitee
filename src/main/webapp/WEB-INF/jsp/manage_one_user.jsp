<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Manage application" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>

<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="row">
    <h3 class="header center-align">Manage User (#${user.id})</h3>
    <div class="col s1">
    </div>
    <div class="col s5">
        <div class="col s12 m12">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">

                        <div class="row">
                            <h5>
                                <strong>${user.firstName} ${user.middleName!=null?user.middleName:""} ${user.lastName}</strong>
                            </h5>
                            <br>
                            <h6><i class="material-icons tiny">location_on</i>${userDetails.region}</h6>
                            <h6><i class="material-icons tiny">location_on</i>${userDetails.city}</h6>
                            <h6><i class="material-icons tiny">school</i>${userDetails.school}</h6>
                            <h6><i class="material-icons tiny">mail</i> <a href="mailto:${user.email}">${user.email}</a>
                            </h6>
                            <br>
                        </div>
                        <div class="row">
                            <h5>Uploaded documents</h5>
                            <c:if test="${empty userDocuments}">
                                There are no uploaded documents
                            </c:if>
                            <c:forEach var="doc" items="${userDocuments}">
                                <span class="chip">
                                    <a href="${appName}/app?cmd=getDocument&documentId=${doc.id}">
                                            ${doc.name}
                                    </a>
                                </span>
                            </c:forEach>

                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">

                        <div class="row">
                            <c:if test="${isBlocked}">
                                <form class="col s12" action="${appName}/app" method="post">
                                    <h5>
                                        User is blocked
                                    </h5>
                                    <br>
                                    <c:if test="${not empty sessionScope.errorMessage}">
                                        <div class="row red accent-1">
                                            <h6 class="center-align">${errorMessage}</h6>
                                        </div>
                                        <c:remove var="errorMessage" scope="session"/>
                                    </c:if>
                                    <div class="row">
                                        <c:if test="${not empty comment}">
                                            <h6>Reason: ${comment}</h6>
                                        </c:if>
                                        <div class="row center-align">
                                            <input type="hidden" name="userId" value="${user.id}">
                                            <button class="btn waves-effect waves-light red lighten-2" type="submit"
                                                    name="cmd" value="unblockUser">
                                                Unblock user
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </c:if>
                            <c:if test="${not isBlocked}">
                                <form class="col s12" action="${appName}/app" method="post">
                                    <h5>
                                        Add user to blacklist
                                    </h5>
                                    <br>
                                    <c:if test="${not empty sessionScope.errorMessage}">
                                        <div class="row red accent-1">
                                            <h6 class="center-align">${errorMessage}</h6>
                                        </div>
                                        <c:remove var="errorMessage" scope="session"/>
                                    </c:if>
                                    <div class="row">
                                        <div class="input-field col s12">
                                        <textarea id="comment" name="comment" class="materialize-textarea validate"
                                                  minlength="10" maxlength="255" required=""
                                                  aria-required="true">${empty comment ? "" : comment}</textarea>
                                            <c:remove var="comment" scope="session"/>
                                            <label for="comment">Please, provide your comment</label>
                                            <span class="helper-text" data-error="Please, your comment"
                                                  data-success="ok"></span>
                                        </div>

                                        <div class="row center-align">
                                            <input type="hidden" name="userId" value="${user.id}">
                                            <button class="btn waves-effect waves-light red lighten-2" type="submit"
                                                    name="cmd" value="blockUser">
                                                Block user
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </c:if>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s5">
        <div class="col s12 m12">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <div class="row">
                            <h5>User's applications</h5>
                            <br>
                            <table class="highlight">
                                <thead>
                                <tr>
                                    <th>Index</th>
                                    <th>Programme name</th>
                                    <th>Current status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:if test="${empty applications}">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <p class="center-align">User doesn't have any applications yet</p>
                                        </td>
                                    </tr>
                                </c:if>

                                <c:if test="${not empty applications}">
                                    <c:forEach var="entry" items="${applications}" varStatus="loop">
                                        <tr style="cursor: pointer;"
                                            onclick="window.location='${appName}/app?cmd=manageOneApplication&applicationId=${entry.application.id}'">
                                            <td>${loop.index+1}</td>
                                            <td>${entry.programme.name}</td>
                                            <td>${entry.currentStatus.status}</td>
                                            <td>
                                                <a href="${appName}/app?cmd=manageOneApplication&applicationId=${entry.application.id}"
                                                   class="black-text">
                                                    <i class="material-icons right">info_outline</i>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="./common/footer.jspf" %>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>