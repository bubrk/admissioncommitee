<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Manage users" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">

    <h3 class="header">
        Manage users
    </h3>


    <div class="row">
        <div class="row">
            <form>
                <div class="input-field col s4">
                    <i class="prefix"><img src="https://img.icons8.com/ios/32/000000/filter-and-sort.png"/></i>
                    <input name="firstName" value="${firstName}" type="text" id="autocomplete-firstName" class="autocomplete">
                    <label for="autocomplete-firstName">Filter by first name</label>
                </div>
                <div class="input-field col s4">
                    <i class="prefix"><img src="https://img.icons8.com/ios/32/000000/filter-and-sort.png"/></i>
                    <input name="lastName" value="${lastName}" type="text" id="autocomplete-lastName" class="autocomplete">
                    <label for="autocomplete-lastName">Filter by last name</label>
                </div>
                <div class="input-field col s3">
                    <i class="prefix"><img src="https://img.icons8.com/ios/32/000000/filter-and-sort.png"/></i>
                    <input name="email" value="${email}" type="text" id="autocomplete-email" class="autocomplete">
                    <label for="autocomplete-email">Filter by email</label>
                </div>


                <div class="input-field col s1">
                    <button class="btn waves-effect waves-light" type="submit" name="cmd" value="manageUsers">
                        Search
                    </button>
                </div>
            </form>

        </div>
        <table class="highlight">
            <thead>
            <tr>
                <th class="left-align">First name</th>
                <th class="left-align">Last name</th>
                <th class="left-align">Email</th>
                <th class="left-align">Role</th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${empty users}">
                <p class="center-align">Nothing found</p>
            </c:if>
            <c:if test="${not empty users}">

                <c:forEach var="entry" items="${users}">

                    <tr style="cursor: pointer;"
                        onclick="window.location='${appName}/app?cmd=manageOneUser&userId=${entry.id}'">
                        <td class="left-align">${entry.firstName}</td>
                        <td class="left-align">${entry.lastName}</td>
                        <td class="left-align">${entry.email}</td>
                        <td class="left-align">${entry.role}</td>
                    </tr>

                </c:forEach>
            </c:if>
            </tbody>
        </table>


<%--        <c:if test="${not empty searchResult}">--%>
<%--            <ex:pagination currentPage="${searchResult.currentPage}"--%>
<%--                           lastPage="${searchResult.totalPages}"--%>
<%--                           url="${SELF}"/>--%>
<%--        </c:if>--%>
    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>