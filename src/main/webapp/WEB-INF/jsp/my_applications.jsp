<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Register page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>

<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="row">
    <div class="col s2"></div>
    <div class="col s8">
        <div class="col s12 m12">
            <h3 class="header center-align">My applications</h3>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <table class="highlight">
                            <thead>
                            <tr>
                                <th>Index</th>
                                <th>Programme name</th>
                                <th>Current status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:if test="${empty searchResults}">
                                <tr>
                                    <td></td>
                                    <td>
                                        <p class="center-align">You don't have any applications yet</p>
                                    </td>
                                </tr>
                            </c:if>

                            <c:if test="${not empty searchResults}">

                                <c:forEach var="entry" items="${searchResults}" varStatus="loop">
                                    <tr style="cursor: pointer;"
                                    onclick="window.location='${appName}/app?cmd=application&applicationId=${entry.application.id}'">

                                        <td>${loop.index+1}</td>
                                        <td>${entry.programme.name}</td>
                                        <td>${entry.currentStatus.status}</td>
                                        <td><a href="${appName}/app?cmd=application&applicationId=${entry.application.id}" class="black-text">
                                                <i class="material-icons right">info_outline</i>
                                            </a>
                                        </td>

                                    </tr>

                                </c:forEach>
                            </c:if>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s2"></div>
</div>

<%@ include file="./common/footer.jspf" %>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>