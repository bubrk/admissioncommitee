<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Programme information" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <h3 class="header center-align">
        Manage your profile
    </h3>

    <div class="row">
        <c:if test="${not empty sessionScope.errorMessage}">
            <div class="row red accent-1">
                <h6 class="center-align">${errorMessage}</h6>
            </div>
            <c:remove var="errorMessage" scope="session"/>
        </c:if>
        <div class="col s1">

        </div>
        <div class="col s10">

            <ul class="collapsible">
                <li class="active">
                    <div class="collapsible-header">

                        <i class="material-icons">account_circle</i>Credentials
                        <c:if test="${not empty loggedUser}">
                            <i class="material-icons green-text right">check</i>
                        </c:if>
                        <c:if test="${empty loggedUser}">
                            <i class="material-icons red-text right">check_box_outline_blank</i>
                        </c:if>

                    </div>
                    <div class="collapsible-body">
                        <div class="row">
                            <table>
                                <tbody>
                                <tr>
                                    <td>First Name:</td>
                                    <td>${loggedUser.firstName}</td>
                                </tr>
                                <tr>
                                    <td>Last Name:</td>
                                    <td>${loggedUser.lastName}</td>
                                </tr>
                                <tr>
                                    <td>Middle Name:</td>
                                    <td>${loggedUser.middleName}</td>
                                </tr>

                                <tr>
                                    <td>Email:</td>
                                    <td>${loggedUser.email}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
<%--                            <a class="waves-effect waves-light btn-small teal lighten-4">--%>
<%--                                <i class="material-icons left">edit</i>Edit</a>--%>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">description</i>School information
                        <c:if test="${not empty userDetails}">
                            <i class="material-icons green-text right">check</i>
                        </c:if>
                        <c:if test="${empty userDetails}">
                            <i class="material-icons red-text right">check_box_outline_blank</i>
                        </c:if>
                    </div>
                    <div class="collapsible-body">
                        <c:if test="${not empty userDetails}">
                            <div class="row">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Region:</td>
                                        <td>${userDetails.region}</td>
                                    </tr>
                                    <tr>
                                        <td>City:</td>
                                        <td>${userDetails.city}</td>
                                    </tr>
                                    <tr>
                                        <td>School:</td>
                                        <td>${userDetails.school}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <a href="${appName}/app?cmd=editSchool"
                                   class="waves-effect waves-light btn-small teal lighten-4">
                                    <i class="material-icons left">edit</i>Edit</a>
                            </div>
                        </c:if>
                        <c:if test="${empty userDetails}">
                            <div class="row">
                                <h6>Please, add information about your school</h6>
                                <a href="${appName}/app?cmd=editSchool"
                                   class="waves-effect waves-light btn-small teal lighten-4">
                                    <i class="material-icons left">edit</i>Add information</a>
                            </div>
                        </c:if>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header">
                        <i class="material-icons">attach_file</i>Documents
                        <c:if test="${not empty userDocuments}">
                            <i class="material-icons green-text right">check</i>
                        </c:if>
                        <c:if test="${empty userDocuments}">
                            <i class="material-icons red-text right">check_box_outline_blank</i>
                        </c:if>
                    </div>
                    <div class="collapsible-body">
                        <div class="row">
                            <c:if test="${not empty userDocuments}">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Documents added:</td>
                                        <td>
                                            <c:forEach var="doc" items="${userDocuments}">
                                                <span class="chip">
                                                    <a href="${appName}/app?cmd=getDocument&documentId=${doc.id}">
                                                            ${doc.name}
                                                    </a>
                                                    <a onclick="post('${appName}/app?cmd=deleteDocument&documentId=${doc.id}', {})">
                                                        <i class="close material-icons">close</i>
                                                    </a>
                                                </span>
                                                <br>
                                            </c:forEach>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </c:if>
                            <c:if test="${empty userDocuments}">
                                You don't have any documents uploaded yet
                            </c:if>
                        </div>
                        <div class="row">
                            <div id="preloader" class="progress hiddendiv">
                                <div class="indeterminate"></div>
                            </div>
                            <form action="${appName}/app?cmd=uploadDocument" method="post"
                                  enctype="multipart/form-data">
                                <div class="file-field input-field col s6">
                                    <div class="btn small">
                                        <span>File</span>
                                        <input type="file" name="fileName">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input id="file_name" class="file-path validate" type="text">
                                        <label for="file_name">Choose document for uploading</label>
                                    </div>
                                </div>
                                <div class="input-field col s6">
                                    <button class="btn waves-effect waves-light" type="submit" name="action"
                                            onclick="showPreloader('preloader')">Upload
                                        <i class="material-icons right">upload</i>
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </li>

            </ul>

        </div>


    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/collapse.js"></script>
<script type="text/javascript" src="${appName}/static/js/preloader.js"></script>
<script type="text/javascript" src="${appName}/static/js/post.js"></script>

</body>
</html>