<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Programme information" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">
    <a href="${appName}/app?cmd=departments&parentId=${department.id}">
        <h3 class="header">
            <i class="material-icons">navigate_before</i>
            ${department.name}
        </h3>
    </a>

    <div class="row">

        <div class="col s8">
            <ul class="collection with-header">
                <li class="collection-header">
                    <h4>${programme.name}</h4>
                </li>
                <li class="collection-item">
                    ${programmeDetails.description}
                </li>
                <li class="collection-item">
                    Budget places: ${programmeDetails.budgetPlaces}
                    <br>
                    Total places: ${programmeDetails.totalPlaces}
                </li>
            </ul>
            <br>

            <ul class="collection with-header">
                <li class="collection-header">
                    <h5>Requirements</h5>
                </li>

                <c:forEach var="group" items="${programmeRequirements}" varStatus="loop">
                    <li class="collection-item">
                        <ex:subjectIndex index="${loop.index}"/>

                        <c:forEach var="subject" items="${group.subjects}">
                            <div class="chip">
                                    ${subject.name}
                            </div>
                        </c:forEach>
                        <p>
                            Minimum score: <span class="">${group.minScore}, </span>
                            Coefficient: <span class="">${group.coefficient}</span>
                        </p>
                    </li>
                </c:forEach>

            </ul>


        </div>

        <div class="col s4">
            <ex:card title="Information for applicants:"
                     style="teal lighten-5">
                <p>Applications are accepted</p>
                <p>from: ${programmeDetails.admissionsStartDate}</p>
                <p>until: ${programmeDetails.admissionsEndDate}</p>
                <br>
                <c:if test="${not empty applicationsNow}">

                    <a href='${appName}/app?cmd=apply&programmeId=${programme.id}'>
                        <button class="btn waves-effect waves-light pulse">Apply now
                            <i class="material-icons right">add_to_photos</i>
                        </button>
                    </a>
                </c:if>
                <c:if test="${not empty applicationsNotOpenedYet}">
                    <p>Applications are not opened yet</p>
                </c:if>
                <c:if test="${not empty applicationsClosed}">
                    <p>Applications are closed</p>
                </c:if>
            </ex:card>
            <c:if test="${not empty loggedUser}">
                <c:if test="${loggedUser.role=='ADMIN'}">
                    <ex:card title="Management">
                        <ul class="collection">
                            <li class="collection-item">
                                <div>Edit programme<a
                                        href="${appName}/app?cmd=editProgramme&programmeId=${programme.id}&departmentId=${programme.departmentId}"
                                        class="secondary-content">
                                    <i class="material-icons">edit</i></a>
                                </div>
                            </li>
                            <li class="collection-item">
                                <div>Delete programme
                                    <a href="${appName}/app?cmd=deleteProgramme&programmeId=${programme.id}"
                                       class="secondary-content">
                                        <i class="material-icons">delete</i></a>
                                </div>
                            </li>

                            <c:if test="${empty applicationsClosed}">
                                <li class="collection-item">
                                    <div>Reports are not available until the end date of
                                        accepting applications
                                    </div>
                                </li>
                            </c:if>
                            <c:if test="${not empty applicationsClosed}">
                                <c:if test="${not empty reports}">
                                    <c:forEach var="document" items="${reports}">
                                        <li class="collection-item">
                                            <div>Open report:
                                                <a href="${appName}/app?cmd=getReport&reportId=${document.id}"
                                                   class="secondary-content">
                                                    <i class="material-icons">attach_file</i>${document.name}</a>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${empty reports}">
                                    <li class="collection-item">
                                        <form id="finalizeForm" action="${appName}/app" method="post">
                                            <input type="hidden" name="programmeId" value="${programme.getId()}"/>
                                            <input type="hidden" name="cmd" value="finalize"/>
                                            <div>Finalize report
                                                <a href="#"
                                                   class="secondary-content"
                                                   title="Click to finalize the report"
                                                   onclick="document.forms['finalizeForm'].submit(); return false;">
                                                    <i class="material-icons">assignment</i>
                                                </a>
                                            </div>
                                        </form>
                                    </li>
                                </c:if>
                            </c:if>
                        </ul>
                    </ex:card>
                </c:if>
            </c:if>
        </div>

    </div>

</div>


<jsp:include page="./common/footer.jspf"/>

</body>
</html>