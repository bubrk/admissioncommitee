<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Register page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<c:if test="${loggedUser!=null}">
    <c:redirect url="/app?cmd=personalInfo"/>
</c:if>

<%@ include file="./common/navbar.jspf" %>

<div class="row">
    <div class="col s3"></div>
    <div class="col s6">
        <div class="col s12 m12">
            <h3 class="header center-align">Registration form</h3>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">


                        <div class="row">
                            <c:if test="${not empty errorMessage}">
                                <h6 class="center-align red-text text-darken-1">${errorMessage}</h6>
                                <c:remove var="errorMessage" scope="session" />
                            </c:if>

                            <form class="col s12" action="${appName}/app" method="post">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="firstName" name="firstName" value="${newUser.firstName}" type="text" class="validate" minlength="3" required="" aria-required="true">
                                        <label for="firstName">First Name</label>
                                        <span class="helper-text" data-error="Please, enter your first name" data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="middleName" name="middleName" value="${newUser.middleName}" type="text" class="">
                                        <label for="middleName">Middle Name</label>
                                        <span class="helper-text">(optional)</span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="lastName" name="lastName" value="${newUser.lastName}" type="text" class="validate" minlength="3" required="" aria-required="true">
                                        <label for="lastName">Last Name</label>
                                        <span class="helper-text" data-error="Please, enter your last name" data-success="ok"></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="email" name="userEmail" value="${newUser.email}" type="email" class="validate" required="" aria-required="true">
                                        <label for="email">Email</label>
                                        <span class="helper-text" data-error="Please, enter a valid email address" data-success=""></span>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="password" name="userPassword" type="password" class="validate" minlength="8" required="" aria-required="true">
                                        <label for="password">Password</label>
                                        <span class="helper-text" data-error="Please, enter your password" data-success=""></span>
                                    </div>
                                </div>
                                <c:if test="${not empty newUser}">
                                    <c:remove var="newUser" scope="session" />
                                </c:if>

                                <div class="row">
                                    <div class="center-align">
                                        <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="register">
                                            Submit
                                        </button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s3"></div>
</div>

<%@ include file="./common/footer.jspf" %>

</body>
</html>