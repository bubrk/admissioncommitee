<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>

<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Report" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>

<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>
<x:parse xml="${reportXML}" var="output"/>

<div class="row">
    <h3 class="header center-align">Report</h3>
    <div class="col s2">
    </div>
    <div class="col s8">
        <div class="col s12 m12">
            <p>Programme: <em>"<x:out select="$output//programme/name"/>"</em></p>
            <p>Total places: <em><x:out select="$output//programmeDetails/totalPlaces"/></em>;
            Budget places: <em><x:out select="$output//programmeDetails/budgetPlaces"/></em></p>
            <p>Admission started: <em><x:out select="$output//programmeDetails/admissionsStartDate"/></em>;
            Admission ended: <em><x:out select="$output//programmeDetails/admissionsEndDate"/></em></p>
            <ul class="collection">
                <table class="highlight">
                    <thead>
                    <tr>
                        <th class="left-align">Abiturient</th>
                        <th class="left-align">Email</th>
                        <th class="left-align">TotalScore</th>
                        <th class="left-align">Accepted</th>
                        <th class="left-align">Budget</th>
                    </tr>
                    </thead>
                    <tbody>
                    <x:forEach select="$output//reportRows/row" var="row">
                        <tr>
                            <td class="left-align"><x:out select="abiturient"/></td>
                            <td class="left-align"><x:out select="email"/></td>
                            <td class="left-align"><x:out select="totalScore"/></td>
                            <td class="left-align"><x:out select="accepted"/>
                            <td class="left-align"><x:out select="budget"/></td>
                        </tr>
                    </x:forEach>
                    </tbody>
                </table>
            </ul>
        </div>
    </div>
</div>

<%@ include file="./common/footer.jspf" %>

</body>
</html>