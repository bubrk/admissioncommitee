<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Reports" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">

    <h3 class="header">
        Manage reports
    </h3>


    <div class="row">
        <table class="highlight">
            <thead>
            <tr>
                <th class="left-align">Programme</th>
                <th class="left-align">Report</th>
                <th class="left-align">Link</th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${empty reports}">
                <tr><td><p class="center-align">Nothing found</p></td></tr>
            </c:if>
            <c:if test="${not empty reports}">

                <c:forEach var="entry" items="${reports}">

                    <tr>
                        <td class="left-align">${entry.getKey().name}</td>
                        <td class="left-align">${entry.getValue().name}</td>
                        <td class="left-align"><a href="${appName}/app?cmd=getReport&reportId=${entry.getValue().id}">download</a></td>
                    </tr>

                </c:forEach>
            </c:if>
            </tbody>
        </table>


        <%--        <c:if test="${not empty searchResult}">--%>
        <%--            <ex:pagination currentPage="${searchResult.currentPage}"--%>
        <%--                           lastPage="${searchResult.totalPages}"--%>
        <%--                           url="${SELF}"/>--%>
        <%--        </c:if>--%>
    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.autocomplete');
        var instances = M.Autocomplete.init(elems, {
            minLength:1,
            data: {
                <c:forEach var="programme" items="${allProgrammes}">
    "${programme.name}":null,
</c:forEach>
            }
        });
    });


</script>

</body>
</html>