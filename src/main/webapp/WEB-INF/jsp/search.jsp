<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Programme information" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<%@ include file="./common/navbar.jspf" %>
<%@ include file="./common/sidenav.jspf" %>

<div class="container">

    <h3 class="header">
        Search programme
    </h3>


    <div class="row">
        <div class="row">
            <form>
                <div class="input-field col s6">
                    <i class="prefix"><img src="https://img.icons8.com/ios/32/000000/filter-and-sort.png"/></i>
                    <input name="programme" value="${programmeName}" type="text" id="autocomplete-input" class="autocomplete">
                    <label for="autocomplete-input">Programme name</label>
                </div>
                <div class="input-field col s2">
                    <select name="dates">
                        <option value="all" ${dateQuery=="all"?"selected":""}>All programmes</option>
                        <option value="opened" ${dateQuery=="opened"?"selected":""}>Opened now</option>
                        <option value="future" ${dateQuery=="future"?"selected":""}>Future</option>
                        <option value="past" ${dateQuery=="past"?"selected":""}>Past</option>
                    </select>
                    <label>Filter dates</label>
                </div>
                <div class="input-field col s2">
                    <select name="sort">
                        <optgroup label="By name">
                            <option value="byNameAz" ${sortingType=="byNameAz"?"selected":""}>A-z</option>
                            <option value="byNameZa" ${sortingType=="byNameZa"?"selected":""}>z-A</option>
                        </optgroup>
                        <optgroup label="By budget places">
                            <option value="byBudget19" ${sortingType=="byBudget19"?"selected":""}>1-9</option>
                            <option value="byBudget91" ${sortingType=="byBudget91"?"selected":""}>9-1</option>
                        </optgroup>
                        <optgroup label="By total places">
                            <option value="byTotal19" ${sortingType=="byTotal19"?"selected":""}>1-9</option>
                            <option value="byTotal91" ${sortingType=="byTotal91"?"selected":""}>9-1</option>
                        </optgroup>
                    </select>
                    <label>Sorting</label>
                </div>
                <div class="input-field col s1">
                    <select name="pageSize">
                        <option value="10" ${pageSize=="10"?"selected":""}>10</option>
                        <option value="20" ${pageSize=="20"?"selected":""}>20</option>
                        <option value="50" ${pageSize=="50"?"selected":""}>50</option>
                    </select>
                    <label>Qty on page</label>
                </div>
                <div class="input-field col s1">
                    <button class="btn waves-effect waves-light" type="submit" name="cmd" value="searchProgramme">
                        Search
                    </button>
                </div>
            </form>

        </div>
        <table class="highlight">
            <thead>
            <tr>
                <th>Programme name</th>
                <th class="right-align">Budget places</th>
                <th class="right-align">Total places</th>
                <th class="center-align">Applications start date</th>
                <th class="center-align">Applications end date</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            <c:if test="${not empty programmes}">

                <c:forEach var="tuple" items="${programmes}">

                    <tr>

                        <td><a href="${appName}/app?cmd=programme&programmeId=${tuple.first.id}"
                               class="black-text">${tuple.first.name}</a></td>
                        <td class="right-align">${tuple.second.budgetPlaces}</td>
                        <td class="right-align">${tuple.second.totalPlaces}</td>
                        <td class="center-align">${tuple.second.admissionsStartDate}</td>
                        <td class="center-align">${tuple.second.admissionsEndDate}</td>
                        <td>
                            <a href="${appName}/app?cmd=programme&programmeId=${tuple.first.id}" class="black-text">
                                <i class="material-icons right">info_outline</i>
                            </a>
                        </td>

                    </tr>

                </c:forEach>
            </c:if>
            </tbody>
        </table>

        <c:if test="${empty programmes}">
                <p class="center-align">Nothing found</p>
        </c:if>

        <c:if test="${not empty programmes}">
            <ex:pagination currentPage="${paginator.currentPage}"
                           lastPage="${paginator.totalPages}"
                           url="${SELF}"/>
        </c:if>
    </div>

</div>


<jsp:include page="./common/footer.jspf"/>
<script type="text/javascript" src="${appName}/static/js/select.js"></script>

</body>
</html>