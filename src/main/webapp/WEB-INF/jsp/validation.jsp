<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:set var="pageTitle" value="Validation page" scope="page"/>
<%@ include file="./common/header.jspf" %>

<body>
<c:if test="${loggedUser!=null}">
    <c:redirect url="/app?cmd=departments"/>
</c:if>

<%@ include file="./common/navbar.jspf" %>

<div class="container">
<div class="row">
    <div class="col s3"></div>
    <div class="col s6">
        <div class="col s12 m12">
            <h3 class="header center-align">Validation</h3>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">


                        <div class="row">
                            <c:if test="${not empty errorMessage}">
                                <h6 class="center-align red-text text-darken-1">${errorMessage}</h6>
                                <c:remove var="errorMessage" scope="session" />
                            </c:if>

                            <h6 class="center-align red-text text-darken-1">${validationCode}</h6>
                            <form class="col s12" action="${appName}/app" method="post">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="code" name="userCode" value="" type="text" class="validate" required="" aria-required="true">
                                        <label for="code">Code</label>
                                        <span class="helper-text" data-error="Please, enter code from email" data-success=""></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="center-align">
                                        <button class="btn waves-effect waves-light" type="submit"  name="cmd" value="validate">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s3"></div>
</div>
</div>
<%@ include file="./common/footer.jspf" %>

</body>
</html>