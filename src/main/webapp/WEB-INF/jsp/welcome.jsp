<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ex" uri="customTags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="I18N/translation"/>

<c:set var="pageTitle" value="Welcome page" scope="page"/>

<%@ include file="./common/header.jspf" %>

<body>
<div class="parallax-container">
    <div class="row container">
        <H1 class="white-text">Doctrina lux est</H1>
    </div>
    <div class="row container right-align">
        <h1 class="white-text">et ignorantia tenebrae</h1>
    </div>
    <div class="parallax">
        <img src="./static/img/1.jpg">
    </div>
</div>
<div class="section brown lighten-4">
    <div class="row container">
        <h2 class="header"><fmt:message key="header.admission_committee"/></h2>
        <p class="grey-text text-darken-3 lighten-3"><fmt:message key="text.welcome1"/>
            <a href="${appName}/app?cmd=departments" title='<fmt:message key="label.departments"/>'>
                <fmt:message key="text.welcome2"/></a>
            <fmt:message key="text.welcome3"/>
            <a href="${appName}/app?cmd=searchProgramme" title='<fmt:message key="label.searchProgramme"/>'>
                <fmt:message key="text.welcome4"/></a>
            <fmt:message key="text.welcome5"/>
            <a href="${appName}/app?cmd=login" title='<fmt:message key="text.welcome6"/>'><fmt:message
                    key="text.welcome6"/></a>
            <fmt:message key="text.welcome7"/>
            <a href="${appName}/app?cmd=register" title='<fmt:message key="text.welcome8"/>'><fmt:message
                    key="text.welcome8"/></a>
            <fmt:message key="text.welcome9"/></p>
        <ul>
            <fmt:message key="text.welcome10"/>
            <li>
                - <fmt:message key="text.welcome11"/>
            </li>
            <li>
                - <fmt:message key="text.welcome12"/>
            </li>
            <li>
                - <fmt:message key="text.welcome13"/>
            </li>
            <li>
                - <fmt:message key="text.welcome14"/>
            </li>
        </ul>
        <p class="grey-text text-darken-3 lighten-3">
            <fmt:message key="text.welcome15"/></p>
        <span>
            <p class="grey-text text-darken-3 lighten-3">
            <fmt:message key="text.welcome16"/>
            <ex:changeLanguage
                    selfUrl="${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string']}">
                <fmt:message key="text.welcome17"/> <i class="tiny material-icons">language</i>
            </ex:changeLanguage>
        </p>
        </span>

        <p class="grey-text text-darken-3 lighten-3">
            <fmt:message key="text.welcome18"/></p>
        <p class="grey-text text-darken-3 lighten-3">
        <form id="loginForm1" action="${appName}/app" method="post">
            <input type="hidden" name="email" value="email5@mail.com"/>
            <input type="hidden" name="password" value="12345678"/>
            <input type="hidden" name="cmd" value="login"/>
            <div><fmt:message key="text.welcome19"/>
                <a href="#"
                   title='<fmt:message key="label.Login"/>'
                   onclick="document.forms['loginForm1'].submit(); return false;">
                    Микола Болконський (User)
                </a>
            </div>
        </form>
        <br>
        <form id="loginForm2" action="${appName}/app" method="post">
            <input type="hidden" name="email" value="email10@mail.com"/>
            <input type="hidden" name="password" value="12345678"/>
            <input type="hidden" name="cmd" value="login"/>
            <div><fmt:message key="text.welcome19"/>
                <a href="#"
                   title='<fmt:message key="label.Login"/>'
                   onclick="document.forms['loginForm2'].submit(); return false;">
                    Олена Курагіна (User)
                </a>
            </div>
        </form>
        <br>
        <form id="loginForm3" action="${appName}/app" method="post">
            <input type="hidden" name="email" value="email1@mail.com"/>
            <input type="hidden" name="password" value="12345678"/>
            <input type="hidden" name="cmd" value="login"/>
            <div><fmt:message key="text.welcome19"/>
                <a href="#"
                   title='<fmt:message key="label.Login"/>'
                   onclick="document.forms['loginForm3'].submit(); return false;">
                    Ілья Ростов (Admin)
                </a>
            </div>
        </form>

        </p>

    </div>
</div>
<div class="parallax-container">
    <div class="parallax"><img src="./static/img/2.jpg"></div>
</div>


<%@ include file="./common/footer.jspf" %>

<script type="text/javascript" src="./static/js/parallax.js"></script>
<script type="text/javascript" src="./static/js/carousel.js"></script>
\
</body>
</html>