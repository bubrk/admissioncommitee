<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="${appName}/static/css/materialize.min.css" rel="stylesheet" type="text/css" media="screen,projection"/>
    <%--    <link rel="stylesheet" type="text/css" href="css/materialize.min.css"  media="screen,projection"/>--%>

    <link href="${appName}/static/css/style.css" rel="stylesheet" type="text/css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Title</title>
</head>
<body>

<div class="container center">
    <br>
    <h1 class="header">ACCESS NOT GRANTED</h1>
    <h4>Ops, it looks like you are in the black list now</h4>
    <c:if test="${not empty commentMessage}">
        <h4>The reason is: <em>"${commentMessage}"</em></h4>
    </c:if>
    <h4>Try to message administration: <a href="mailto:admin@admission.committee.edu">admin@admission.committee.edu</a></h4>
</div>

</body>
</html>
