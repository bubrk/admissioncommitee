<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="${appName}/static/css/materialize.min.css" rel="stylesheet" type="text/css" media="screen,projection"/>
    <%--    <link rel="stylesheet" type="text/css" href="css/materialize.min.css"  media="screen,projection"/>--%>

    <link href="${appName}/static/css/style.css" rel="stylesheet" type="text/css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Title</title>
</head>
<body>

<div class="container center">
    <br>
    <h1 class="header">UNEXPECTED ERROR :(</h1>
    <c:if test="${not empty errorMessage}">
        <h5 class="header red-text">Message: ${errorMessage}</h5>
        <c:remove var="errorMessage" scope="session" />
    </c:if>

    <h4>Oops! It looks like our developers fell asleep.</h4>
    <h4>Or maybe we should to pay them finally?</h4>
    <h4>Let's go <a href="./">home</a> and try one more time from there</h4>

</div>

</body>
</html>
