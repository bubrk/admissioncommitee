window.post = function(url, params) {
    return fetch(url, {method: "POST", body: JSON.stringify(params)});
}