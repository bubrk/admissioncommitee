document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {edge:"right"});
});

function openSideNav(){
    var instance = M.Sidenav.getInstance('.sidenav');
    instance.open();
}
