var index = 1;

document.addEventListener('DOMContentLoaded', function () {
    let elem = document.getElementById("groups_amount");
    let groupsAmount = 0;

    if (elem != null) {
        groupsAmount = elem.value;
    }


    if (groupsAmount > 0) {
        setChipsWithInitialData(groupsAmount);
        index = ++groupsAmount;
    } else {
        addSubjectGroup();
    }

});

function addSubjectGroup() {
    const nextSubjectGroup = htmlToElement(`<div id="group_${index}">
                                            <div class="input-field col s12">
                                                <div id="chips_${index}" class="chips chips-autocomplete">
                                                <input class="custom-class" name="subjects_group_${index}"/>
                                                </div>
                                                <input type="hidden" id="subjects_group_${index}" name="subjects_group_${index}">
                                            </div>
                                            <div class="input-field col s6">
                                                <input id="score_${index}" name="score_group_${index}" value="" type="number"
                                                       class="validate"
                                                       min="100" max="200" required aria-required="true">
                                                <label for="score_${index}">Min score</label>
                                                <span class="helper-text" data-error="Please, enter your score"
                                                      data-success="ok"></span>
                                            </div>
                                            <div class="input-field col s6">
                                                <input id="coef_${index}" name="coef_group_${index}" value="" type="number" class="validate"
                                                       min="0.01" step="0.01" max="1" required aria-required="true">
                                                <label for="coef_${index}">Coeficient</label>
                                                <span class="helper-text" data-error="Please, enter coeficient"
                                                      data-success="ok"></span>
                                            </div>
                                        </div>`);
    document.getElementById("subjectRequirements")
        .appendChild(nextSubjectGroup);
    initChip(`chips_${index}`, {});

    index++;
}

function setChipsWithInitialData(groupsAmount) {
    for (let index = 1; index <= groupsAmount; index++) {
        let data = [];
        let elem = document.getElementById(`initial_subjects_group_${index}`);
        if (elem != null) {
            let subjects = elem.value.split(',');
            for (let i = 0; i < subjects.length - 1; i++) {
                if (subjects[i]){
                    let chip = {};
                    chip.tag = subjects[i];
                    data.push(chip);
                }
            }
        }

        initChip(`chips_${index}`, data);
    }
}

function initChip(chipId, initialData) {
    let elems = document.getElementById(chipId)
    let list = document.getElementById("subjectList").value;

    let subjects = JSON.parse(list);

    let instance = M.Chips.init(elems, {
        placeholder: 'Subjects', secondaryPlaceholder: '+another subject',
        data: initialData,
        autocompleteOptions: {
            data: subjects,
            limit: Infinity,
            minLength: 1
        }
    });
}

function submitForm(formId) {
    submitChips();
    document.getElementById(formId).submit();
}

function submitChips() {
    const requirements = document.getElementById("subjectRequirements");
    const elems = document.querySelectorAll('.chips');
    elems.forEach(elem => {
        const index = elem.id.replace("chips_", "");
        const data = M.Chips.getInstance(elem).chipsData;
        data.forEach(chip => {
            requirements.appendChild(htmlToElement(`<input type="hidden" 
                name="subjects_group_${index}" 
                value="${chip.tag}"/>`));
        })
    })
}

/**
 * @param {String} HTML representing a single element
 * @return {Element}
 */
function htmlToElement(html) {
    let template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

function removeElement(id){
    let elem = document.getElementById(id);
    elem.remove();
}