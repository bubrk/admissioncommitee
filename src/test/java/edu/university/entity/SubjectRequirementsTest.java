package edu.university.entity;

import edu.university.exception.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SubjectRequirementsTest {

    private static Validator validator;

    @BeforeAll
    static void prepareValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void testEmptySubjectsList() {
        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(new ArrayList<>(),150,0.7)));

        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(null,150,0.7)));
    }

    @Test
    void testInvalidScores() {
        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(getListOfSubjects(),90,0.7)));

        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(getListOfSubjects(),250,0.7)));

        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(getListOfSubjects(),-150,0.7)));
    }

    @Test
    void testInvalidCoefficients() {
        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(getListOfSubjects(),150,-1)));

        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(getListOfSubjects(),150,3)));

        Assertions.assertThrows(ValidationException.class,()->validateRequirements(
                getSubjectRequirements(getListOfSubjects(),150,1.3)));
    }

    @Test
    void testValidRequirements() {
        try {
            validateRequirements(
                    getSubjectRequirements(getListOfSubjects(),150,0.7));

            validateRequirements(
                    getSubjectRequirements(getListOfSubjects(),100,0.01));

            validateRequirements(
                    getSubjectRequirements(getListOfSubjects(),200,1));
        } catch(Exception e) {
            System.out.println(e.getMessage());
            fail("Should not have thrown any exception");
        }
    }

    static void validateRequirements(SubjectRequirements subjectRequirements) throws ValidationException {
        Set<ConstraintViolation<SubjectRequirements>> violations = validator.validate(subjectRequirements);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.iterator()
                    .next().getMessage());
        }
    }


    static List<Subject> getListOfSubjects(){
        ArrayList<Subject> subjects = new ArrayList<>();
        subjects.add(getSubject("Математика"));
        subjects.add(getSubject("Фізика"));

        return subjects;
    }

    static Subject getSubject(String name) {
        return new Subject.Builder()
                .name(name)
                .build();
    }

    static SubjectRequirements getSubjectRequirements(List<Subject> subjects, int score, double coef) {
        return new SubjectRequirements.Builder()
                .subjects(subjects)
                .minScore(score)
                .coefficient(coef)
                .build();
    }
}