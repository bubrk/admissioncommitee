package edu.university.entity;



import edu.university.exception.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.fail;


class SubjectTest {

    private static Validator validator;

    @BeforeAll
    static void prepareValidator(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void testNullAndEmptyInput(){
        //@NullAndEmptySource
        Assertions.assertThrows(ValidationException.class,()->validateSubject(getSubject(null)),
                "Subject name can't be null");

        Assertions.assertThrows(ValidationException.class,()->validateSubject(getSubject("")),
                "Subject name can't be empty");
    }

    @Test
    void testToLongInput(){
        String longName= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore";
        Assertions.assertThrows(ValidationException.class,()->validateSubject(getSubject(longName)));
    }

    @ParameterizedTest
    @MethodSource("testCasesCorrectSubjectNames")
    void testCorrectSubjectsName(String in) {
        try {
            validateSubject(getSubject(in));
        } catch(Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @ParameterizedTest
    @MethodSource("testCasesIncorrectSubjectNames")
    void testIncorrectSubjectsName(String in) {
        Assertions.assertThrows(ValidationException.class,()->validateSubject(getSubject(in)));
    }

    static void validateSubject(Subject subject) throws ValidationException {
        Set<ConstraintViolation<Subject>> violations = validator.validate(subject);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.iterator()
                    .next().getMessage());
        }
    }

    static Stream<String> testCasesCorrectSubjectNames() {
        return Stream.of(
                "Test-test",
                "Математика",
                "Фізика",
                "Об'єкт",
                "Українська мова",
                "Українська мова та література"
        );
    }

    static Stream<String> testCasesIncorrectSubjectNames() {
        return Stream.of(
                "#$%123",
                "Математика~{=+}",
                ":)",
                "\uD83D\uDE0A",
                "How are you?"
        );
    }

    static Subject getSubject(String name){
        return new Subject.Builder()
                .name(name)
                .build();
    }

}