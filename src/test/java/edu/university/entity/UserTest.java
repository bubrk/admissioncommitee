package edu.university.entity;

import edu.university.exception.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private static Validator validator;

    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }


    @ParameterizedTest
    @MethodSource("testValidUserCases")
    void testValidUsers(User in) {
        try {
            validateUser(in);
        } catch(Exception e) {
            System.out.println(e.getMessage());
            fail("Should not have thrown any exception");
        }
    }

    @ParameterizedTest
    @MethodSource("testIncorrectNamesCases")
    void testIncorrectNames(User usersIn) {
        Assertions.assertThrows(ValidationException.class,()-> validateUser(usersIn));
    }

    @ParameterizedTest
    @MethodSource("testIncorrectEmailCases")
    void testIncorrectEmails(User usersIn) {
        Assertions.assertThrows(ValidationException.class,()-> validateUser(usersIn));
    }

    @ParameterizedTest
    @MethodSource("testIncorrectPasswordCases")
    void testIncorrectPasswords(User usersIn) {
        Assertions.assertThrows(ValidationException.class,()-> validateUser(usersIn));
    }

    static void validateUser(User user) throws ValidationException {
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.iterator()
                    .next().getMessage());
        }
    }

    static Stream<User> testIncorrectEmailCases() {
        return Stream.of(
                getUser("FirstName","MiddleName","LastName","12345678","emailmail.com"),
                getUser("FirstName","MiddleName","LastName","12345678","emailmail"),
                getUser("FirstName","MiddleName","LastName","12345678","test@mail.333"),
                getUser("FirstName","MiddleName","LastName","12345678","email@mail.site"),
                getUser("FirstName","MiddleName","LastName","12345678","email@_mail.com"),
                getUser("FirstName","MiddleName","LastName","12345678","@emailmail.com"),
                getUser("FirstName","MiddleName","LastName","12345678",""),
                getUser("FirstName","MiddleName","LastName","12345678",null)
        );
    }

    static Stream<User> testIncorrectPasswordCases() {
        return Stream.of(
                getUser("FirstName","MiddleName","LastName","12345","email@mail.com")
        );
    }

    static Stream<User> testIncorrectNamesCases() {
        return Stream.of(
                getUser("FirstName$#","MiddleName","LastName","12345678","email@mail.com"),
                getUser("FirstName","MiddleName%^","LastName","12345678","email@mail.com"),
                getUser("FirstName","MiddleName","LastName?","12345678","email@mail.com"),
                getUser("","MiddleName","LastName","12345678","email@mail.com"),
                getUser("FirstName","MiddleName","","12345678","email@mail.com"),
                getUser(null,"MiddleName","LastName","12345678","email@mail.com"),
                getUser("FirstName","MiddleName",null,"12345678","email@mail.com"),
                getUser("FirstName :)","MiddleName","LastName","12345678","email@mail.com"),
                getUser("FirstName MiddleName","","LastName","12345678","email@mail.com"),
                getUser("F","","LastName","12345678","email@mail.com"),
                getUser("FirstName","","L","12345678","email@mail.com")
        );
    }

    static Stream<User> testValidUserCases() {
        return Stream.of(
                getUser("FirstName","MiddleName","LastName","12345678","email@mail.NET"),
                getUser("Микола","Миколайович","Миколенко","12345678","email@mail.com"),
                getUser("Просто","","Микола","12345678","email@mail.ua")
        );
    }

    static User getUser(String firstName, String middleName, String lastName, String password, String email){
        return new User.Builder()
                .email(email)
                .password(password)
                .firstName(firstName)
                .middleName(middleName)
                .lastName(lastName)
                .build();
    }

}