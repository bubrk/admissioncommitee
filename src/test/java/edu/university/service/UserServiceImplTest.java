package edu.university.service;

import edu.university.config.ObjectFactory;
import edu.university.dao.UserDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

class UserServiceImplTest {
    @Mock
    private UserDAO userDAO;
    private UserService userService;

    UserServiceImplTest() {
        MockitoAnnotations.openMocks(this);
        this.userService = new UserServiceImpl();
    }

    @Test
    void getUser() {
        try {
            // This is just a playground with mockito
            given(userDAO.getUser(1)).willReturn(Optional.empty());
            boolean userPresent = userDAO.getUser(1).isPresent();
            assertFalse(userPresent);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Should not catch any exceptions");
        }
    }
}